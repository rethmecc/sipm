**nEXO SiPM DAQ / Analysis Repository**

File Structure
--------------

`bin`: executable files

`code`: all source code

`lib`: helper libraries

`macro`: root analysis macros

`obj`: contains the object files created by the makefile - do not edit by hand!

`packages`: file format library

-------------
Prerequisites
-------------

ROOT - Must be installed in the typical fashion.

HDF5 - The relevant files are included in `packages`. Before use:

* `packages/hdf5-1.8.13` should be moved to a packages subdirectory of the user's home folder

* A new environment variable `HDF5SYS` should be created and set to the new path of the hdf5-1.8.13 directory

* The path to `hdf5-1.8.13/lib` should be appended to the `LD_LIBRARY_PATH` environment variable

* `hdf5-1.8.13/bin/h5redeploy` should be run to change site specific paths.

-----
Usage
-----

Making Executables: 

* `make` - compiles all

* `make lib` - compiles libraries required to run

* `make PF` - compiles PulseFinding.exe

* `make DAQ` - compiles data acquisition software

* `make clean` - removes all `.o`, `.lib`, and `.exe` files.

Aquiring data from LeCroy scope: `python DAQSoftware/fetch.py <FILE SAVENAME> -n <NUMBER OF EVENTS> -r <NUMBER OF RUNS>`

        Note: default save location is /data20/exo/scope/
                  
Processing acquired data to find pulses: `./bin/PulseFinding.exe <RUN NUMBER>`

        Note: Run Number is referenced from LampRunInfo.txt.

-------------
File Overview
-------------

code:

* `RootDict.cxx`, `RootDict.h` - Incomprehensible files generated automatically by ROOT CINT for doing all sorts of arcane ROOT CINT-y things.

* `Linkdef.h` - Unknown, perhaps same as previous.

* `LecroyFile.cxx`, `LecroyFile.h`, `LecroyHdfFile.cxx`, `LecroyHdfFile.h`, `DataFile.h` - Files used in processing the hdf5 files saved from the oscilloscope.

* `Waveform.cxx`, `Waveform.h` - The code for the class used to store data representing the waveform of a particular event.

* `WaveformProcessor.cxx`, `WaveformProcessor.h` - The class used in the processing of the waveforms. Includes functions for finding the peaks, and outputting the relevant calculated parameters.

* `PulseFinding.cxx` - The code for the class used to build PulseFinding.exe, which largely relies on the WaveformProcessor object to process the waveforms.

-------
Authors
-------
See AUTHORS

-------------
Documentation
-------------
See DOCUMENTATION