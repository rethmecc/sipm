//gSystem->Load("lib/libSipmAnalysis.so")
#include <fstream>

struct EventData{
  double pulseBounds[100][2];
  double peaks[100];
  int peakBins[100];
  double peakiness[100];
  double lb;
  double ub;
  double nPulse;
  double nPeak;
};

WaveformProcessor* wfProc;

int GetMatching(int aEvent, int nPeaks, int* peakBins, int nBinsSlopeCheck, double diffAmpFactor, double lowerBLFactor, double upperBLFactor, double minPulseWidth, double peakinessThresh){

  //std::cout << "nBSC: " << nBinsSlopeCheck << ", dAF: " << diffAmpFactor << ", lBLF: " << lowerBLFactor << ", uBLF: " << upperBLFactor << ", mPW: " << minPulseWidth << ", pT: " << peakinessThresh << std::endl;



  wfProc->readWaveform(aEvent);
  wfProc->processBaseline();
  
  int res = wfProc->findPulseGeneric(nBinsSlopeCheck, diffAmpFactor, lowerBLFactor, upperBLFactor, minPulseWidth, peakinessThresh);

  int nMatch = 0;
  int nFalse = 0;
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    for (int jPeak = 0; jPeak < wfProc->mEventData.nPeak; jPeak++){
      if ((wfProc->mEventData.peakBins[jPeak] == peakBins[iPeak])){
	nMatch++;
	break;
      }
    }
  }

  for (int jPeak = 0; jPeak < wfProc->mEventData.nPeak; jPeak++){
    bool matched = false;
    for (int iPeak = 0; iPeak < nPeaks; iPeak++){
      if ((wfProc->mEventData.peakBins[jPeak] == peakBins[iPeak])){
        matched = true;
        break;
      }   
    } 
    if (!(matched)) nFalse++;
  }
  //std::cout << nMatch - nFalse << std::endl;
  return nMatch - nFalse;
 
}


void TrainPF(const char* aSetupFileName, int aRun, int aEvent, int bEvent, char* dataFile, int aFitType=0, int aChannel=1){//int test;cin >> test;cout << &test << endl;

  wfProc = WaveformProcessor::instanceForRootMacro(aSetupFileName,aRun,6,aChannel);
  ifstream iFile;
  iFile.open(dataFile);
  double mPW = 2e-8;

  int nPeaks[100];
  int peakBins[100][100];
  int fileRun;
  int fileEvent;

  double bestPars[5];
  double bestTotalScore = 0;
  int score;
  double totalscore;
  
  for (int event = aEvent; event <= bEvent; event++){
    iFile >> fileRun;
    iFile >> fileEvent;
    if ((fileRun != aRun) || (fileEvent != event)) std::cout << "File error!" << std::endl;
    iFile >> nPeaks[event - aEvent];
    for (int i = 0; i < nPeaks[event - aEvent]; i++){
      iFile >> peakBins[event - aEvent][i];
    }
  }
  double percent = 0;
  double frac;
  for (int nBSC = 1; nBSC <= 6; nBSC++){
    for (double dAF = 0.6; dAF <= 2.6; dAF += 0.2){
      percent += 1.52;
      std::cout << percent << "%" << std::endl;
      for (double lBLF = 2.0; lBLF <= 6.0; lBLF += 0.4){
	for (double uBLF = 0.1; uBLF <= 1.1; uBLF += 0.1){
	  for (double pT = 0.02; pT <= 0.04; pT += 0.002){
	    totalscore = 0.0;
	    for (int event = aEvent; event <= bEvent; event++){
	      score = GetMatching(event, nPeaks[event-aEvent], peakBins[event-aEvent], nBSC, dAF, lBLF, uBLF, mPW, pT);
	      frac = score;
	      frac /= nPeaks[event-aEvent];
	      totalscore += frac;
	    }
	    totalscore /= (bEvent - aEvent + 1);
	    if ((totalscore > bestTotalScore)){
	      bestTotalScore = totalscore;
	      bestPars[0] = nBSC;
	      bestPars[1] = dAF;
	      bestPars[2] = lBLF;
	      bestPars[3] = uBLF;
	      bestPars[4] = pT;
	    }
	  }
	}
      }
    }
  }
  std::cout << bestTotalScore << " " << bestPars[0] << " " << bestPars[1] << " " << bestPars[2] << " " << bestPars[3] << " " << bestPars[4] << std::endl;    
}
