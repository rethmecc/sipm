#include <fstream>

//For drawing and merging temperature dependence graphs

void TempGraphs(int nRuns, int aRun[], double aTemp[])
{
	ifstream iFile;
	char filename[100];
	double tempdata[100][14];
	char buffer[100];
	for(int i = 0; i < nRuns; i++)
	{
		sprintf(filename, "./Dark Analysis/Parameters/%i-DarkAnalysis.txt", aRun[i]);
		iFile.open(filename);
		for(int j = 0; j < 7; j++) iFile >> buffer;
		for(int j = 0; j < 7; j++)
		{
			iFile >> tempdata[i][2*j];
			iFile >> buffer;
			iFile >> tempdata[i][2*j+1];
		}
		iFile.close();
	}

	char* titles[7] = {"Dark Noise Rate", "Average Number of Afterpulses", "Crosstalk Probability", "Average Pulse Amplitude", "Average Pulse Rise Time", "Average Pulse Fall Time", "Average Pulse Chi-Square Value"};
	char savename[100];
	
	for(int i = 0; i < 7; i++)
	{
		TGraphErrors* g1 = new TGraphErrors(nRuns);
		for (int j = 0; j < nRuns; j++)
		{
			if (tempdata[j][2*i+1] < tempdata[j][2*i]){
				g1->SetPoint(j, aTemp[j], tempdata[j][2*i]);
				g1->SetPointError(j, 1, tempdata[j][2*i+1]);
			}
		}
		g1->SetTitle(titles[i]);
		g1->GetXaxis()->SetTitle("Temperature ^{/circ}C");
		g1->GetYaxis()->SetTitle(titles[i]);

		TCanvas* c1 = new TCanvas(titles[i], titles[i], 900, 700);
		g1->Draw("AP");
		
		sprintf(savename, "./Temp Graphs/%s.pdf", titles[i]);
		c1->SaveAs(savename);

		//delete c1;
		//delete g1;
	}
}
