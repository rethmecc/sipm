#include<fstream>
#include<stdio.h>
#include <cmath>


// --- Map the ntpule content
struct NtpCont{
	float evt; 
	float blmu; 
	float np; 
	float pa; 
	float pt; 
	float nfp; 
	float fa; 
	float ft; 
	float frt; 
	float fft; 
	float fblmu; 
	float chi2; 
	float ndf; 
	float chi2r; 
	float ndfr;
};

// --- Produces the time distribution
// >>> Only keep events with 1 pulse
TH1D* gHSingleTime;
TH1D* gHDTime;
int nBin;
double LaserTime; // ns (roughly)
double minTime;
double maxTime;
char HName[50];
double shift;
double dnrate;
double dntotal;
int numevents[4];
int num2to80[4];
int num80to500[4];

double igt2to80[4][100];
double igt80to500[4][100];

double FitFunction(double* x, double* p){
	double val = p[0]*TMath::Exp(p[1]/2*(p[1]*p[3]*p[3]-2*p[4]*(x[0]-p[2])))*TMath::Erfc((p[1]*p[3]*p[3]-p[4]*(x[0]-p[2]))/(sqrt(2.0)*p[3]));
	return val;
}

void calcTimeDist(int aType, double cut){
	double lim1 = 3e-9;
	double lim2 = 8e-8;
	double lim3 = 5e-7;
		
	nBin=20000;
	double bpns = nBin/2000;
	LaserTime=50; // ns (roughly)
	minTime=-1000;
	maxTime=1000;
	shift = 54.2;
	sprintf(HName,"HSingleTime%i",aType);
	gHSingleTime = (TH1D*) gROOT->FindObjectAny(HName);
	if(gHSingleTime) gHSingleTime->Reset(); //if option "ICE" is specified, resets only Integral, Contents and Errors.
	else gHSingleTime = new TH1D(HName,HName,nBin,minTime,maxTime);
	if((aType==102)||(aType==104)||(aType==106)||(aType==108)) numevents[(aType-102)/2] = 0;

	int entry;
	
	if((aType==102)||(aType==103)){
		shift = 53.9904;
		dnrate = 4.91791e-06;
		entry = 0;
	}
	else if((aType==104)||(aType==105)){
		shift = 54.617;
		dnrate = 4.90659e-06;
		entry = 1;
	}
	else if((aType==106)||(aType==107)){
		shift = 53.7375;
		dnrate = 9.40057e-08;
		entry = 2;
	}
	else {
		shift = 54.2782;
		dnrate = 1.1601e-07;
		entry = 3;
	}

//Looks up run number in RunInfo.txt, finds associated file name and formats it.
	char FileNames [100] [2] [100];
	char activename [200];
	ifstream iFile;
	iFile.open("/home/deap/nEXO/RunInfo.txt");
	int counter = 0;
	char buffer [10000];
	for (int i = 0; i < 10; i++){
		iFile >> buffer;
	}
	while(!iFile.eof()){
		iFile >> FileNames[counter][0];
		iFile >> FileNames[counter][1];
		counter++;
		for (int i = 0; i < 8; i++){
			iFile >> buffer;
		}
	}
	for (int i = 0; i < counter; i++){
		int runnum;
		sscanf(FileNames[i][0], "%d", &runnum);
		if((runnum == aType)){
			char* newbuff;
			newbuff = FileNames[i][1];
			cout << newbuff << endl;
			sprintf(activename, "/home/deap/nEXO/%s.anat0", newbuff);
			cout << activename << endl;		
			break;
		}
		if((i == counter - 1)){
			cout << "No file found" << endl;
		}
	}


	TFile* fIn = (TFile*) gROOT->GetListOfFiles()->FindObject(activename);
	if(!fIn) fIn = new TFile(activename);
	TNtuple* ntp = (TNtuple*) fIn->Get("ntp");
	NtpCont* cont = (NtpCont*) ntp->GetArgs();
    int nPulse = ntp->GetEntries(); //get number of pulses
	int iPulse=0;
    while(iPulse<nPulse){ //iterate through pulses
		ntp->GetEntry(iPulse); //reads current entry and returns total number of bytes read
		if(cont->nfp==1 && cont->fa<-0.01){// one pulse (can be >1PE due to xtalk)
			gHSingleTime->Fill(cont->ft*1e9-shift);
			if(cut > 0){
				if((cont->ft > lim1+shift*1e-9) && (cont->ft < cut*1e-9+shift*1e-9)) num2to80[(aType-103)/2]++;
				if(cont->ft < cut*1e-9+shift*1e-9) numevents[(aType-103)/2]++;
			}
			else if(cut < 0){
				if((cont->ft > cut*1e-9+shift*1e-9) && (cont->ft < lim2+shift*1e-9)) num2to80[(aType-102)/2]++;
				else if((cont->ft > lim2+shift*1e-9) && (cont->ft < lim3+shift*1e-9)) num80to500[(aType-102)/2]++;
				if(cont->ft > cut*1e-9+shift*1e-9) numevents[(aType-102)/2]++;
			}
		}

		if(iPulse%1000==0) cout << iPulse << " " << cont->nfp <<  endl;
		iPulse++;		
	}

	if((aType==102)||(aType==104)||(aType==106)||(aType==108)) dntotal = dnrate * 1000000;
	else dntotal = dnrate * 100000;

	double sum = 0;
	TAxis* Xaxis = gHSingleTime->GetXaxis();
	for(int iBin = 0; iBin < gHSingleTime->GetNbinsX(); iBin++){
		if(gHSingleTime->GetBinContent(iBin) > dntotal/bpns) gHSingleTime->SetBinContent(iBin, gHSingleTime->GetBinContent(iBin)-dntotal/bpns);
		else gHSingleTime->SetBinContent(iBin, 0);
		sum += gHSingleTime->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	int counter = 0;
	double average = 0;
	double stdev = 0;
	double values[500];
	double dcount;
	int binskip;
	double newshift;

	for(int i =  0; i < 100; i++){
		newshift = (i-50)*0.1;
		for(int iBin = 0; iBin < gHSingleTime->GetNbinsX(); iBin++){
			if(cut > 0){
				if((gHSingleTime->GetBinCenter(iBin) > lim1*1e9+newshift) && (gHSingleTime->GetBinCenter(iBin) < cut+newshift)){
					igt2to80[entry][i]+=gHSingleTime->GetBinContent(iBin)/sum;
				}
			}
			else if(cut < 0){
				if((gHSingleTime->GetBinCenter(iBin) > cut+newshift) && (gHSingleTime->GetBinCenter(iBin) < lim2*1e9+newshift)){
					igt2to80[entry][i]+=gHSingleTime->GetBinContent(iBin)/sum;
				}
				else if((gHSingleTime->GetBinCenter(iBin) > lim2*1e9+newshift) && (gHSingleTime->GetBinCenter(iBin) < lim3*1e9+newshift)){
					igt80to500[entry][i]+=gHSingleTime->GetBinContent(iBin)/sum;
				}
			}
		}
	}


	for(int iBin = 0; iBin < gHSingleTime->GetNbinsX(); iBin++){
		if((gHSingleTime->GetBinCenter(iBin) < 1)){
			gHSingleTime->SetBinContent(iBin, gHSingleTime->GetBinContent(iBin)/sum);
			gHSingleTime->SetBinError(iBin, gHSingleTime->GetBinError(iBin)/sum);
		}
		else{
			if(gHSingleTime->GetBinCenter(iBin) < 5) binskip=2;
			else if(gHSingleTime->GetBinCenter(iBin) < 10) binskip=20;
			else if(gHSingleTime->GetBinCenter(iBin) < 50) binskip=30;
			else if(gHSingleTime->GetBinCenter(iBin) < 100) binskip=50;
			else if(gHSingleTime->GetBinCenter(iBin) < 500) binskip=100;
			else binskip=200;


			if((iBin % binskip != 0)){
				values[counter] = gHSingleTime->GetBinContent(iBin);
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				values[counter] = gHSingleTime->GetBinContent(iBin);
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);

				for(int i = 0; i < counter; i++){
					stdev += (values[i]-average)*(values[i]-average);
				}
				stdev /= counter;
				stdev = sqrt(stdev);
				dcount = 1.0 * counter;
				stdev /= sqrt(dcount);
				gHSingleTime->SetBinError(iBin, stdev/sum);
				
				stdev = 0;
				average = 0;
				counter = 0;
			}
		}
	}
	if((cut>0)){
		for(int iBin = gHSingleTime->FindBin(cut); iBin < gHSingleTime->GetNbinsX(); iBin++){
			gHSingleTime->SetBinContent(iBin, 0);
		}
	}
	else if((cut<0)){
		for(int iBin = 0; iBin < gHSingleTime->FindBin(-1*cut); iBin++){
			gHSingleTime->SetBinContent(iBin, 0);
		}
	}
	
}

void TimeDist_v7(){
	TCanvas* c1 = new TCanvas("c1", "Time Distribution", 900, 600);
	c1->SetLogy();
	c1->SetLogx();
	gStyle->SetOptTitle(0); 

	int aType = 102;
	int bType = 103;
	int cType = 104;
	int dType = 105;
	int eType = 106;
	int fType = 107;
	int gType = 108;
	int hType = 109;

	double cut1 = 6.0;
	double cut2 = 2.0;
	double cut3 = 6.0;
	double cut4 = 2.0;

	for(int i = 0; i < 4; i++){
		num2to80[i] = 0;
		num80to500[i] = 0;
	}
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 100; j++){
			igt2to80[i][j] = 0;
			igt80to500[i][j] = 0;
		}
	}

	//aType corresponds with run number in RunInfo.txt file
	calcTimeDist(aType, -1*cut1);
	TH1D* h1 = gHSingleTime;

	calcTimeDist(bType, cut1);
	TH1D* h2 = gHSingleTime;

	TH1D* h3 = new TH1D("h3","h3",nBin,minTime,maxTime);

	double sum = 0;
	TAxis* Xaxis = h3->GetXaxis();
	for(int iBin = 0; iBin < h3->GetNbinsX(); iBin++){
		if((h3->GetBinCenter(iBin) < cut1)){
			h3->SetBinContent(iBin, h2->GetBinContent(iBin));
			h3->SetBinError(iBin, h2->GetBinError(iBin));
		}
		else{
			h3->SetBinContent(iBin, h1->GetBinContent(iBin));
			h3->SetBinError(iBin, h1->GetBinError(iBin));
		}
		sum += h3->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	for(int iBin = 0; iBin < h3->GetNbinsX(); iBin++){
		h3->SetBinContent(iBin, h3->GetBinContent(iBin)/sum);
		h3->SetBinError(iBin, h3->GetBinError(iBin)/sum);
	}
	h3->SetBit(TH1::kNoTitle); 
	h3->SetMarkerSize(0.8);
	h3->SetMarkerStyle(4);
	h3->SetMarkerColor(2);
	h3->SetMaximum(5);
	h3->GetXaxis()->SetRangeUser(0.1, 1000);
	h3->GetXaxis()->SetTitle("Adjusted Time (ns)");
	h3->GetYaxis()->SetTitle("Probability Distribution Function (ns^{-1})");
	h3->GetXaxis()->CenterTitle();
	h3->GetYaxis()->CenterTitle();
	h3->GetXaxis()->SetTitleFont();
	h3->GetXaxis()->SetTitleSize();
	h3->GetXaxis()->SetTitleOffset(1.1);
	h3->GetXaxis()->SetLabelFont();
	h3->GetXaxis()->SetLabelSize();
	h3->GetYaxis()->SetTitleFont();
	h3->GetYaxis()->SetTitleSize();
	h3->GetYaxis()->SetLabelFont();
	h3->GetYaxis()->SetLabelSize();
	h3->SetStats(0);
	h3->Draw("EP9");



	calcTimeDist(cType, -1*cut2);
	TH1D* h4 = gHSingleTime;

	calcTimeDist(dType, cut2);
	TH1D* h5 = gHSingleTime;

	TH1D* h6 = new TH1D("h6","h6",nBin,minTime,maxTime);

	double sum = 0;
	TAxis* Xaxis = h6->GetXaxis();
	for(int iBin = 0; iBin < h6->GetNbinsX(); iBin++){
		if((h6->GetBinCenter(iBin) < cut2)){
			h6->SetBinContent(iBin, h5->GetBinContent(iBin));
			h6->SetBinError(iBin, h5->GetBinError(iBin));
		}
		else{
			h6->SetBinContent(iBin, h4->GetBinContent(iBin));
			h6->SetBinError(iBin, h4->GetBinError(iBin));
		}
		sum += h6->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	for(int iBin = 0; iBin < h6->GetNbinsX(); iBin++){
		h6->SetBinContent(iBin, h6->GetBinContent(iBin)/sum);
		h6->SetBinError(iBin, h6->GetBinError(iBin)/sum);
	}
	h6->SetMarkerSize(0.8);
	h6->SetMarkerStyle(25);
	h6->SetMarkerColor(4);
	h6->SetMaximum(5);
	h6->GetXaxis()->SetRangeUser(0.1, 1000);
	h6->Draw("same EP9");


	calcTimeDist(eType, -1*cut3);
	TH1D* h7 = gHSingleTime;

	calcTimeDist(fType, cut3);
	TH1D* h8 = gHSingleTime;

	TH1D* h9 = new TH1D("h9","h9",nBin,minTime,maxTime);

	double sum = 0;
	TAxis* Xaxis = h9->GetXaxis();
	for(int iBin = 0; iBin < h9->GetNbinsX(); iBin++){
		if((h9->GetBinCenter(iBin) < cut3)){
			h9->SetBinContent(iBin, h8->GetBinContent(iBin));
			h9->SetBinError(iBin, h8->GetBinError(iBin));
		}
		else{
			h9->SetBinContent(iBin, h7->GetBinContent(iBin));
			h9->SetBinError(iBin, h7->GetBinError(iBin));
		}
		sum += h9->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	for(int iBin = 0; iBin < h9->GetNbinsX(); iBin++){
		h9->SetBinContent(iBin, h9->GetBinContent(iBin)/sum);
		h9->SetBinError(iBin, h9->GetBinError(iBin)/sum);
	}
	h9->SetMarkerSize(0.8);
	h9->SetMarkerStyle(20);
	h9->SetMarkerColor(2);
	h9->SetMaximum(5);
	h9->GetXaxis()->SetRangeUser(0.1, 1000);
	h9->Draw("same EP9");


	calcTimeDist(gType, -1*cut4);
	TH1D* h10 = gHSingleTime;

	calcTimeDist(hType, cut4);
	TH1D* h11 = gHSingleTime;

	TH1D* h12 = new TH1D("h12","h12",nBin,minTime,maxTime);

	double sum = 0;
	TAxis* Xaxis = h12->GetXaxis();
	for(int iBin = 0; iBin < h12->GetNbinsX(); iBin++){
		if((h12->GetBinCenter(iBin) < cut4)){
			h12->SetBinContent(iBin, h11->GetBinContent(iBin));
			h12->SetBinError(iBin, h11->GetBinError(iBin));
		}
		else{
			h12->SetBinContent(iBin, h10->GetBinContent(iBin));
			h12->SetBinError(iBin, h10->GetBinError(iBin));
		}
		sum += h12->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	for(int iBin = 0; iBin < h12->GetNbinsX(); iBin++){
		h12->SetBinContent(iBin, h12->GetBinContent(iBin)/sum);
		h12->SetBinError(iBin, h12->GetBinError(iBin)/sum);
	}
	h12->SetMarkerSize(0.8);
	h12->SetMarkerStyle(21);
	h12->SetMarkerColor(4);
	h12->SetMaximum(5);
	h12->GetXaxis()->SetRangeUser(0.1, 1000);
	h12->Draw("same EP9");
	
    leg = new TLegend(0.65,0.75,0.9,0.9);
    
	leg->AddEntry("h3","T2K MPPC @ 777nm","p");
    leg->AddEntry("h6","T2K MPPC @ 405nm","p");
	leg->AddEntry("h9","2013 MPPC @ 777nm","p");
    leg->AddEntry("h12","2013 MPPC @ 405nm","p");

	char savename [100];
	sprintf(savename, "/home/deap/nEXO/TimeDist Fits/all.pdf");

	leg->Draw();
	c1->SaveAs(savename);

	double frac2to80[4];
	double frac80to500[4];
	for(int i = 0; i < 4; i++){
		frac2to80[i] = 1.0 * num2to80[i] / 1000000;
		frac80to500[i] = 1.0 * num80to500[i] / 1000000;
	}

	double results[100][100][5];
	std::cout << "Writing: " << std::endl;
	for(int i = 0; i < 100; i++){
		std::cout << i << "%" << std::endl;
		for(int j = 0; j < 100; j++){	
			results[i][j][0] = igt2to80[2][i]/igt2to80[0][j];
			if(results[i][j][0] < 0.53) results[i][j][0] = 0.53/results[i][j][0];
			else results[i][j][0] /= 0.53;

			results[i][j][1] = igt80to500[2][i]/igt80to500[0][j];
			if(results[i][j][1] < 0.065) results[i][j][1] = 0.065/results[i][j][1];
			else results[i][j][1] /= 0.065;

			results[i][j][2] = igt80to500[0][i]/igt2to80[0][j];
			if(results[i][j][2] < 0.33) results[i][j][2] = 0.33/results[i][j][2];
			else results[i][j][2] /= 0.33;

			results[i][j][3] = igt80to500[2][i]/igt2to80[2][j];
			if(results[i][j][3] < 0.041) results[i][j][3] = 0.041/results[i][j][3];
			else results[i][j][3] /= 0.041;

			results[i][j][4] = results[i][j][0] + results[i][j][1] + results[i][j][2] + results[i][j][3];
		}
	}
	std::cout << "Reading: " << std::endl;
	int bestpair[2] = {0,0};
	double bestval = results[0][0][1];
	for(int i = 0; i < 100; i++){
		std::cout << i << "%" << std::endl;
		for(int j = 0; j < 100; j++){	
			if(results[i][j][1] < bestval){
				bestval = results[i][j][1];
				bestpair [0] = i;
				bestpair [1] = j;
			}
		}
	}

	std::cout << "Best: " << (bestpair[0]-50)*0.1 << " " << (bestpair[1]-50)*0.1 << " " << bestval << std::endl;
	std::cout << results[bestpair[0]][bestpair[1]][0] << std::endl;
	std::cout << results[bestpair[0]][bestpair[1]][1] << std::endl;
	std::cout << results[bestpair[0]][bestpair[1]][2] << std::endl;
	std::cout << results[bestpair[0]][bestpair[1]][3] << std::endl;

/*
	double igtfrac2to80[4];
	double igtfrac80to500[4];


	std::cout << frac2to80[2]/frac2to80[0] << std::endl;
	std::cout << frac80to500[2]/frac80to500[0] << std::endl;
	std::cout << frac80to500[0]/frac2to80[0] << std::endl;
	std::cout << frac80to500[2]/frac2to80[2] << std::endl;
	std::cout << std::endl << igtfrac2to80[2]/igtfrac2to80[0] << std::endl;
	std::cout << igtfrac80to500[2]/igtfrac80to500[0] << std::endl;
	std::cout << igtfrac80to500[0]/igtfrac2to80[0] << std::endl;
	std::cout << igtfrac80to500[2]/igtfrac2to80[2] << std::endl;
	//std::cout << "new 777: 2-80 = " << frac2to80[2] << " 80-500 = " << frac80to500[2] << " events = " << numevents[2] << std::endl;
	//std::cout << "new 405: 2-80 = " << frac2to80[3] << " 80-500 = " << frac80to500[3] << " events = " << numevents[3] << std::endl;

	ofstream oFile;
	sprintf(savename, "/home/deap/nEXO/TimeDist Fits/%d.txt", aType);
	oFile.open(savename);
	oFile << "Parameters: " << f1->GetParameter(0) << ", " << f1->GetParameter(1) << ", " << f1->GetParameter(2) << ", " << f1->GetParameter(3) << ", " << f1->GetParameter(4) << "; Chi Square: " << f1->GetChisquare() << std::endl;
	oFile.close();
*/

}
