#include<fstream>
#include<stdio.h>
#include <cmath>


// --- Map the ntpule content
struct NtpCont{
	float evt; 
	float blmu; 
	float np; 
	float pa; 
	float pt; 
	float nfp; 
	float fa; 
	float ft; 
	float frt; 
	float fft; 
	float fblmu; 
	float chi2; 
	float ndf; 
	float chi2r; 
	float ndfr;
};

// --- Produces the time distribution
// >>> Only keep events with 1 pulse
TH1D* gHSingleTime;
TH1D* gHDTime;
int nBin;
double LaserTime; // ns (roughly)
double minTime;
double maxTime;
char HName[50];
double dnsum;

double FitFunction(double* x, double* p){
	double val = p[0]*TMath::Exp(p[1]/2*(p[1]*p[3]*p[3]-2*p[4]*(x[0]-p[2])))*TMath::Erfc((p[1]*p[3]*p[3]-p[4]*(x[0]-p[2]))/(sqrt(2.0)*p[3]));
	return val;
}

void calcTimeDist(int aType, double cut){
	nBin=20000;
	LaserTime=50; // ns (roughly)
	minTime=-1000;
	maxTime=1000;
	shift = 54.2;
	sprintf(HName,"HSingleTime%i",aType);
	gHSingleTime = (TH1D*) gROOT->FindObjectAny(HName);
	if(gHSingleTime) gHSingleTime->Reset(); //if option "ICE" is specified, resets only Integral, Contents and Errors.
	else gHSingleTime = new TH1D(HName,HName,nBin,minTime,maxTime);

	if((aType==102)||(aType==103)){
		shift = 53.9;
	}
	else if((aType==104)||(aType==105)){
		shift = 54.4;
	}
	else if((aType==106)||(aType==107)){
		shift = 53.7;
	}
	else {
		shift = 54.0;
	}

//Looks up run number in RunInfo.txt, finds associated file name and formats it.
	char FileNames [100] [2] [100];
	char activename [200];
	ifstream iFile;
	iFile.open("/home/deap/nEXO/RunInfo.txt");
	int counter = 0;
	char buffer [10000];
	for (int i = 0; i < 10; i++){
		iFile >> buffer;
	}
	while(!iFile.eof()){
		iFile >> FileNames[counter][0];
		iFile >> FileNames[counter][1];
		counter++;
		for (int i = 0; i < 8; i++){
			iFile >> buffer;
		}
	}
	for (int i = 0; i < counter; i++){
		int runnum;
		sscanf(FileNames[i][0], "%d", &runnum);
		if((runnum == aType)){
			char* newbuff;
			newbuff = FileNames[i][1];
			cout << newbuff << endl;
			sprintf(activename, "/home/deap/nEXO/%s.anat0", newbuff);
			cout << activename << endl;		
			break;
		}
		if((i == counter - 1)){
			cout << "No file found" << endl;
		}
	}

	TFile* fIn = (TFile*) gROOT->GetListOfFiles()->FindObject(activename);
	if(!fIn) fIn = new TFile(activename);
	TNtuple* ntp = (TNtuple*) fIn->Get("ntp");
	NtpCont* cont = (NtpCont*) ntp->GetArgs();
    int nPulse = ntp->GetEntries(); //get number of pulses
	int iPulse=0;
    while(iPulse<nPulse){ //iterate through pulses
		ntp->GetEntry(iPulse); //reads current entry and returns total number of bytes read
		if(cont->nfp==1 && cont->fa<-0.01){// one pulse (can be >1PE due to xtalk)
			gHSingleTime->Fill(cont->ft*1e9-shift);
		}
		if(iPulse%1000==0) cout << iPulse << " " << cont->nfp <<  endl;
		iPulse++;		
	}
	
	double sum = 0;
	TAxis* Xaxis = gHSingleTime->GetXaxis();
	for(int iBin = 0; iBin < gHSingleTime->GetNbinsX(); iBin++){
		sum += gHSingleTime->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	dnsum = 0;
	int counter = 0;
	double average = 0;
	for(int iBin = 0; iBin < gHSingleTime->GetNbinsX(); iBin++){
		if((gHSingleTime->GetBinCenter(iBin) < 1)){
			gHSingleTime->SetBinContent(iBin, gHSingleTime->GetBinContent(iBin)/sum);
		}
		else if ((gHSingleTime->GetBinCenter(iBin) < 5)){
			if((iBin%2 != 0)){
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);
				average = 0;
				counter = 0;
			}
		}
		else if ((gHSingleTime->GetBinCenter(iBin) < 10)){
			if((iBin%20 != 0)){
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);
				average = 0;
				counter = 0;
			}
		}
		else if ((gHSingleTime->GetBinCenter(iBin) < 50)){
			if((iBin%30 != 0)){
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);
				average = 0;
				counter = 0;
			}
		}
		else if ((gHSingleTime->GetBinCenter(iBin) < 100)){
			if((iBin%50 != 0)){
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);
				average = 0;
				counter = 0;
			}
		}
		else if ((gHSingleTime->GetBinCenter(iBin) < 500)){
			if((iBin%100 != 0)){
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);
				average = 0;
				counter = 0;
			}
		}
		else{
			if((iBin%200 != 0)){
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, 0);
			}
			else{
				average = average*counter + gHSingleTime->GetBinContent(iBin);
				counter++;
				average /= counter;
				gHSingleTime->SetBinContent(iBin, average/sum);
				average = 0;
				counter = 0;
			}
		}
	}
	if((cut>0)){
		for(int iBin = gHSingleTime->FindBin(cut); iBin < gHSingleTime->GetNbinsX(); iBin++){
			gHSingleTime->SetBinContent(iBin, 0);
		}
	}
	else if((cut<0)){
		for(int iBin = 0; iBin < gHSingleTime->FindBin(-1*cut); iBin++){
			gHSingleTime->SetBinContent(iBin, 0);
		}
	}
	for(int iBin = gHSingleTime->FindBin(-205); iBin < gHSingleTime->FindBin(-5); iBin++){
		dnsum += gHSingleTime->GetBinContent(iBin);
	}

	double dnrate = dnsum/200;
	for(int iBin = 0; iBin < gHSingleTime->GetNbinsX(); iBin++){
		if((gHSingleTime->GetBinContent(iBin) < dnrate)){
			gHSingleTime->SetBinContent(iBin, 0);
		}
		else{
			gHSingleTime->SetBinContent(iBin, gHSingleTime->GetBinContent(iBin)-dnrate);
		}
	}
	
}

void TimeDist_v5(int aType, int bType, double cut1, int cType, int dType, double cut2){
	TCanvas* c1 = new TCanvas("c1", "Time Distribution", 900, 600);
	c1->SetLogy();
	c1->SetLogx();

	//aType corresponds with run number in RunInfo.txt file
	calcTimeDist(aType, -1*cut1);
	TH1D* h1 = gHSingleTime;

	calcTimeDist(bType, cut1);
	TH1D* h2 = gHSingleTime;

	TH1D* h3 = new TH1D("h3","h3",nBin,minTime,maxTime);

	double sum = 0;
	TAxis* Xaxis = h3->GetXaxis();
	for(int iBin = 0; iBin < h3->GetNbinsX(); iBin++){
		if((h3->GetBinCenter(iBin) < cut1)){
			h3->SetBinContent(iBin, h2->GetBinContent(iBin));
		}
		else{
			h3->SetBinContent(iBin, h1->GetBinContent(iBin));
		}
		sum += h3->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	for(int iBin = 0; iBin < h3->GetNbinsX(); iBin++){
		h3->SetBinContent(iBin, h3->GetBinContent(iBin)/sum);
	}
	h3->SetBit(TH1::kNoTitle); 
	h3->SetMarkerSize(0.8);
	h3->SetMarkerStyle(4);
	h3->SetMarkerColor(2);
	h3->SetMaximum(5);
	h3->GetXaxis()->SetRangeUser(0.1, 1000);
	h3->GetXaxis()->SetTitle("Adjusted Time (ns)");
	h3->GetYaxis()->SetTitle("Probability Distribution Function (ns^{-1})");
	h3->GetXaxis()->CenterTitle();
	h3->GetYaxis()->CenterTitle();
	h3->GetXaxis()->SetTitleFont();
	h3->GetXaxis()->SetTitleSize();
	h3->GetXaxis()->SetTitleOffset(1.1);
	h3->GetXaxis()->SetLabelFont();
	h3->GetXaxis()->SetLabelSize();
	h3->GetYaxis()->SetTitleFont();
	h3->GetYaxis()->SetTitleSize();
	h3->GetYaxis()->SetLabelFont();
	h3->GetYaxis()->SetLabelSize();
	h3->SetStats(0);
	h3->Draw("P9");



	calcTimeDist(cType, -1*cut2);
	TH1D* h4 = gHSingleTime;

	calcTimeDist(dType, cut2);
	TH1D* h5 = gHSingleTime;

	TH1D* h6 = new TH1D("h6","h6",nBin,minTime,maxTime);

	double sum = 0;
	TAxis* Xaxis = h6->GetXaxis();
	for(int iBin = 0; iBin < h6->GetNbinsX(); iBin++){
		if((h6->GetBinCenter(iBin) < cut2)){
			h6->SetBinContent(iBin, h5->GetBinContent(iBin));
		}
		else{
			h6->SetBinContent(iBin, h4->GetBinContent(iBin));
		}
		sum += h6->GetBinContent(iBin) * Xaxis->GetBinWidth(iBin);
	}
	for(int iBin = 0; iBin < h6->GetNbinsX(); iBin++){
		h6->SetBinContent(iBin, h6->GetBinContent(iBin)/sum);
	}
	h6->SetMarkerSize(0.8);
	h6->SetMarkerStyle(25);
	h6->SetMarkerColor(4);
	h6->SetMaximum(5);
	h6->GetXaxis()->SetRangeUser(0.1, 1000);
	h6->Draw("same P9");
	
    leg = new TLegend(0.65,0.75,0.9,0.9);
    


	char savename [100];
	if(aType==102 && cType==104){
		sprintf(savename, "/home/deap/nEXO/TimeDist Fits/old.pdf");
		leg->AddEntry("h3","T2K MPPC @ 777nm","p");
    	leg->AddEntry("h6","T2K MPPC @ 405nm","p");
	}
	else if(aType==102 && cType==106){
		sprintf(savename, "/home/deap/nEXO/TimeDist Fits/777nm.pdf");
		leg->AddEntry("h3","T2K MPPC @ 777nm","p");
    	leg->AddEntry("h6","2013 MPPC @ 777nm","p");
	}
	else if(aType==106 && cType==108){
		sprintf(savename, "/home/deap/nEXO/TimeDist Fits/new.pdf");
		leg->AddEntry("h3","2013 MPPC @ 777nm","p");
    	leg->AddEntry("h6","2013 MPPC @ 405nm","p");
	}
	else if(aType==104 && cType==108){
		sprintf(savename, "/home/deap/nEXO/TimeDist Fits/405nm.pdf");
		leg->AddEntry("h3","T2K MPPC @ 405nm","p");
    	leg->AddEntry("h6","2013 MPPC @ 405nm","p");
	}
	else{
		sprintf(savename, "/home/deap/nEXO/TimeDist Fits/nameerror.pdf");
	}

	leg->Draw();
	c1->SaveAs(savename);
/*
	ofstream oFile;
	sprintf(savename, "/home/deap/nEXO/TimeDist Fits/%d.txt", aType);
	oFile.open(savename);
	oFile << "Parameters: " << f1->GetParameter(0) << ", " << f1->GetParameter(1) << ", " << f1->GetParameter(2) << ", " << f1->GetParameter(3) << ", " << f1->GetParameter(4) << "; Chi Square: " << f1->GetChisquare() << std::endl;
	oFile.close();
*/

}
