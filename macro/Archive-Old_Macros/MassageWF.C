#include "code/Waveform.cxx"
#include "code/LecroyFile.cxx"

// Parameters
double WFAmpBining = 1.6e-3;
double AmpMin=0.;
double AmpMax=0.32;
int riseTime=15;//bins
int fallTime=1000;//bins
int negPolarity=1;


double FuncExpGaus(double* x, double*p){
 // p[0]: gaussian mu
  // p[1]: amplitude
  // p[2]: gaussian sig
  // p[3]: exponential decay constant
	// p[4]: baseline
  // convolution of an exponential and a gaussian, formula found in a
  // chromatography paper on exponentially modified gaussians.
  // http://www.springerlink.com/content/yx7554182g164612/
	double time = x[0]-p[0];
  return p[1]*exp(1/2*p[2]*p[2]/p[3]/p[3]-time/p[3])
    * (TMath::Erf(1/sqrt(2)*(p[0]/p[2]+p[2]/p[3]))
			 + TMath::Erf(1/sqrt(2)*(time/p[2]-p[2]/p[3])))+p[4];
}

double FuncExpGausMulti(double* x, double*p){
	// p[0]: baseline
	// p[1]: gaussian sig
  // p[2]: exponential decay constant
	// p[3]: number of pulses
	// p[4+2*i]: pulse[i] amplitude
	// p[5+2*i]: pulse[i] time
  // convolution of an exponential and a gaussian, formula found in a
  // chromatography paper on exponentially modified gaussians.
  // http://www.springerlink.com/content/yx7554182g164612/	
	double val=0.;
	for(int iPulse=0; iPulse<p[3]; iPulse++){
		double time(x[0]-p[5+2*iPulse]);
		val+=p[4+2*iPulse]*
			exp(1/2*p[1]*p[1]/p[2]/p[2]-time/p[2])*
			(TMath::Erf(1/sqrt(2)*(p[0]/p[1]+p[1]/p[2]))+
			 TMath::Erf(1/sqrt(2)*(time/p[1]-p[1]/p[2])));
	}
	return val;
}


void MassageWF(const char* aFileName, int aEvent){
	LecroyFile* dataFile = new LecroyFile(aFileName);
	char wfName[20];
	sprintf(wfName,"WFCh0Ev%i",aEvent);
	Waveform* wf = dataFile->getWaveform(wfName);
	wf->Draw();

	// --- Calculate baseline parameters
	TH1F* HBaseline = (TH1F*) gROOT->FindObjectAny("HBaseline");
	if(HBaseline) HBaseline->Reset("C");
	else{
		HBaseline = new TH1F("HBaseline","HBaseline",
												 (int)((AmpMax-AmpMin)/WFAmpBining),AmpMin,AmpMax);
	}
	for(int iBin=1; iBin<=wf->GetNbinsX(); iBin++){
		HBaseline->Fill(wf->GetBinContent(iBin));
	}
	int blMostProbBin=0;
	int blMostProbCont=0;
	for(int iBin=1; iBin<=HBaseline->GetNbinsX(); iBin++){
		if(HBaseline->GetBinContent(iBin)>blMostProbCont){
			blMostProbCont=HBaseline->GetBinContent(iBin);
			blMostProbBin=iBin;
		}				
	}
	double blMu=HBaseline->GetBinLowEdge(blMostProbBin);
	std::cout << "Baseline " << blMu << " " << 	blMostProbCont << std::endl;

	// --- Function for pulse fitting
	TF1* FExpGaus = (TF1*) gROOT->FindObjectAny("FExpGaus");
	if(FExpGaus) FExpGaus->Delete();
	FExpGaus = new TF1("FExpGaus",FuncExpGaus,0.,0.5e-6,5);

	// --- Look for pulses
	int nPulse=0;
	int nPulseMax=100;
	double tPulse[100];
	double aPulse[100];
	for(int iBin=riseTime+1; iBin<=wf->GetNbinsX(); iBin++){
		/*if(iBin> 1000 && iBin<1100) 
			std::cout << iBin << " " 
								<< wf->GetBinContent(iBin) << " "
								<< wf->GetBinCenter(iBin) << " "
								<< -(wf->GetBinContent(iBin)-wf->GetBinContent(iBin-riseTime))
								<< std::endl;
		*/
		if(-(wf->GetBinContent(iBin)-wf->GetBinContent(iBin-riseTime))>0.005){
			double amp=-wf->GetBinContent(iBin);
			iBin++;
			while(iBin<=wf->GetNbinsX() && 
						amp<-(wf->GetBinContent(iBin))){
				amp = -(wf->GetBinContent(iBin));
				iBin++;
			}
			iBin--;
			if(iBin<wf->GetNbinsX()){
				tPulse[nPulse] = wf->GetBinCenter(iBin);
				aPulse[nPulse] = wf->GetBinContent(iBin);				
				std::cout << "Pulse found " << nPulse << " " << iBin << " "
									<< tPulse[nPulse] << " " 
									<< aPulse[nPulse] << std::endl;

				// --- Fit pulse
				FExpGaus->SetRange(wf->GetBinCenter(iBin-riseTime-200),
													 wf->GetBinCenter(iBin+2*fallTime));
				FExpGaus->SetParameter(0,wf->GetBinCenter(iBin));
				FExpGaus->SetParameter(1,(wf->GetBinContent(iBin)-blMu)/2.);//not sure
				FExpGaus->SetParameter(2,0.57e-9);
				FExpGaus->SetParameter(3,21.6e-9);
				FExpGaus->FixParameter(4,blMu);
				wf->Fit("FExpGaus","R0");
				FExpGaus->DrawCopy("same");
				nPulse++;

			}
			iBin+=fallTime;
		}
	}
	if(nPulse>0){
		TGraph* GPulse = new TGraph(nPulse,tPulse,aPulse);
		GPulse->SetMarkerColor(2);
		GPulse->SetMarkerStyle(23);
		GPulse->Draw("psame");
	}



}
