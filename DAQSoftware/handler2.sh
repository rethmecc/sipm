#!/bin/bash

#Created by Lloyd James <lloyd.thomas.james@gmail.com>

#Input Paramters, obviously 1->true
recordtemp=0
tempsave="/data20/exo/temperature"
recordcurrent=1
currentsave="/data20/exo/keithley"
waveformsave="/data20/exo/scope"

#For dealing with keyboard interrupts
trap ctrl_c SIGINT SIGTERM
function ctrl_c(){
    if [[ "$recordtemp" -eq 1 ]]; then kill $PIDT; fi
    if [[ "$recordcurrent" -eq 1 ]]; then
	kill $PIDC
	kill $PIDC
    fi
    exit $?
}

#Arguments passed from python argument handler
filename=$1
nevents=$2
nruns=$3
vinit=$4
vfin=$5
vstep=$6
winit=$7
wfin=$8

#Begins to record temperature
if [[ "$recordtemp" -eq 1 ]]; then
    ./readtemp.sh "$tempsave/$filename" &
    PIDT=$!
fi

#Determines step size for scope window increase
nsteps="$(python -c "print ($vfin - $vinit)/$vstep" )"
nsteps=${nsteps/.*}
if [[ "$nsteps" -gt 1 ]]; then
    windowstep="$(python -c "print ($wfin - $winit)/$nsteps" )"
else
    windowstep=0
fi
windowsize=$winit

#Loops through operating voltages
voltage=$vinit
while [[ "$(python -c "print int(( $voltage <= $vfin ))" )" -eq 1 ]]; do

    #Sets the Keithley voltage
    #echo $voltage > input.txt
    #echo "output.txt" >> input.txt
    #./setvolt.exe
    #result=$(<output.txt)
    #if [[ "$result" -ne 1 ]]; then 
	#echo "Failed setting voltage"
	#kill $PIDT
	#exit 1
    #fi

    #Begins to record current measurements
    if [[ "$recordcurrent" -eq 1 ]]; then
	echo "$currentsave/$filename.txt" > input.txt
	./readcurrent.exe &
	PIDC=$!
    fi

    #Adjusts the scope window size and fetches waveforms
    #python sock.py "C1:volt_div $windowsizeV"
    #sleep 1
    #savename="$(python getfilename.py $filename $voltage)"
    python fetch.py $filename -n $nevents -r $nruns -s $waveformsave
    #windowsize="$(python -c "print $windowsize+$windowstep" )"

    #Kills the current recording
    if [[ "$recordcurrent" -eq 1 ]]; then
	kill $PIDC
	kill $PIDC
    fi
    voltage="$(python -c "print $voltage+$vstep" )"
done

#Kills the temperature recording
if [[ "$recordtemp" -eq 1 ]]; then kill $PIDT; fi