#!/bin/bash

#Created by Lloyd James <lloyd.thomas.james@gmail.com>

#Location of serial port connected to DAQ unit, can change if used usb port changes
export SERIAL="/dev/ttyUSB0"

#Handler for the kill command
trap ctrl_c SIGINT SIGTERM
function ctrl_c(){
    kill $PID
    exit $?
}

#Listens for output from serial and sticks it into a save file
if [ ! -z $1 ]; then
    file="$1.txt"
else
    file=tempdata.txt
fi
echo $file
cat $SERIAL > $file &
PID=$!
ps $PID

#Resets DAQ unit to factory settings
echo "*RST" > $SERIAL
sleep 1s

#Activates used channels to listen to temperature
echo "CONF:TEMP TC,J,(@302,303,304,305,308,310,314,317,319,320)" > $SERIAL
sleep 1s

#Calibrates each channel
echo "CALC:SCAL:GAIN 0.958859 ,(@302)" > $SERIAL
echo "CALC:SCAL:OFFS 0.359456 ,(@302)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@302)" > $SERIAL
echo "CALC:SCAL:GAIN 0.973605 ,(@303)" > $SERIAL
echo "CALC:SCAL:OFFS -0.183147 ,(@303)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@303)" > $SERIAL
echo "CALC:SCAL:GAIN 1.01568 ,(@304)" > $SERIAL
echo "CALC:SCAL:OFFS -0.581754 ,(@304)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@304)" > $SERIAL
echo "CALC:SCAL:GAIN 0.964719 ,(@305)" > $SERIAL
echo "CALC:SCAL:OFFS -0.0650253 ,(@305)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@305)" > $SERIAL
echo "CALC:SCAL:GAIN 0.990527 ,(@308)" > $SERIAL
echo "CALC:SCAL:OFFS -0.519964 ,(@308)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@308)" > $SERIAL
echo "CALC:SCAL:GAIN 0.953191 ,(@310)" > $SERIAL
echo "CALC:SCAL:OFFS 0.090041 ,(@310)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@310)" > $SERIAL
echo "CALC:SCAL:GAIN 0.994234 ,(@314)" > $SERIAL
echo "CALC:SCAL:OFFS -0.227023 ,(@314)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@314)" > $SERIAL
echo "CALC:SCAL:GAIN 0.999138 ,(@317)" > $SERIAL
echo "CALC:SCAL:OFFS -0.397036 ,(@317)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@317)" > $SERIAL
echo "CALC:SCAL:GAIN 0.979384 ,(@319)" > $SERIAL
echo "CALC:SCAL:OFFS 0.2885200 ,(@319)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@319)" > $SERIAL
echo "CALC:SCAL:GAIN 0.965976 ,(@320)" > $SERIAL
echo "CALC:SCAL:OFFS 0.7019230 ,(@320)" > $SERIAL
echo "CALC:SCAL:STAT 1 ,(@320)" > $SERIAL
sleep 1

#Sets up temperature scan of channels, making 1 temp reading per scan
echo "ROUT:SCAN (@302,303,304,305,308,310,314,317,319,320)" > $SERIAL
echo "TRIG:COUN 1" > $SERIAL

#Every ~1s retreives a time stamp and a temperature scan from the unit
while [ True ]; do
    echo "SYST:TIME?" > $SERIAL
    sleep 0.1s
    echo "INIT" > $SERIAL
    sleep 0.7s
    echo "FETC?" > $SERIAL
    sleep 0.2s
done
