/********************************************************************\
  Name:         scgpib.cxx
  Created by:   PAA
  Edited  by:   Lennart Huth <huth@physi.uni-heidelberg.de>
                Lloyd James <lloyd.thomas.james@gmail.com>
  $Id$
                GPIB through MSCB
\********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <math.h>
#include "midas.h"
#include "mscb.h"
#include "OdbGpib.h"
#include <signal.h>
#include <stdint.h>
#include<iostream>
#include<time.h>

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  /*-- Globals -------------------------------------------------------*/

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char const *frontend_name = "scGPIB";
  /* The frontend file name, don't change it */
  char const *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = TRUE; // default = TRUE No effect visible

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 0000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 10000;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */  
  INT max_event_size_frag = 5 * 1024 * 1024;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 10000;
  char const mydevname[] = {"GPIB410"};
  HNDLE hDB, hDD, hSet, hVar;
  int lmscb_fd;
  
  /* savefile */
  std::ifstream iFile;
  std::ofstream oFile;

  //Params
  float voltage;
  
  /*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
 
  INT read_trigger_event(char *pevent, INT off);
  INT read_scaler_event(char *pevent, INT off);
  INT read_mscb_event(char *pevent, INT off);
  INT localmscb_init(char const *eqname);

  INT set_voltage(float value, bool tout);
  float read_voltage(bool tout);

 /*-- Equipment list ------------------------------------------------*/

  EQUIPMENT equipment[] = {
    {"current",               /* equipment name */
     {15, 0,                   /* event ID, trigger mask */
      "SYSTEM",                /* event buffer */
      EQ_PERIODIC,             /* equipment type */
      0,                       /* event source */
      "MIDAS",                 /* format */
      TRUE,                    /* enabled */
      RO_ALWAYS |   /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      10000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", "",},
     read_mscb_event,       /* readout routine */
    },

    {""}
  };

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int status, size;
  char str[64];

  set_equipment_status("scgpib", "Initializing...", "#00FF00");  
  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */
  status = localmscb_init("current");
  if (status != FE_SUCCESS) {
    cm_msg(MERROR,"scgpib","Access to mscb failed [%d]", status);
    return status;
  }
  
  // Get the hardware information
  sprintf (str, "%s", "*IDN?");
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 6);
  ss_sleep(1000);
  size = sizeof(str);
  status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);
  printf("Device: %s\n", str);

  ss_sleep(1000);
  sprintf(str,"%s","CONF:CURR");
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 10);
  
  set_equipment_status("scgpib", "Ok", "#00FF00");  
  
  //Get input stuff:
  iFile.open("input.txt");
  char outname [100];
  char buffer [50];
  iFile >> buffer;
  voltage = atof(buffer);
  iFile >> outname;

  // open datafile to print in:
  oFile.open(outname);
  return SUCCESS;
}

/*-- MSCB event --------------------------------------------------*/
INT read_mscb_event(char *pevent, INT off)
{
  int   status, size;
  float fvalue[N_GPIB], *pfdata;
  char  str[64];
  bool correctvoltage;
  int nattempts;

  bk_init(pevent);
  size = sizeof(str);

  //Repeatedly sets the voltage and then measures the new voltage.                                      
  correctvoltage = false;
  nattempts = 0;
  while(!correctvoltage){
    status = set_voltage(voltage, true);
    float read_volt = read_voltage(true);

    //Breaks if measured voltage is within float precision of desired one                               
    if((fabs(read_volt - voltage)<0.006))
      {
	correctvoltage = true;
	oFile << 1;
      }
    else nattempts++;

    //Writes an error signal to file if it cannot set the voltage in 5 attempts                        
    if(nattempts == 5){
      printf("Error, could not set voltage!\n");
      oFile << 0;
      break;
    }
  }

  //Kills DAQ loop once this is completed                                                               
  raise(SIGINT);

  if (status != MSCB_SUCCESS) {
    cm_msg(MINFO,"scgpib","mscb_read failed [%d] on %d-ch%d"
           , status, gpib_settings.dd.base_address, 6);
    set_equipment_status("scgpib", "mscb_read error", "#daa520");
  }
  set_equipment_status("scgpib", str, "#00FF00");
  return bk_size(pevent);
}

/*-- Set Voltage -------------------------------------------------------*/
INT set_voltage(float value, bool tout)
{
  int status;
  char  str[64];

  ss_sleep(1000);
  sprintf(str,"%s %g","SOUR:VOLT", value);
  if(tout)
    printf("Setting Voltage: %g\n",value);
  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 17);
  ss_sleep(1000);

  return status;
}

/*-- Read Voltage ------------------------------------------------------*/
float read_voltage(bool tout)
{
  int size, status;
  char  str[64];

  ss_sleep(1000);
  sprintf(str,"%s","SOUR:VOLT?");

  status = mscb_write(lmscb_fd, gpib_settings.dd.base_address, 5, str, 13);
  ss_sleep(1000);;

  size = sizeof(str);
  status = mscb_read(lmscb_fd, gpib_settings.dd.base_address, 6, &(str[0]), &size);

  ss_sleep(500);
  if(tout)
    printf("Applied voltage: %s \n",str);
  return (atof(str));

}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  oFile.close();
  iFile.close();
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  set_equipment_status("scgpib", "Stopped...", "#00FF00");  
  printf("Stopped Frontend!\n");
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  /* put here clear scalers etc. */
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events
/*-- Trigger event routines ----------------------------------------*/

extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int i;
  DWORD lam;
  
  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
	return lam;
  }
  return 0;
}

/*-- Local MSCB event --------------------------------------------------*/
INT localmscb_init(char const *eqname)
{
  int  status, size;
  MSCB_INFO node_info;
  char set_str[80];
  GPIB_SETTINGS_STR(gpib_settings_str);
  
  cm_get_experiment_database(&hDB, NULL);
  
  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/%s/Settings", eqname);
  
  /* create PARAM settings record */
  status = db_create_record(hDB, 0, set_str, strcomb(gpib_settings_str));
  if (status != DB_SUCCESS)  return FE_ERR_ODB;
  
  status = db_find_key(hDB, 0, set_str, &hSet);
  status = db_find_key(hDB, hSet, "DD", &hDD);
  if (status == DB_SUCCESS) {
    size = sizeof(gpib_settings.dd);
    db_get_record(hDB, hDD, &(gpib_settings.dd), &size, 0);
    
    /* Open device on MSCB */
    lmscb_fd = mscb_init(gpib_settings.dd.mscb_device, sizeof(gpib_settings.dd.mscb_device)
			 , gpib_settings.dd.mscb_pwd, TRUE);
    printf("lmscb_fd: %d \n",lmscb_fd);
    if (lmscb_fd < 0) {
      cm_msg(MERROR, "mscb_init",
	     "Cannot access MSCB submaster at \"%s\". Check power and connection.",
	     gpib_settings.dd.mscb_device);
      return FE_ERR_HW;
    }
    
    // check if devices is alive 
#if 0
    status = mscb_ping(lmscb_fd, (unsigned short) gpib_settings.dd.base_address, 1);
    if (status != FE_SUCCESS) {
      cm_msg(MERROR, "mscb_init",
	     "Cannot ping MSCB address %d. Check power and connection.", gpib_settings.dd.base_address);
    }
#endif
    
    // Check for right device
    status = mscb_info(lmscb_fd, (unsigned short) gpib_settings.dd.base_address, &node_info);
    if (strcmp(node_info.node_name, mydevname) != 0) {
      cm_msg(MERROR, "mscb_init",
	     "Found one expected node \"%s\" at address \"%d\"."
	     , node_info.node_name, gpib_settings.dd.base_address);
      return FE_ERR_HW;
    }
    printf("device %s found\n", node_info.node_name);
  }
  return FE_SUCCESS;
}

