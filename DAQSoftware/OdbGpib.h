
#ifndef _SC_GPIB_
#define _SC_GPIB_

#define N_GPIB 1
#define GPIB_SETTINGS_STR(_name) char const *_name[] = {\
    "[DD]",\
    "MSCB Device = STRING : [32] mscb502",\
    "MSCB Pwd = STRING : [32] ",\
    "Base Address = INT : 1",\
    "",\
    "[READ_PAR]",\
    "voltage = FLOAT :0",\
    "current = FLOAT :0",\
    "",\
    "[WRITE_PAR]",\
    "voltage = FLOAT :0",\
    "current_limit = FLOAT :0",\
    "",\
    "[.]",\
    "Names = STRING[2] :",\
    "[32] Voltage",\
    "[32] Current",\
    "",\
    NULL }

typedef struct {
  struct {
    char      mscb_device[32];
    char      mscb_pwd[32];
    INT       base_address;
  } dd;
  char      names[2][32];
  struct {
    float voltage;
    float current;
  } read_par;
  struct {
    float voltage;
    float current_limit;
  } write_par;
  
  
} GPIB_SETTINGS;

GPIB_SETTINGS gpib_settings;

#endif
