import optparse
from subprocess import call

#Parses command line arguments
parser = optparse.OptionParser(description='usage: voltscanfetch.py <filename> [-n] [-r] [-i] [-f] [-s] [-I] [-F]')
parser.add_option('-n', type=int, dest='nevents', default=1000,
                                                help='number of events to store per run')
parser.add_option('-r', type=int, dest='nruns', default=1,
                                                help='number of runs')
parser.add_option('-i', type=float, dest='vinit', default=1.0,
                                                help='initial voltage for keithley')
parser.add_option('-f', type=float, dest='vfin', default=-1.0,
                                                help='final voltage for keithley')
parser.add_option('-s', type=float, dest='step', default=0.5,
                                                help='voltage step size')
parser.add_option('-I', type=float, dest='winit', default=1.0,
                                                help='initial V/div for scope')
parser.add_option('-F', type=float, dest='wfin', default=3.0,
                                                help='final V/div for scope')

(options, args) = parser.parse_args()

#If no explicit final voltage is supplied, only does the initial voltage
if options.vfin == -1.0:
    options.vfin = options.vinit

#Calls handler shell script to do the DAQ
call(["./handler.sh", args[0], str(options.nevents), str(options.nruns), str(options.vinit), str(options.vfin), str(options.step), str(options.winit), str(options.wfin)])
