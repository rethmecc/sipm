import sys

voltage = float(sys.argv[2])

if sys.argv[1][-4:] == 'hdf5':
        fetchname = sys.argv[1][:-5] + '_' + \
        str(voltage)[:str(voltage).index('.')] + '-' + \
        str(voltage)[str(voltage).index('.') + 1:str(voltage).index('.') + 3] + \
        '.hdf5'
else:
        fetchname = sys.argv[1] + '_' + \
        str(voltage)[:str(voltage).index('.')] + '-' + \
        str(voltage)[str(voltage).index('.') + 1:str(voltage).index('.') + 3] + \
        '.hdf5'

print fetchname
