//Carl Rethmeier; June-July 2015; rethmeier.carl@gmail.com

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <math.h>
#include <time.h>
#include "LecroyFile.h"
#include "WaveformProcessor.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"
#include "TPad.h"

int main(int argc,char* argv[]){
  int run=0;
  int numEvents=-1;
  int channel=1;
  int verbosity=0;
  double level=3.5;
  double window=3000;
  bool trunc=false;
  int darkStart=900;
  int lightStart=4400;
  double cut=-0.002;
  double gCut=0.01;
  double gRange=0.006;
  int smooth=0;
  int nBins1PE=6;
  char saveNameDef[20]="NormGain.txt";
  char* saveName=saveNameDef;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "r:n:c:hv:l:w:b:a:f:tp:g:j:s:d:")) != -1)
   switch (c)
     {
     case 'r':  //Run#
       run = atoi(optarg);
       break;
     case 'n':  //Number of events
       numEvents = atoi(optarg);
       break;
     case 'c':  //Channel#
       channel = atoi(optarg);
       break;
     case 'f':  //Savefile name
       saveName = optarg;
       break;
     case 'l':  //Threshold level in # of sigma
       level = atof(optarg);
       break;
     case 'w':  //Window length
       window = atoi(optarg);
       break;
     case 't':  //Window length
       trunc = true;
       break;
     case 'v':  //amount of text displayed
       verbosity = atoi(optarg);
       break;
     case 'b':  //Start of dark region
       darkStart = atoi(optarg);
       break;
     case 'a':  //Start of light region
       lightStart = atoi(optarg);
       break;
     case 'p':  //Maximum height of "zero"
       cut = atof(optarg);
       break;
     case 'g':  //Minumum 1PE pulse height
       gCut = atof(optarg);
       break;
     case 'j':  //Upper and lower bound of 1PE fit
       gRange = atof(optarg);
       break;
     case 's':  //Smoothing range
       smooth = atof(optarg);
       break;
     case 'd':  //1PE peak nbins
       nBins1PE = atof(optarg);
       break;
     case 'h':  //Print usage
       std::cout << "Usage: " << argv[0] << " [parameters]... [options]...\n"
                 << "This application measures the gain of a detector\n" 
                 << "It uses a gaussian fit to find the amplitude of the 1PE pulses\n"
                 << "\nParameters and options:\n"
                 << "-r run#          Run number in \'LampRunInfo.txt\'; default=0\n"
                 << "-n numEvents     Number of waveforms to be processed, starting from 0. -1 for all; default=-11\n"
                 << "-c channel#      Oscilloscope channel #; default=1\n"
                 << "-f fileName      Name of ASCII file in which resukts will be saved; default=\"PEWF.txt\"\n"
                 << "-l level         Minimum peak height in baseline RMS; default=8\n"
                 << "-w windowLength  Length of window to check for zeros in bins; default=3000\n"
                 << "-b beforeLight   Start of region before lamp activation in bins; default=900\n"
                 << "-a afterLight    Start of region after lamp activation in bins; default=4400\n"
                 << "-s smoothLength  Perform boxcar waveform smoothing with given radius\n"
                 << "-p peCut         Threshold level for counting zeros for <PE>; default=-0.002\n"
                 << "-g gainCut       Lower threshold for 1PE level for gain calculation; default=0.01\n"
                 << "-j jitter        +- for 1PE peak fit; default=0.006\n"
                 << "-d densityPE     Number of bins used in 1PE peak fit; default=6\n"
                 << "-v verbosity     Verbosity level (0,1,2,3,4,5); default=0\n"
                 << "-t               Flag to truncate savefile contents\n"
                 << "-h               Display this usage info\n";
       exit(0);
       break;
     case '?':
       if (optopt == 'r' || optopt == 'n' || optopt == 'c' || optopt == 'l' || optopt == 'w' || optopt == 'v' || optopt == 'b' || optopt == 'f' || optopt == 'a' || optopt == 'p' || optopt == 'g' || optopt == 'j' || optopt == 's' || optopt == 'd')
         fprintf (stderr, "Option -%c requires an argument.\n", optopt);
       else if (isprint (optopt))
         fprintf (stderr, "Unknown option `-%c'.\n", optopt);
       else
         fprintf (stderr,
                  "Unknown option character `\\x%x'.\n",
                  optopt);
     return 1;
     default:
     abort ();
     }
  
  std::ofstream fout;
  float dark=0;
  float light=0;
  double nBins;
  double secPerBin;
  double temp=0;
  int percent=0;
  double gain;
  double sigma;
  double maxLoc;
  double PE;
  
  char name[17];  
  if (channel==1)
    sprintf(name,"proc/gainHist%i.root",run);
  else
    sprintf(name,"proc/gainHist%i.%i.root",run,channel);
  
  TFile* saveHist = new TFile(name,"RECREATE");
  TNtuple* ntp = new TNtuple("ntp","ntp","W#:time:height:charge:width:trigTime");
  TNtuple* minHistDark = new TNtuple("minHistDark","minHistDark","W#:minHeight");
  TNtuple* minHistLight = new TNtuple("minHistLight","minHistLight","W#:minHeight");
  
  if (trunc){
    fout.open(saveName,std::ios::trunc);
    fout << "run  gain  sigma  dark  light  <PE>\n";
  }
  else
    fout.open(saveName,std::ios::app);
  
  WaveformProcessor* wfProc = new WaveformProcessor("LampRunInfo.txt",run,channel,-1);
  wfProc->readWaveform(0);
  nBins=wfProc->getCurrentWaveform()->GetNbinsX();
  secPerBin=wfProc->getCurrentWaveform()->GetBinWidth(0);
  if (numEvents>wfProc->getWaveformCount() || numEvents<1) numEvents=wfProc->getWaveformCount();
  
  for (int i=0;i<numEvents;i++){
    if (verbosity==0 && (i+1)*100./numEvents>=(percent+1)){
      percent=(i+1)*100./numEvents;
      std::cout << std::string(4,'\b');
      std::cout << percent << "%" << std::flush;
    }
    
    if (verbosity>0){
      std::cout << std::string(25,'\b');
      std::cout << "Processing WF " << i;
    }
    wfProc->readWaveform(i);
//     if (smooth)
      wfProc->smoothWaveform(smooth);
    wfProc->processBaseline();
    wfProc->findClearPulses(ntp,darkStart,lightStart+window,level,verbosity-1);
    minHistDark->Fill(i,wfProc->findMin(darkStart,darkStart+window,verbosity));
    minHistLight->Fill(i,wfProc->findMin(lightStart,lightStart+window,verbosity));
  }
  
  std::cout << std::endl;
  
  ntp->Draw("height");
  TH1F *tempHist = (TH1F*)gPad->GetPrimitive("htemp")->Clone("tempHist");
  //Calculate gain---------
//   TH1D* tempHist = new TH1D("tempHist","tempHist",100,gCut,gCut*3.5);
//   char condition0[50];
//   sprintf(condition0,"height>%f && height<%f",gCut,gCut*3.5);
//   ntp->Project("tempHist","height",condition0);
  maxLoc=tempHist->GetBinCenter(tempHist->GetMaximumBin());
  char condition[50];
  sprintf(condition,"height>%f && height<%f",maxLoc-gRange,maxLoc+gRange);
  TH1D* PE1 = new TH1D("PE1","PE1",nBins1PE,maxLoc-gRange,maxLoc+gRange);
  ntp->Project("PE1","height",condition);
  TF1* fit = new TF1("fit","gaus");
  PE1->Fit(fit);
  gain=fit->GetParameter(1);
  sigma=fit->GetParameter(2);
  
  //Calculate <PE>
  TH1D* tempHistDark = new TH1D("tempHistDark","tempHistDark",100,cut,0.0015);
  TH1D* tempHistLight = new TH1D("tempHistLight","tempHistLight",100,cut,0.0015);
  char condition2[50];
  sprintf(condition2,"minHeight>%f",cut);
  minHistDark->Project("tempHistDark","minHeight",condition2);
  minHistLight->Project("tempHistLight","minHeight",condition2);
  dark=tempHistDark->GetEntries();
  light=tempHistLight->GetEntries();
  PE=log(dark/light);
  
  std::cout << "gain=" << gain << "  sigma=" << sigma << "  dark=" << dark << "  light=" << light << "  <PE>=" << PE << std::endl;
  fout << run << "  " << gain << "  " << sigma << "  " << dark << "  " << light << "  " << PE << std::endl;

  delete wfProc;
  
  saveHist->cd();
  
  ntp->Write();
  PE1->Write();
  tempHist->Write();
  tempHistDark->Write();
  tempHistLight->Write();
  
  minHistDark->Write();
  minHistLight->Write();
  saveHist->Close();
  
  return 0;
}
