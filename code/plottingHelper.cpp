/**
 *  Collection of Functions for quick data plotting
 *
 */
double FuncExpGausMulti2(double* x, double*p){
    // p[0]: baseline
    // p[1]: gaussian sig
    // p[2]: first  exponential decay constant
    // p[3]: second exponential decay constant
    // p[4]: peak ratio
    // p[5]: activate second TC set to 1 for use both TCs, 0 otherwise
    // p[6]: number of pulses
    // p[7+2*i]: pulse[i] amplitude
    // p[8+2*i]: pulse[i] time
    double val=p[0];
    if(p[5]==1)
    {
        for(int iPulse=0; iPulse<p[6]; iPulse++){
            double time = x[0]-p[8+2*iPulse];
            if(((p[1]*p[1]/p[2]/2.-time)/p[2])<700){ //otherwise exponential explodes
                val+=p[4]*p[7+2*iPulse]/2.*
                        exp((p[1]*p[1]/p[2]/2.-time)/p[2])*
                        TMath::Erfc((p[1]*p[1]/p[2]-time)/sqrt(2)/p[1]);
            }
            if(((p[1]*p[1]/p[3]/2.-time)/p[3])<700){ //otherwise exponential explodes
                val+=(1-p[4])*p[7+2*iPulse]/2.*
                        exp((p[1]*p[1]/p[3]/2.-time)/p[3])*
                        TMath::Erfc((p[1]*p[1]/p[3]-time)/sqrt(2)/p[1]);
            }
        }
    }
    else if(p[5]== 0)
    {
        for(int iPulse=0; iPulse<p[6]; iPulse++){
            double time = x[0]-p[8+2*iPulse];
            if(((p[1]*p[1]/p[2]/2.-time)/p[2])<700){ //otherwise exponential explodes
                val+=p[7+2*iPulse]/2.*
                        exp((p[1]*p[1]/p[2]/2.-time)/p[2])*
                        TMath::Erfc((p[1]*p[1]/p[2]-time)/sqrt(2)/p[1]);
            }
        }
    }
    else{std::cout << "SOMETHINGS WRONG!" <<p[5]<<std::endl;}
    return val;
}


void plot_test()
{
    c1 = new TCanvas("c1","test",200,10,700,500);
    TF1 f("f",FuncExpGausMulti2,-1e-6,1e-6,9);
    f.SetParameter(0,1);
    f.SetParameter(1,2e-9);
    f.SetParameter(2,10e-9);
    f.SetParameter(3,900e-9);
    f.SetParameter(4,0);
    f.SetParameter(5,0);
    f.SetParameter(6,1);
    f.SetParameter(7,-0.03);
    f.SetParameter(8,1e-9);
    f.Draw("");

}

void PlotGain_Ketek()
{
    const int n = 7;
    c1 = new TCanvas("c1","Ketek Dark Noise Rate",200,10,700,500);
    Double_t x[n]  = {25.92-24.4344, 26.42-24.4344, 26.92-24.4344, 27.42-24.4344, 27.92-24.4344, 28.42-24.4344, 28.92-24.4344};
    Double_t y[n]  = {1.41961e-02, 1.88979e-02, 2.36382e-02, 2.83682e-02, 3.31876e-02, 3.79730e-02, 4.27282e-02};
    Double_t ex[n] = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t ey[n] = {2.71680e-06, 2.67708e-06, 2.79730e-06, 2.97736e-06, 3.14046e-06, 3.31630e-06, 3.55037e-06};
    TGraphErrors *gr = new TGraphErrors(n,x,y,ex,ey);
    gr->SetTitle("Ketek Dark Noise Rate");
    TF1 f("linear Fit","[0]+[1]*x",0,40);
    f.SetParameters(-.23,0.01);

    gr->SetMarkerStyle(21);
    gr->SetMarkerColor(2);
    f.SetLineColor(2);
    gr->Fit("linear Fit","R");
    f.Draw("same");
    cout << f.GetX(0) << endl;
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Operating Voltage [V]");
    gr->GetYaxis()->SetTitle("Dark Noise Rate [1/s]");
}
void PlotDN_Ketek()
{
    TCanvas *c1 = new TCanvas("c1","Ketek Dark Noise Rate",200,10,700,500);
    const int n = 7;
    Double_t x[n]       = {25.92-24.4344, 26.42-24.4344, 26.92-24.4344, 27.42-24.4344, 27.92-24.4344, 28.42-24.4344, 28.92-24.4344};
    Double_t ex[n]      = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t dn[n]      = {13.49, 19.95, 27.64, 35.49, 44.48, 57.86, 73.64};
    Double_t edn[n]     = {0.006, 0.01, 0.01, 0.02, 0.03, 0.04, 0.04};
      TGraphErrors *gr = new TGraphErrors(n,x,dn,ex,edn);
    gr->SetTitle("");
    gr->SetMarkerStyle(21);
    gr->SetMarkerColor(4);
    gr->GetXaxis()->SetLabelFont(40);
    gr->GetYaxis()->SetLabelFont(40);
    gr->GetXaxis()->SetTitleFont(40);
    gr->GetYaxis()->SetTitleFont(40);
    gr->GetXaxis()->SetTitleSize(0.05);
    gr->GetXaxis()->SetTitleOffset(1.1);
    gr->GetYaxis()->SetTitleSize(0.05);
    gr->GetXaxis()->SetLabelSize(0.05);
    gr->GetYaxis()->SetLabelSize(0.05);
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Over Voltage [V]  ");
    gr->GetYaxis()->SetTitle("Dark Noise Rate [1/s]  ");

}
void PlotXT_Ketek()
{
    c1 = new TCanvas("c1","Ketek Crosstalk Probability",200,10,700,500);
    Double_t x[5]  = {25.92, 26.42, 26.92, 27.42, 27.92};
    Double_t y[5]  = {4.951, 7.450, 10.61, 14.51, 13.2};
    Double_t ex[5] = {0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t ey[5] = {5.3e-2, 6.5e-2, 8.1e-2, 9.9e-2, 9.26e-2};
    TGraphErrors *gr = new TGraphErrors(5,x,y,ex,ey);
    gr->SetTitle("");
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Operating Voltage [V]");
    //gr->GetXaxis()->SetTitleFont(6);
    //gr->GetXaxis()->SetTitleOffset(0);
    gr->GetYaxis()->SetTitle("Crosstalk probability [%]");

}



void PlotGain_hama()
{
    const int n = 4;
    c1 = new TCanvas("c1","Hamamatsu Gain",200,10,700,500);
    Double_t x[n]  = {58.50-56.4919, 59.00-56.4919, 59.50-56.4919, 60.00-56.4919};
    Double_t y[n]  = {3.60617e-02, 4.50695e-02, 5.40246e-02, 6.30519e-02};
    Double_t ex[n] = {0.05, 0.05, 0.05};
    Double_t ey[n] = {5.17068e-06 , 5.82016e-06, 5.80929e-06, 6.26563e-06};
    TGraphErrors *gr = new TGraphErrors(n,x,y,ex,ey);
    gr->SetTitle("Hamamatsu Gain");
    TF1 f("linear Fit","[0]+[1]*x",0,400);
    f.SetParameters(-.23,0.01);

    gr->SetMarkerStyle(21);
    gr->SetMarkerColor(4);
    gr->Fit("linear Fit","R");
    f.Draw("same");
    f.SetLineColor(4);
    cout << f.GetX(0) << endl;
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Operating Voltage [V]");
    gr->GetYaxis()->SetTitle("Pulse Amplitude [V]");
    PlotGain_Ketek();
    gr->Draw("same");
    f.Draw("same");
}
void PlotDN_hama()
{
    TCanvas *c1 = new TCanvas("c1","Hamamatsu Dark Noise Rate",200,10,700,500);
    Double_t x[5]  = {25.92-24.4344, 26.42-24.4344, 26.92-24.4344, 27.42-24.4344, 27.92-24.4344};
    Double_t y[5]  = {15.21, 22.68, 28.74, 43.51, 52.54};
    Double_t ex[5] = {0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t ey[5] = {4e-11, 6e-11, 9e-11, 16e-11, 18e-11};
    TGraphErrors *gr = new TGraphErrors(5,x,y,ex,ey);
    gr->SetTitle("Hamamatsu Dark Noise Rate");
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Over Voltage [V]");
    gr->GetYaxis()->SetTitle("Dark Noise Rate [1/s]");

}
void PlotXT_hama()
{
    c1 = new TCanvas("c1","Hamamatsu Crosstalk Probability",200,10,700,500);
    Double_t x[5]  = {25.92, 26.42, 26.92, 27.42, 27.92};
    Double_t y[5]  = {4.951, 7.450, 10.61, 14.51, 13.2};
    Double_t ex[5] = {0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t ey[5] = {5.3e-2, 6.5e-2, 8.1e-2, 9.9e-2, 9.26e-2};
    TGraphErrors *gr = new TGraphErrors(5,x,y,ex,ey);
    gr->SetTitle("");
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Operating Voltage [V]");
    //gr->GetXaxis()->SetTitleFont(6);
    //gr->GetXaxis()->SetTitleOffset(0);
    gr->GetYaxis()->SetTitle("Crosstalk probability [%]");

}

void recover(double of, double t, double dt)
{
    TF1 *exp = new TF1("exp","-[0]+[0]*exp(-(x-[2])/[1])",0,8e-6);
    exp->SetParameters(of,t,dt);
    exp->Draw("same");
}
void CalcXT(int Nt, int Ns)
{
    TF1 *f = new TF1("f","1-[0]/[1]",0,1);
    TF1 *df = new TF1("df","sqrt((1-[0]/[1])*([0]/[1])/[1])",0,1);
    df->SetParameter(0,Ns);
    f->SetParameter(0,Ns);
    df->SetParameter(1,Nt);
    f->SetParameter(1,Nt);
    cout <<" XTalk "<< f->Eval(0) << " +/- " << df->Eval(0) <<endl;
}
void PlotAP_ktk()
{
    const int n = 7;
    Double_t x[n]       = {25.92-24.4344, 26.42-24.4344, 26.92-24.4344, 27.42-24.4344, 27.92-24.4344, 28.42-24.4344, 28.92-24.4344};
    Double_t ex[n]      = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t dn[n]      = {13.49, 19.95, 27.64, 35.49, 44.48, 57.86, 73.64};
    Double_t edn[n]     = {0.006, 0.01, 0.01, 0.02, 0.03, 0.04, 0.04};
    Double_t ap1[n]     = {1.33165e-02, 1.87684e-02, 3.08423e-02, 3.47236e-02, 4.45460e-02, 6.12304e-02, 7.20898e-02};
    Double_t eap1[n]    = {1.10611e-03, 1.35092e-03, 1.87028e-03, 2.08961e-03, 2.65252e-03, 3.16035e-03, 3.71300e-03};
    Double_t ap2[n]     = {1.06311e-01, 1.47277e-01, 1.88433e-01, 2.16011e-01, 2.56417e-01, 2.96806e-01, 3.36314e-01};
    Double_t eap2[n]    = {1.65257e-03, 1.95852e-03, 2.05735e-03, 2.34747e-03, 2.56183e-03, 2.80447e-03, 2.81971e-03};
    Double_t ap3[n]     = {1.56507e-02, 2.71683e-02, 3.00025e-02, 3.16068e-02, 5.27669e-02, 4.81647e-02, 5.05420e-02};
    Double_t eap3[n]    = {1.52429e-03, 2.26120e-03, 2.39457e-03, 2.63039e-03, 3.53669e-03, 4.00607e-03, 5.40865e-03};

    Double_t AP[n]={0};
    Double_t eAP[n]={0};
    for(int i = 0; i<n; ++i)
    {
        AP[i]= ap1[i] + ap2[i] + ap3[i];
        eAP[i]= eap1[i] + eap2[i] + eap3[i];
    }
    TGraphErrors *gr = new TGraphErrors(n,x,(AP),ex,(eAP));
    gr->SetTitle("");
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Over Voltage [V]");
    gr->GetYaxis()->SetTitle("After Pulsing Probability");

}
