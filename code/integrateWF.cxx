#include <iostream>
#include <stdlib.h>
#include "TFile.h"
#include "TNtuple.h"
#include "LecroyFile.h"
#include "Waveform.h"
#include "WaveformProcessor.h"

int main(int argc, char* argv[]){
  if (argc<4) {std::cout << "Usage: " << argv[0] << "[run] [start] [end] [channel]\n"; exit(0);}
  
  int run = atoi(argv[1]);
  double start = atof(argv[2]);
  double end = atof(argv[3]);
  int channel; if (argc>4) channel = atoi(argv[4]); else channel = 1;
  
  WaveformProcessor* wfProc = new WaveformProcessor("LampRunInfo.txt",run,channel,-1);
  int nEvents=wfProc->getWaveformCount();
  wfProc->readWaveform(0);
  int nBins=wfProc->getCurrentWaveform()->GetNbinsX();
  int trig=wfProc->getCurrentWaveform()->FindBin(0);
  double secPerBin=wfProc->getCurrentWaveform()->GetBinWidth(0);
  
  //convert start and end time to bins
  start=wfProc->getCurrentWaveform()->FindBin(start);
  end=wfProc->getCurrentWaveform()->FindBin(end);
  if (end<1) end=nBins;
  
  char name[10];
  sprintf(name,"AP%i.root",run);
  TFile* save = new TFile(name,"RECREATE");
  TNtuple* charge = new TNtuple("charge","charge","WF:integral:minHeight");
  double integral;
  double minHeight;std::cout << "start=" << start << std::endl;
  
  for (int i=0;i<nEvents;i++){
    if (i%100==0)
      std::cout << "Processing WF " << i << "\n";
    wfProc->readWaveform(i);
    wfProc->processBaseline();
    integral=wfProc->getCurrentWaveform()->Integral(start,end)-wfProc->getBaselineMu()*(end-start);
    minHeight=wfProc->findMin(trig-200e-09/secPerBin,trig+200e-09/secPerBin);
    charge->Fill(i,integral,minHeight);
  }
  
  delete wfProc;
  save->cd();
  charge->Write();
  save->Close();
  
  return 0;
}
