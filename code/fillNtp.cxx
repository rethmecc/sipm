//Carl Rethmeier; June-July 2015; rethmeier.carl@gmail.com
//Get a WF range
//Scan for pulses
//Place them in nTuple

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <math.h>
#include <time.h>
#include "LecroyFile.h"
#include "WaveformProcessor.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TSpectrum.h"
#include "TMath.h"
#include "TVirtualFitter.h"

struct result{
  double value;
  double error;
};

double getZeros(TNtuple* ntp, double Hlow, double Hhigh);
result darkRateWindow(TNtuple* ntp, double scanWindow, double APWindow, int WFperSeq);
Double_t fpeaks(Double_t *x, Double_t *par);
void getPeaks(TH1D* h);
std::ofstream fout;

int main(int argc,char* argv[]){
  int run=0;
  int numEvents=1;
  int channel=1;
  int verbosity=0;
  double level=3.5;
  double nWindow=1;
  int start=1;
  int end=-1;
  bool trunc=false;
  bool deep=false;
  double smooth=0;
  double APWindow=0.1;
  char saveNameDef[10]="CTWF.txt";
  char* saveName=saveNameDef;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "r:n:c:hv:l:w:s:b:e:f:da:")) != -1)
   switch (c)
     {
     case 'r':  //Run#
       run = atoi(optarg);
       break;
     case 'n':  //Number of events
       numEvents = atoi(optarg);
       break;
     case 'c':  //Channel#
       channel = atoi(optarg);
       break;
     case 'f':  //Savefile name
       saveName = optarg;
       break;
     case 'l':  //Threshold level in # of sigma
       level = atof(optarg);
       break;
     case 'w':  //Window length
       nWindow = atoi(optarg);
       break;
     case 'v':  //amount of text displayed
       verbosity = atoi(optarg);
       break;
     case 's':  //Perform smoothing
       smooth = atof(optarg);
       break;
     case 'b':  //Start of region
       start = atoi(optarg);
       break;
     case 'e':  //End of region
       end = atoi(optarg);
       break;
     case 'a':  //Afterpulse window
       APWindow = atof(optarg);
       break;
     case 'd':  //Deep analysis
       deep=true;
       break;
     case 'h':  //Print usage
       std::cout << "Usage: " << argv[0] << " [parameters]... [options]...\n"
                 << "This application uses the height of dark noise pulses to measure the cross-talk rate\n" 
                 << "It assumes that there is no light source, and that the scope triggers on a dark noise pulse.\n"
                 << "\nParameters and options:\n"
                 << "-r run#          Run number in \'LampRunInfo.txt\'; default=0\n"
                 << "-n numEvents     Number of waveforms to be processed, starting from 0; default=1\n"
                 << "-c channel#      Oscilloscope channel #; default=1\n"
                 << "-f fileName      Name of ASCII file in which resukts will be saved; default=\"PEWF.txt\"\n"
                 << "-l level         Minimum peak height in baseline RMS; default=8\n"
                 << "-w windows       Number of regions to divide waveform into; default=1\n"
                 << "-b begin         Start of region to scan for peaks; default=1\n"
                 << "-e end           End of region to scan for peaks. Use -1 for end of WF; default=-1\n"
                 << "-v verbosity     Verbosity level (0,1,2,3,4,5); default=0\n"
                 << "-s smoothLength  Perform boxcar waveform smoothing with given radius\n"
                 << "-a apWindow      Time in seconds after last pulse to wait before searching for DN; default=0.1\n"
                 << "-d               Perform deep scan for peaks\n"
                 << "-h               Display this usage info\n";
       exit(0);
       break;
     case '?':
       if (optopt == 'f' || optopt == 'e' || optopt == 'b' || optopt == 'v' || optopt == 'w' || optopt == 'l' || optopt == 'c' || optopt == 'n' || optopt == 'r' || optopt == 'a')
         fprintf (stderr, "Option -%c requires an argument.\n", optopt);
       else if (isprint (optopt))
         fprintf (stderr, "Unknown option `-%c'.\n", optopt);
       else
         fprintf (stderr,
                  "Unknown option character `\\x%x'.\n",
                  optopt);
     return 1;
     default:
     abort ();
     }
  
  float dark=0;
  float light=0;
  double nBins;
  double secPerBin;
  double CT;
  double Hlow;
  double Hhigh;
  int window;
  double temp=0;
  double P0;
  result DN;
  int percent=0;
  
  char name[20];  
  if (channel==1)
    sprintf(name,"proc/minHist%i.root",run);
  else
    sprintf(name,"proc/minHist%i.%i.root",run,channel);
  
  TFile* saveHist = new TFile(name,"RECREATE");
  TNtuple* ntp = new TNtuple("ntp","ntp","W#:time:height:charge:width:trigTime");
  TNtuple* minHist = new TNtuple("minHist","minHist","W#:minHeight");
  
  if (trunc){
    fout.open(saveName,std::ios::trunc);
    fout << "run | darkRate (Hz) | darkRateErr (Hz)\n";
  }
  else
    fout.open(saveName,std::ios::app);
  
  WaveformProcessor* wfProc = new WaveformProcessor("LampRunInfo.txt",run,channel,-1);
  wfProc->readWaveform(0);
  nBins=wfProc->getCurrentWaveform()->GetNbinsX();
  secPerBin=wfProc->getCurrentWaveform()->GetBinWidth(0);
  if (end<0)
    end=nBins;
  window=(end-start)/nWindow;
  if (numEvents>wfProc->getWaveformCount() || numEvents<1) numEvents=wfProc->getWaveformCount();
  
  for (int i=0;i<numEvents;i++){
    if (verbosity==0 && (i+1)*100./numEvents>=(percent+1)){
      percent=(i+1)*100./numEvents;
      std::cout << std::string(4,'\b');
      std::cout << percent << "%" << std::flush;
    }
    
    if (verbosity>0){
      std::cout << "Processing WF " << i << std::endl;
    }
    wfProc->readWaveform(i);
//     if (smooth)
      wfProc->smoothWaveform(smooth);
    for (double j=0;j<nWindow;j++){
      temp=wfProc->findMin(start+window*j,start+window*(j+1));
      minHist->Fill(i,temp);
      if (temp>Hhigh)
        Hhigh=temp;
    }
    temp=wfProc->findMin(start,start+window*nWindow);
    if (temp<Hlow)
      Hlow=temp;
    wfProc->processBaseline();
    if (deep)
      wfProc->findPulse2(ntp,start,end,level,verbosity);
    else
      wfProc->findClearPulses(ntp,start,end,level,verbosity-1);
  }
  
  std::cout << std::endl;

  delete wfProc;
  
  saveHist->cd();
  
  ntp->Write();
  
  fout << run << "    ";
//   P0=getZeros(minHist,Hlow,Hhigh);
  DN=darkRateWindow(ntp, 3e-6, APWindow, 50);
  fout << DN.value << "    " << DN.error;
  fout << std::endl;
  
//   CT=-log(P0);
  
  std::cout << "Dark rate = " << DN.value << "±" << DN.error << "Hz" << std::endl;
//   std::cout << "<CT> = " << CT << "\n";
  
  minHist->Write();
  saveHist->Close();
  
  return 0;
}

double getZeros(TNtuple* ntp,double Hlow,double Hhigh){
//---------------------------------------------------------------------  
  static int run=0; //run #
  
  int nBins=300;  //bins in hist
  int minLoc; //location of minimum after zeroeth peak
  int minLoc2; //location of minimum before zeroeth peak
  int maxLoc; //location of maximum (assumed to be largest bin in zeroeth peak)
  double minVal=1000;  //value at minLoc
  double minVal2=1000;  //value at minLoc2
  double last;  //used to store value of previous bin when finding minLoc
  int consec; //counter used in finding minimum; ensures that noise does not result in false minimum
  bool minFound=false;
  double PE1=0; //integral of 1PE peak
  double allPE=0; //Integral of all peaks
  
  char name[10];
  sprintf(name,"hist%i",run);
  
  TH1D* hist = new TH1D(name,name,nBins,Hlow,Hhigh);

  ntp->Project(name,"minHeight");

  hist->GetBinContent(1); //Deals with a weird (and hopefully temporary) bug in ROOT 6-02-08; works fine in 5.34-08
  maxLoc=hist->GetMaximumBin();
  last = hist->GetBinContent(maxLoc);
  consec=0;
  for (int i=maxLoc-1; i>0; i--){
    if (hist->GetBinContent(i)>last){ //find size of upward slope
      consec++;
    }
    /*else if (consec>0 && hist->GetBinContent(i)<0.01*hist->GetBinContent(maxLoc))
      consec--;
    */else
      consec=0;
    if (hist->GetBinContent(i)<minVal){ //record minimum value
      minVal=hist->GetBinContent(i);
      minLoc=i;
    }
    if (consec>2 || hist->GetBinContent(i)>minVal*10 || (hist->GetBinContent(i)==0 && i<maxLoc)){  //when reached upward slope of at least 2 bins, stop looking for min
      minFound=true;
      break;
    }
    last=hist->GetBinContent(i);
  }
  
  last = hist->GetBinContent(maxLoc);
  consec=0;
  for (int i=maxLoc+1; i<nBins; i++){// Right side of zero peak
    if (hist->GetBinContent(i)>last){ //find size of upward slope
      consec++;
    }
    /*else if (consec>0 && hist->GetBinContent(i)<0.01*hist->GetBinContent(maxLoc))
      consec--;
    */else
      consec=0;
    if (hist->GetBinContent(i)<minVal2){ //record minimum value
      minVal2=hist->GetBinContent(i);
      minLoc2=i;
    }
    if (consec>2 || hist->GetBinContent(i)>minVal2*10 || (hist->GetBinContent(i)==0 && i<maxLoc)){  //when reached upward slope of at least 2 bins, stop looking for min
      break;
    }
    last=hist->GetBinContent(i);
  }
  std::cout << "minLoc=" << minLoc << "     minLoc2=" << minLoc2 << std::endl;
  if (minFound){  //integrate zeroeth peak
    PE1=hist->Integral(minLoc,minLoc2);
    allPE=hist->Integral(0,hist->FindBin(minLoc2));
  }
  else
    std::cout << "No minimum\n";

  hist->Write();  //save histogram in results.root
  run++;
  
  fout << PE1/allPE << " " << allPE << " " << PE1 << std::endl;
  std::cout << "PE1=" << PE1 << "   allPE=" << allPE << std::endl;
  return PE1/allPE;
}

result darkRateWindow(TNtuple* ntp, double scanWindow, double APWindow, int WFperSeq){
  double lastTime;
  int sequence=0;
  int N=0;
  int N0=0;
  double P0;
  int i=0;
  
  int Entries=ntp->GetEntries();
  float fTrigTime=0;
  TBranch* trigTimeBranch=0;
  ntp->SetBranchAddress("trigTime",&fTrigTime,&trigTimeBranch);
  float fW=0;
  TBranch* WBranch=0;
  ntp->SetBranchAddress("W#",&fW,&WBranch);
  float fTime=0;
  TBranch* timeBranch=0;
  ntp->SetBranchAddress("time",&fTime,&timeBranch);
  
  WBranch->GetEntry(i);
  trigTimeBranch->GetEntry(i);
  while (i<Entries){
    lastTime=fTrigTime+fTime;
    WBranch->GetEntry(i);
    trigTimeBranch->GetEntry(i);
    timeBranch->GetEntry(i);
    if (fW/WFperSeq>=sequence+1){
      sequence=fW/WFperSeq;
      lastTime=0;
    }
    //Find 0.1s space between pulse
    if (fTrigTime+fTime>lastTime+APWindow){
      //Look for pulses in scanWindow
      N0+=(fTrigTime+fTime-lastTime-APWindow)/scanWindow;
      N+=(fTrigTime+fTime-lastTime-APWindow)/scanWindow+1;
    }
    i++;
  }
  std::cout << "N0=" << N0 << " N=" << N << std::endl;
  result dn;
  P0=N0/static_cast<double>(N);
  dn.value=-log(P0)/scanWindow;
  dn.error=sqrt(P0*(1-P0)/N)/scanWindow;
  return dn;
}
//---------------------------------------------------------------------
/*void getPeaks(TH1D* h){
   Int_t npeaks=5;
   TCanvas *c1 = new TCanvas("c1","c1",10,10,1000,900);
   c1->Divide(1,2);
   c1->cd(1);
   Double_t par[3000];
   TH1F *h2 = (TH1F*)h->Clone("h2");
   //Use TSpectrum to find the peak candidates
   TSpectrum *s = new TSpectrum(2*npeaks);
   Int_t nfound = s->Search(h,2,"",0.10);
   printf("Found %d candidate peaks to fit\n",nfound);
   //Estimate background using TSpectrum::Background
   TH1 *hb = s->Background(h,20,"same");
   if (hb) c1->Update();

   //estimate linear background using a fitting method
   c1->cd(2);
   TF1 *fline = new TF1("fline","pol1",0,1000);
   h->Fit("fline","qn");
   //Loop on all found peaks. Eliminate peaks at the background level
   par[0] = fline->GetParameter(0);
   par[1] = fline->GetParameter(1);
   npeaks = 0;
   Double_t *xpeaks = s->GetPositionX();
   for (int p=0;p<nfound;p++) {
      Double_t xp = xpeaks[p];
      Int_t bin = h->GetXaxis()->FindBin(xp);
      Double_t yp = h->GetBinContent(bin);
      if (yp-TMath::Sqrt(yp) < fline->Eval(xp)) continue;
      par[3*npeaks+2] = yp;
      par[3*npeaks+3] = xp;
      par[3*npeaks+4] = 3;
      npeaks++;
   }
   printf("Found %d useful peaks to fit\n",npeaks);
   printf("Now fitting: Be patient\n");
   TF1 *fit = new TF1("fit",fpeaks,0,1000,2+3*npeaks);
   //we may have more than the default 25 parameters
   TVirtualFitter::Fitter(h2,10+3*npeaks);
   fit->SetParameters(par);
   fit->SetNpx(1000);
   h2->Fit("fit");
}
Double_t fpeaks(Double_t *x, Double_t *par) {
  Int_t npeaks=5;
   Double_t result = par[0] + par[1]*x[0];
   for (Int_t p=0;p<npeaks;p++) {
      Double_t norm  = par[3*p+2];
      Double_t mean  = par[3*p+3];
      Double_t sigma = par[3*p+4];
      result += norm*TMath::Gaus(x[0],mean,sigma);
   }
   return result;
}*/
