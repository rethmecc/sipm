/*******************************************************************************
* PulseFinding.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
* v0.2  2015/07/22  Revisions for new pulse-finding (Lloyd James, lloyd.thomas.james@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <time.h>
#include "LecroyFile.h"
#include "WaveformProcessor.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"

// Parameters

int main(int argc, char** argv){
  int aRun = argc>1 ? atoi(argv[1]) : 0;
  const char* aSetupFileName = argc>2 ? argv[2] : "LampRunInfo.txt";
  double aChannel = argc>3 ? atoi(argv[3]) : 1;
  double pT = argc>4 ? atof(argv[4]) : 0.08; //WaveformProcessor compares a value to this number, called "peakiness".
                                             //Peakiness is found as vert/horiz, where:
                                             //   horiz = 20+ln([current peak x-position] - [previous peak x-position]) divided by
                                             //   vert  = ratio of [current peak amplitude] / [previous peak amplitude]
                                             //If the "peakiness" is greater than the "peakiness threshold" (pT), then it counts
                                             //as a correctly identified peak.
                                             //
                                             //I don't understand the basis for this, as the horiz is an offset from 20, which makes
                                             //no sense to me, nor the justification for using the natural log over base 10.  It also 
                                             //obviously is not working, as the pulse finder is incorrectly identifying noise within a
                                             //peak as a second peak, which I think is due to a smaller difference in x-positions will
                                             //cause a larger "peakiness" value, when this should not be the case.
  double lBLF = argc>5 ? atof(argv[5]) : 4.5;
  double nSSCR = argc>6 ? atof(argv[6]) : 1e-9;
  double nSSCF = argc>7 ? atof(argv[7]) : 5e-9;
  double dAFR = argc>8 ? atof(argv[8]) : 0.1;
  double dAFF = argc>9 ? atof(argv[9]) : 0.7;
  double mPW = argc>10 ? atof(argv[10]) : 2e-8;
  double aPolarity = argc>11 ? atoi(argv[11]) : -1;
  WaveformProcessor* wfProc = new WaveformProcessor(aSetupFileName,aRun,aChannel,aPolarity);
 
 // --- Open output file
  //>>> Strip directory name from file name
  int slashIndex=0;
  for(int index=0; index<strlen(wfProc->getFileName()); index++){
    if(strncmp(wfProc->getFileName()+index,"/",1)==0) slashIndex=index;
  }
  char outFileName[200];
  sprintf(outFileName,"ntp/%s.fanat",wfProc->getFileName()+slashIndex);
  std::cout << "PulseFinding " << outFileName <<  " " << wfProc->getBaselineRMS() << slashIndex << std::endl;

  TFile outFile(outFileName ,"RECREATE");
  TNtuple ntp("ntp","ntp",
          "evt:tt:blmu:blRMS:np:pa:pt:paa:pq:pw:fna:pr:p:pn");
          //items are: event, trigger time, baseline mean, baseline RMS, number of peaks, peak amplitude, peak time, 
          //           peak absolute amplitude, peak charge, peak width, fifty nanosecond average, peak rise above pulse, peakiness, and peak number.
  float ntpCont[14];
  int skippedcount = 0;

  //Run time information
  time_t t;
  time(&t);
  int t0 = t;
  
  //Average Amplitude
  int nEvent = wfProc->getWaveformCount();

  //Initisation stuff
  int nSkipped = 0;
  for(int iEvent=0; iEvent<nEvent; iEvent++){
    wfProc->readWaveform(iEvent);
    wfProc->processBaseline();
    
    //Applies the peak-finding. Returned int is not useful. Currently uses these params - 
    //    if we want to vary these we should make them inputted from the command line
    int flag=0;
    if (iEvent==6076) flag=1;
    int res = wfProc->findPulseGeneric(nSSCR, nSSCF, dAFR, dAFF, lBLF, mPW, pT, flag);

    //Fills ntuple with results of peak-finding
    for(int iPulse=0; iPulse<wfProc->getNPulse(); iPulse++){
      ntpCont[0]=iEvent;
      ntpCont[1]=wfProc->getTriggerTime();
      ntpCont[2]=wfProc->getBaselineMu();
      ntpCont[3]=wfProc->getBaselineRMS();
      ntpCont[4]=wfProc->getNPulse();
      ntpCont[5]=wfProc->getPulseAmplitude(iPulse);
      ntpCont[6]=wfProc->getPulseTime(iPulse);
      ntpCont[7]=wfProc->getPulseAbsAmplitude(iPulse);
      ntpCont[8]=wfProc->getPulseCharge(iPulse);
      ntpCont[9]=wfProc->getPulseWidth(iPulse);
      ntpCont[10]=wfProc->get50NanoAvg(iPulse);
      ntpCont[11]=wfProc->getPeakRise(iPulse);
      ntpCont[12]=wfProc->getPeakiness(iPulse);
      ntpCont[13]=iPulse;

      ntp.Fill(ntpCont);
    } 
      if ((wfProc->getNPulse() == 0)){
	      //std::cout << "Skipped event " << iEvent  << std::endl;
	      nSkipped++;
      } 
    

     
    //Outputs information on elapsed runs
    if(floor(iEvent*200./nEvent)==(iEvent*200./nEvent)){
        time(&t);
        std::cout << iEvent << "/" << nEvent << " running for " << (t-t0) << "s" << " " << std::endl; //<< wfProc->getNPulse() <<std::endl; //"\r";
    }    
  }		

  //Completion summary
  std::cout << "Writing output in " << outFileName << std::endl;
  std::cout << "Skipped " << nSkipped << " triggers." << std::endl;
  delete wfProc;
  outFile.cd();
  ntp.Write();
  outFile.Close();
  
  return 0;
}

