/*******************************************************************************
* WaveformProcessor.cxx
* Initially derived from Distribution.cxx
*
* Description:
* Zee file for procezzing zee vaveformz. --Paul
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
* v1.0  2014/06/30  WaveformProcessor (co-op students and Fabrice Retiere, fretiere@triumf.ca)
* v1.1  2014/08/31  Improved fitting (Lloyd James, lloyd.thomas.james@gmail.com and Nolan Ohmart, ohmartn@gmail.com)
* v2.0  2015/08/31  Redid pulse finding (Lloyd James, lloyd.thomas.james@gmail.com and Carl Rethmeier, rethmeier.carl@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "DataFile.h"
#include "LecroyFile.h"
#include "LecroyHdfFile.h"
#include "WaveformProcessor.h"
#include "Waveform.h"


#include "TFile.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"
#include "TNtuple.h"

// --- Constuctor
WaveformProcessor::WaveformProcessor(const char* aSetupFileName, int aRun, int aChannel, int aPolarity){
  init(aSetupFileName,aRun,aChannel, aPolarity);
}

// Some initialisation code
void WaveformProcessor::init(const char* aSetupFileName, int aRun, int aChannel, int aPolarity){

    mChannel=aChannel;
    mPolarity=aPolarity;
    strcpy(mSetupFileName,aSetupFileName);

    
    // --- Open run index file to get filename for run
    std::ifstream parFile(aSetupFileName);
    char buf[200];
    parFile >> buf >> buf;
    parFile >> mRun >> mFileName;
    while(!parFile.eof() && mRun!=aRun){
        parFile >> mRun >> mFileName;
    }
    if(parFile.eof()){
        std::cerr << "Could not find run requested " << aRun << std::endl;
        exit(0);
    }


    // --- Open input file
    //>>> check file extension
    int iChar=strlen(mFileName)-1;
    while(iChar>0 && strncmp(mFileName+iChar,".",1)!=0) iChar--;
    if(strncmp(mFileName+iChar+1,"root",4)==0){
        mDataFile = new LecroyFile(mFileName);
    }
    else{
        if(strncmp(mFileName+iChar+1,"hdf5",4)==0){
            mDataFile = new LecroyHdfFile(mFileName,aChannel);
        }
        else{
            std::cerr << "cannot tell file type. Aborting " << std::endl;
            exit(1);
        }
    }


    // --- prepare vector of pulses
    mNPulse=0;
    mNPulseMax= 20;
    mPulse = new Pulse[mNPulseMax];

    mSmoothedWF=NULL;
    mDataWF=NULL;
    mBins=NULL;

    mForMacro = false;
    
}


WaveformProcessor* WaveformProcessor::mInstanceForRootMacro=0;
int WaveformProcessor::mRun=-1;
int WaveformProcessor::mChannel=-1;
int WaveformProcessor::mEv=-1;

//Creates instance of WaveformProcessor for use in root  macros
WaveformProcessor* WaveformProcessor::instanceForRootMacro(const char* aSetupFileName, int aRun, int aChannel, int aPolarity)
{
    if(!mInstanceForRootMacro){
      mInstanceForRootMacro = new WaveformProcessor(aSetupFileName, aRun, aChannel, aPolarity);
      mInstanceForRootMacro->setForMacro(true);
    }	
    else{
        if(mRun!=aRun || aChannel!=mChannel){
            mInstanceForRootMacro->clear();
            mInstanceForRootMacro->init(aSetupFileName, aRun, aChannel, aPolarity);
            mInstanceForRootMacro->setForMacro(true);
        }
    }
    
    return mInstanceForRootMacro;
}

// Destructor
WaveformProcessor::~WaveformProcessor(){
    clear();
}
void WaveformProcessor::clear(){
    delete mDataFile;
    if(mHBaseline) mHBaseline->Delete();
    if(mSmoothedWF) mSmoothedWF->Delete();
    delete[] mPulse;
}


// Access the waveform
void WaveformProcessor::readNextWaveform(){
    mWF = mDataFile->getNextWaveform(); // index is not used!
}
void WaveformProcessor::readWaveform(int aIndex){
    mWF = mDataFile->getWaveform(aIndex);
}
void WaveformProcessor::readWaveform(const char* aHName){
    mWF = mDataFile->getWaveform(aHName);
}
int WaveformProcessor::getCurrentWaveformIndex(){
    char ciwf[50];
    strcpy(ciwf,mWF->GetName()+7);
    return atoi(ciwf);
}


// Baseline
// Calculates baseline parameters
int WaveformProcessor::processBaseline(){
    // Create histogram for baseline calculation
    mHBaseline = (TH1F*) gROOT->FindObjectAny("HBaseline");
    mHBaselineFit = (TF1*) gROOT->FindObjectAny("HBaselineFit");
    if(mHBaseline) mHBaseline->Delete();
    if(mHBaselineFit) mHBaselineFit->Delete();
    mAmpMin = mWF->GetMinimum();
    mAmpMax = mWF->GetMaximum();
    int nBinsBaseline = 500;
    mHBaseline = new TH1F("HBaseline","HBaseline",nBinsBaseline,mAmpMin,mAmpMax);
    mHBaselineFit = new TF1("HBaselineFit","gaus");
    mHBaseline->Reset("C");
    for(int iBin=1; iBin <= mWF->GetNbinsX(); iBin++){
        mHBaseline->Fill(mWF->GetBinContent(iBin));
    }
    int blMostProbBin=0;
    int blMostProbCont=0;
    for(int iBin=1; iBin <= mHBaseline->GetNbinsX(); iBin++){
        if(mHBaseline->GetBinContent(iBin) > blMostProbCont){
            blMostProbCont = mHBaseline->GetBinContent(iBin);
            blMostProbBin = iBin;
        }
    }
    mBmu=mHBaseline->GetBinCenter(blMostProbBin);
    mHBaseline->Fit(mHBaselineFit,"Q");
    mBRMS=mHBaselineFit->GetParameter(2);

    return mBRMS<5*1e-3; // Otherwise noisy. i.e. pick up
}

//Generic Pulse Finder by Lloyd
int WaveformProcessor::findPulseGeneric(double nSSCR, double nSSCF, double dAFR, double dAFF, double lBLF, double mPW, double pT, int flag, int buf){
   //Parameters
  ////////////////////////////////
  double nSecSlopeCheckRise = nSSCR;
  double nSecSlopeCheckFall = nSSCF;
  double diffAmpFactorRise = dAFR;
  double diffAmpFactorFall = dAFF;
  double lowerBLFactor = lBLF;
  double minPulseWidth = mPW;
  double peakinessThresh = pT;
  ////////////////////////////////

  double avgVar = 0;
  int rIntersects [100]; //Indices of intersections between lower bound and data on rising slopes
  int nrIntersects = 0;
  int fIntersects [100]; //Indices of intersections between lower bound and data on falling slopes            
  int nfIntersects = 0;
  int lMax [1000]; //Indices of local maxima above lower bound
  int nlMax = 0;
  int missedMax [100];
  int nMissedMax = 0;
  int peaks [100]; //Indices of local maxima above noise upper bound with minimum 'peakiness'
  int nPeaks = 0;
  int probablePeaks [100]; //Indices of local maxima above noise upper bound
  int nProbablePeaks = 0;
  int edge [1000];// 0 -> rising edge, 1 -> falling edge
  int smallPulseRegions [100] [2];
  int nSmallPulseRegions = 0;

  //BinData struct used to store information about each bin, used in plotting for root macros
  if ((!mBins) && (mForMacro)) mBins = new BinData [mWF->GetNbinsX()];
  if (mForMacro){
    for (int iBin = 0; iBin < mWF->GetNbinsX(); iBin++){
      mBins[iBin].islMax = false;
      mBins[iBin].isProbPeak = false;
      mBins[iBin].isPeak = false;
      mBins[iBin].edge = 0;
      mBins[iBin].rise = 0.0;
      mBins[iBin].slopecheck = 0.0;
      mBins[iBin].peakiness = 0.0;
    }
  }

  //Caclulates the average magnitude of variation between bins in the waveform
  for(int iBin = 1; iBin <= mWF->GetNbinsX(); iBin++){
    avgVar += fabs(mWF->GetBinContent(iBin) - mWF->GetBinContent(iBin-1));
  }
  avgVar /= mWF->GetNbinsX();

  double binWidthNS = mWF->GetBinWidth(1) * 1e9;
  //Applies smoothing to the waveform
  smoothWaveform(binWidthNS,2);//flag);
  //if (!mDataWF) mDataWF=mWF;

  double buffer = buf / binWidthNS; // TEST for searching on either side of a peak
  double lb = mBmu + lowerBLFactor*avgVar/binWidthNS;  //Lower baseline defined thusly
  avgVar = 0;
  for(int iBin = 1; iBin <= mWF->GetNbinsX(); iBin++){
    avgVar += fabs(mWF->GetBinContent(iBin) - mWF->GetBinContent(iBin-1));
  }
  avgVar /= mWF->GetNbinsX();

  //Checks for radio noise
  for (int iBin = mWF->FindBin(-100e-9); iBin < mWF->FindBin(100e-9); iBin++){
    if (((mPolarity == 1) && (mWF->GetBinContent(iBin) < -1*lb)) || ((mPolarity == -1) && (mWF->GetBinContent(iBin) > 1*lb))){
      
      mNPulse = 0;
      return 0;
    }
  }

  //Finds the intersections between the waveform and the lower baselineo.
  for(int iBin = 1; iBin <= mWF->GetNbinsX(); iBin++){
    // Checks if one bin below the baseline is followed by one above the baseline, which could imply the start of a pulse.
    if((mPolarity*mWF->GetBinContent(iBin+1) > lb) && (mPolarity*mWF->GetBinContent(iBin) < lb)){ //Finds pulse starts
      if ((nrIntersects == 100)){
        mNPulse = 0;
        return 0;
      }
      rIntersects[nrIntersects] = iBin;
      nrIntersects++;
/*      if (flag==1){
        std::cout << "Diagnostics" << std::endl;
        std::cout << mWF->GetBinCenter(iBin) << " ";
      }*/
    }
    // Opposite of above, checking if a bin above the baseline is followed by one below, which could imply the end of the pulse.
    else if((mPolarity*mWF->GetBinContent(iBin+1) < lb) && (mPolarity*mWF->GetBinContent(iBin) > lb)){ //Finds pulse ends
      if ((nfIntersects == 100)){
        mNPulse = 0;
        return 0;
      }
      fIntersects[nfIntersects] = iBin;
      nfIntersects++;
     /* if (flag==1){
        std::cout << mWF->GetBinCenter(iBin) << std::endl << std::endl;
      }*/
    }
  }

  //Finds all local maxima that lie above the lower baseline                      
  for(int iBin = rIntersects[0]; iBin < fIntersects[nfIntersects-1]; iBin++){
    // May need to find improvement for this, as it flags ALL bins above the baseline and bounded by two bins of lower value.
    if((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin+1))) && (mPolarity*mWF->GetBinContent(iBin) > lb)){ //Bin larger than bins on either side and above lb
      // TEST for checking either side
      /*bool posPeak = true;
        for (int iBuf = buffer; iBuf > 0; iBuf--){
        if ( (mPolarity*(mWF->GetBinContent(iBin)) < mPolarity*(mWF->GetBinContent(iBin-iBuf))) ||
             (mPolarity*(mWF->GetBinContent(iBin)) < mPolarity*(mWF->GetBinContent(iBin+iBuf))) ){
          posPeak = false;
          break;
        }
      }
      if (posPeak==true){*/
      if ((nlMax == 1000)){ //To prevent array overflow for noise pulses
        mNPulse = 0;
        return 0;
      }
      lMax[nlMax] = iBin;
      if (mForMacro) mBins[iBin].islMax = true;
      nlMax++;
    //}
  }
    else if ((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) == mPolarity*(mWF->GetBinContent(iBin+1))) && (mPolarity*mWF->GetBinContent(iBin) > lb)){  //For cases when the top of a peak is flattened
      int jBin;
      // What the hell does this for..loop do?
      // Finds the next bin of equal value?...
      // The case to enter this elseif is if the values iBin+1 == iBin, so jBin should be equal to iBin+1 since it is initialized
      // with that value, then the next if statement checks if jBin-1 (i.e. iBin) is less than jBin...
      for (jBin = iBin+1; mPolarity*(mWF->GetBinContent(jBin)) == mPolarity*(mWF->GetBinContent(iBin)); jBin++){
      }
      if ((mPolarity*(mWF->GetBinContent(jBin-1)) > mPolarity*(mWF->GetBinContent(jBin)))){
      	for (int kBin = iBin; kBin < jBin; kBin++){
	        if ((nlMax == 1000)){
	          mNPulse = 0;
	          return 0;
	        }
	        lMax[nlMax] = kBin;
      	  if (mForMacro) mBins[kBin].islMax = true;
      	  nlMax++;
	      }
      	iBin = jBin-1;
      }
    }
  }

  int pulses[100][2];
  int nPulses = 0;
  int pulseMax [100];

  //Finds the regions made by pulses                                                                                               
  int iInt0;
  if((rIntersects[0] < fIntersects[0])) iInt0 = 0; // In case there is an offset
  else iInt0 = 1;
 

  for (int iInt = iInt0; iInt < nfIntersects; iInt++){
    bool hasMax = false;
    bool hasLargeMax = false;
    for (int iMax = 0; iMax < nlMax; iMax++){
      // Checks if the previously identified maximum is between a rising and falling edge, and is "large" (twice as large as the baseline)
      if((lMax[iMax] > rIntersects[iInt-iInt0]) && (lMax[iMax] < fIntersects[iInt]) && (mPolarity*mWF->GetBinContent(lMax[iMax]) > 2*lb)){
        hasLargeMax = true;
        nrIntersects++;
        break;
      }
      // Same as above, but without the "large" condition
      else if ((lMax[iMax] > rIntersects[iInt-iInt0]) && (lMax[iMax] < fIntersects[iInt])){
      	hasMax = true;
      }
    }

    // If the pulse was a maximum and was large, and the pulse width is above a value set from the command line, then 
    // record the width of the pulse
    if ((hasLargeMax) && ((mWF->GetBinCenter(fIntersects[iInt]) - mWF->GetBinCenter(rIntersects[iInt-iInt0])) >= minPulseWidth)){
      pulses[nPulses][0] = rIntersects[iInt-iInt0];
      pulses[nPulses][1] = fIntersects[iInt];
      nPulses++;
    }
     else if (hasMax){/*  Code for finding pulses which are small compared to the noise - not currently implemented
      int jInt;
      for (jInt = iInt+1; jInt < nfIntersects; jInt++){
	if ((mWF->GetBinCenter(fIntersects[jInt]) - mWF->GetBinCenter(fIntersects[jInt-1]) > minPulseWidth)){
	  jInt--;
	  break;
	}
      }
      int max = findSmallPulse(rIntersects[iInt-iInt0], fIntersects[jInt]);
      probablePeaks[nProbablePeaks] = max;
      if (mForMacro) mBins[max].isProbPeak = true;
      nProbablePeaks++;
      smallPulseRegions[nSmallPulseRegions][0] = rIntersects[iInt-iInt0];
      smallPulseRegions[nSmallPulseRegions][1] = fIntersects[jInt];
      nSmallPulseRegions++;
      iInt = jInt;*/
      }
  }
if (flag==1) std::cout << nPulses << std::endl;
  // Finds maxima of all pulses
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    pulseMax[iPulse] = pulses[iPulse][0];
    for (int iBin = pulses[iPulse][0]+1; iBin < pulses[iPulse][1]; iBin++){
      if ((mPolarity*mWF->GetBinContent(iBin) > mPolarity*mWF->GetBinContent(pulseMax[iPulse]))){
	      pulseMax[iPulse] = iBin;
      }
    }
    if (flag==1) {
      std::cout << "Diag for PulseMax" << std::endl;
      std::cout << "Center: " << mWF->GetBinCenter(pulseMax[iPulse]) << std::endl << std::endl;
    }
  }

  // Recheck rising edge for peaks
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    // Checks between the maximum and the rising intercept
    for (int iBin = mWF->FindBin(mWF->GetBinCenter(pulseMax[iPulse])-3e-9); iBin > mWF->FindBin(mWF->GetBinCenter(pulses[iPulse][0])); iBin--){
      if ((mWF->FindBin(mWF->GetBinCenter(pulseMax[iPulse])) - iBin > 6e-9/mWF->GetBinWidth(1)) && (mPolarity*mWF->GetBinContent(iBin) - mPolarity*mWF->GetBinContent(iBin-5e-9/mWF->GetBinWidth(1)) < 0)) break;
      bool preex = false;

      //Checks that the maximum has not already been found
      for (int iMax = 0; iMax < nlMax; iMax++){
  	    if ((mWF->FindBin(mWF->GetBinCenter(iBin)) == lMax[iMax]) || (mWF->FindBin(mWF->GetBinCenter(iBin+1)) == lMax[iMax])) preex = true;
      }
      
      // Accepts peak tops that are 1 or 2 bins wide
      if((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin+1))) && (mPolarity*mWF->GetBinContent(iBin) > lb) && !preex){
	      missedMax[nMissedMax] = iBin;
	      if (mForMacro) mBins[mWF->FindBin(mWF->GetBinCenter(iBin))].islMax = true;
	      nMissedMax++;
	    }
	    else if ((mWF->GetBinContent(iBin)==mWF->GetBinContent(iBin+1)) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin+2)))&& (mPolarity*mWF->GetBinContent(iBin) > lb) && !preex){ 
	      missedMax[nMissedMax] = iBin+1;
	      if (mForMacro) mBins[mWF->FindBin(mWF->GetBinCenter(iBin+1))].islMax = true;
	      nMissedMax++;
	    }
    }
  }

  //Identifies points that are likely peaks
  for (int iMax = 0; iMax < nlMax; iMax++){
    for (int iPulse = 0; iPulse < nPulses; iPulse++){
      // Checks for local maxima on the rising edge by comparing them to the pulse's max.
      if ((lMax[iMax] < pulses[iPulse][1]) && (lMax[iMax] > pulses[iPulse][0]) && (lMax[iMax] < pulseMax[iPulse])){
	      edge[iMax] = 0;
	      if (mForMacro) mBins[lMax[iMax]].edge = 0;
	      break;
      }
      // Checks for local maxima on the falling edge.
      else if ((lMax[iMax] < pulses[iPulse][1]) && (lMax[iMax] > pulses[iPulse][0]) && (lMax[iMax] >= pulseMax[iPulse])){
      	edge[iMax] = 1;
      	if (mForMacro) mBins[lMax[iMax]].edge = 1;
      	break;
      }
      else edge[iMax] = 2;
    }
  }

  // Checks that the change in amplitude a number of bins before and after a potential peak exceeds a threshold relative to the noice level
  int nBinsSlopeCheckRise = nSecSlopeCheckRise / mWF->GetBinWidth(2);
  int nBinsSlopeCheckFall = nSecSlopeCheckFall / mWF->GetBinWidth(2);
  if ((nBinsSlopeCheckRise == 0)) nBinsSlopeCheckRise++;
  if ((nBinsSlopeCheckFall == 0)) nBinsSlopeCheckFall++;
  for (int iMax = 0; iMax < nlMax; iMax++){
    // Checks if peak was assigned as local maxima on rising edge
    if ((edge[iMax] == 0)){ //Rising edge peaks
      double ampDiffRise = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]+nBinsSlopeCheckRise);
      double ampDiffFall = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]-nBinsSlopeCheckFall);
      ampDiffRise /= nBinsSlopeCheckRise;
      ampDiffFall /= nBinsSlopeCheckFall;
      if (mForMacro) mBins[lMax[iMax]].slopecheck = ampDiffRise / avgVar;
      
      if ((ampDiffRise / avgVar > diffAmpFactorRise) && (ampDiffFall / avgVar > diffAmpFactorFall) && (lMax[iMax] - probablePeaks[nProbablePeaks-1] != 1)){
      	probablePeaks[nProbablePeaks] = lMax[iMax];
	      edge[nProbablePeaks] = edge[iMax];
      	if (mForMacro) mBins[lMax[iMax]].isProbPeak = true;
      	nProbablePeaks++;
      }
    }
    else if ((edge[iMax] == 1)){ //Falling edge peaks
      double ampDiffFall = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]-nBinsSlopeCheckFall);
      double ampDiffRise;
      if ((mWF->GetBinContent(lMax[iMax])==mWF->GetBinContent(lMax[iMax]+1))){
	      ampDiffRise = mPolarity*mWF->GetBinContent(lMax[iMax]+1) - mPolarity*mWF->GetBinContent(lMax[iMax]+nBinsSlopeCheckRise+1);
      }
      else ampDiffRise = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]+nBinsSlopeCheckRise);
      ampDiffFall /= nBinsSlopeCheckFall;
      ampDiffRise /= nBinsSlopeCheckRise;
      if (mForMacro) mBins[lMax[iMax]].slopecheck = ampDiffFall / avgVar;
      if ((ampDiffFall / avgVar > diffAmpFactorFall) && (ampDiffRise / avgVar > diffAmpFactorRise) && ((lMax[iMax] - probablePeaks[nProbablePeaks-1] != 1) || (nProbablePeaks == 0))){
      	probablePeaks[nProbablePeaks] =lMax[iMax];
      	edge[nProbablePeaks] = edge[iMax];
      	if (mForMacro) mBins[lMax[iMax]].isProbPeak = true;
      	nProbablePeaks++;
      }
    }
  }

  //Repeats the process for the maxima that were caught on the recheck
  for (int iMax = 0; iMax < nMissedMax; iMax++){
    double ampDiffRise = mPolarity*mDataWF->GetBinContent(missedMax[iMax]) - mPolarity*mDataWF->GetBinContent(missedMax[iMax]+nBinsSlopeCheckRise);
    double ampDiffFall = mPolarity*mDataWF->GetBinContent(missedMax[iMax]) - mPolarity*mDataWF->GetBinContent(missedMax[iMax]-nBinsSlopeCheckFall);
    ampDiffRise /= nBinsSlopeCheckRise;
    ampDiffFall /= nBinsSlopeCheckFall;
    if (mForMacro) mBins[mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax]))].slopecheck = ampDiffRise / avgVar;
    if ((ampDiffFall / avgVar > diffAmpFactorFall) && (ampDiffRise / avgVar > diffAmpFactorRise) && (missedMax[iMax] - probablePeaks[nProbablePeaks-1] != 1)){
      probablePeaks[nProbablePeaks] = mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax]));
      edge[nProbablePeaks] = 0;
      if (mForMacro) mBins[mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax]))].isProbPeak = true;
      nProbablePeaks++;
    }
  }

  //Puts peaks in order                                                                                                                                                                                     
  int buffPeaks [100];
  int lowest = 0;
  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    if ((probablePeaks[iPeak] < probablePeaks[lowest])) lowest = iPeak;
  }
  buffPeaks[0] = probablePeaks[lowest];

  for (int iPeak = 1; iPeak < nProbablePeaks; iPeak++){
    for (int jPeak = 0; jPeak < nProbablePeaks; jPeak++){
      if ((probablePeaks[jPeak] > buffPeaks[iPeak-1])){
	      lowest = jPeak;
        break;
      }
    }
    for (int jPeak = 0; jPeak < nProbablePeaks; jPeak++){
      if ((probablePeaks[jPeak] < probablePeaks[lowest]) && (probablePeaks[jPeak] > buffPeaks[iPeak-1])) lowest=jPeak;
    }
    buffPeaks[iPeak] = probablePeaks[lowest];
  }

  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    probablePeaks[iPeak] = buffPeaks[iPeak];
  }
      

  //Calculates the approximate rise of a probable peak on the falling edge above the pulse.                               
  double probPeakRise[100];

  int preBin;
  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    for(preBin = probablePeaks[iPeak]-1; mPolarity*mWF->GetBinContent(preBin-1)<=mPolarity*mWF->GetBinContent(preBin) && preBin > 0; preBin--){
    }
      //preBin--;

    probPeakRise[iPeak] = mPolarity*mWF->GetBinContent(probablePeaks[iPeak]) - mPolarity*mWF->GetBinContent(preBin);
    if (mForMacro) mBins[probablePeaks[iPeak]].rise = probPeakRise[iPeak];
  }

   //Estimates certainty that a probable peak is a peak by comparing the ratio of the vertical prominence of the peak to the log of the horizontal separation from the previous peak against a threshold
  double peakiness[100];
  double peakiness2[100];
  double peakRise[100];
  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    bool isPulseMax = false;
    for (int iPulse = 0; iPulse < nPulses; iPulse++){
      if ((mWF->GetBinContent(probablePeaks[iPeak]) == mWF->GetBinContent(pulseMax[iPulse]))) isPulseMax = true; 
    }
    bool isFirst = true;
    for (int iPulse = 0; iPulse < nPulses; iPulse++){
      if ((probablePeaks[iPeak] < pulses[iPulse][1]) && (probablePeaks[iPeak] > pulses[iPulse][0]) && (nPeaks > 0)){
	      for (int jPeak = 0; jPeak < nPeaks; jPeak++){
	        if ((peaks[jPeak] > pulses[iPulse][0])){
      	    isFirst = false;
	          break;
	        }
	      }
      }
    }
    bool isSmall = false;
    for (int iSPR = 0; iSPR < nSmallPulseRegions; iSPR++){
      if ((probablePeaks[iPeak] < smallPulseRegions[iSPR][1]) && (probablePeaks[iPeak] >= smallPulseRegions[iSPR][0])) isSmall = true;
    }

     //Peaks that are pulse maxima, or the first in their pulse, automatically qualify as peaks
    if ((isPulseMax)) peakiness[iPeak] = -1.0;
    else if ((nProbablePeaks == 1)) peakiness[iPeak] = -1.0;
    else if ((isFirst) && (mPolarity*mWF->GetBinContent(probablePeaks[iPeak]) > 1.5*lb)) peakiness[iPeak] = -1.0;
    else if ((isSmall)) peakiness[iPeak] = -1.0;
    else{
      int prevPeak;
      for (prevPeak = iPeak-1; prevPeak >= 0; prevPeak--){
	      if ((peakiness[prevPeak]==-1.0) || (peakiness[prevPeak] > peakinessThresh)) break;
      }
      // Why is the "peakiness" related to two peaks?
      double horiz = 20+log(mWF->GetBinCenter(probablePeaks[iPeak]) - mWF->GetBinCenter(probablePeaks[prevPeak]));
      double vert = (probPeakRise[iPeak]/(mPolarity*mWF->GetBinContent(probablePeaks[prevPeak])));
      // Diagnositcs with flags
      /*if (flag==1) {
        std::cout << "Diagnostics: iPeak prevPeak" << std::endl;
        std::cout << mWF->GetBinCenter(probablePeaks[iPeak]) << " " << mWF->GetBinCenter(probablePeaks[prevPeak]) << std::endl;
        std::cout << "Peakiness: " << vert/horiz << std::endl << std::endl;
      }*/

      peakiness[iPeak] = vert / horiz;
    }
    if (mForMacro) mBins[probablePeaks[iPeak]].peakiness = peakiness[iPeak];

     //Decides finally upon what points are peaks based upon the 'peakiness' measurements above.
    if (((peakiness[iPeak] >= peakinessThresh) || (peakiness[iPeak] == -1.0)) && (mWF->GetBinContent(probablePeaks[iPeak]) != mWF->GetBinContent(peaks[nPeaks-1]))){
      peaks[nPeaks] = probablePeaks[iPeak];
      peakiness2[nPeaks] = peakiness[iPeak];
      peakRise[nPeaks] = probPeakRise[iPeak];
      if (mForMacro) mBins[probablePeaks[iPeak]].isPeak = true;
      if (mForMacro) mBins[peaks[nPeaks]].rise = peakRise[nPeaks];
      nPeaks++;
    }
  }

  //Places info about pulses into an EventData struct for each of access of macros
  mEventData.lb = mPolarity*lb;
  mEventData.nPulse = nPulses;
  mEventData.nPeak = nPeaks;
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    mEventData.pulseBounds[iPulse][0] = mWF->GetBinCenter(pulses[iPulse][0]);
    mEventData.pulseBounds[iPulse][1] = mWF->GetBinCenter(pulses[iPulse][1]);
    mEventData.pulseBounds[iPulse][2] = mWF->GetBinCenter(pulseMax[iPulse]);
  }
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    mEventData.peaks[iPeak] = mWF->GetBinCenter(peaks[iPeak]);
    mEventData.peakiness[iPeak] = peakiness2[iPeak];
    mEventData.peakBins[iPeak] = peaks[iPeak];
  }
  mEventData.nlMax = nlMax;

   // >>> Calculate the charge and width of each pulse
  double charges[100];
  int widths[100];
  int pulseNPeaks[100];
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    charges[iPulse] = 0;
    for (int iBin = pulses[iPulse][0]; iBin <= pulses[iPulse][1]; iBin++){
      charges[iPulse] += (mPolarity*mWF->GetBinContent(iBin) - mBmu)*mWF->GetBinWidth(iBin);
    }
    widths[iPulse] = pulses[iPulse][1] - pulses[iPulse][0];
  }

  //Calculates the number of peaks in each pulse, and assigns each peak to the pulse it lies on
  int whichPulse[100];
  for (int i = 0; i < 100; i++) whichPulse[i] = -1;
  int peakCount[100];
  for (int iPulse = 0; iPulse < nPulses; iPulse++) peakCount[iPulse] = 0;
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    for (int iPulse = 0; iPulse < nPulses; iPulse++){
      if((peaks[iPeak] > pulses[iPulse][0]) && (peaks[iPeak] < pulses[iPulse][1])){
	      whichPulse[iPeak] = iPulse;
      	peakCount[iPulse]++;
      	break;
      }
    }
  }
  
  //Computes the average of bins within 50ns of each peak
  double fiftyNanoAvg[100];
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    fiftyNanoAvg[iPeak] = 0;
    for (int iBin = peaks[iPeak]; iBin < peaks[iPeak] + (50e-9 / mWF->GetBinWidth(peaks[iPeak])); iBin++){
      fiftyNanoAvg[iPeak] += mWF->GetBinContent(iBin);
    }
    fiftyNanoAvg[iPeak] /= (50e-9 / mWF->GetBinWidth(peaks[iPeak]));
  }
  
  
  //Sticks all computed information into Pulse struct for PulseFinder to access
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    if((iPeak<mNPulseMax) && (whichPulse[iPeak] != -1)){
      mPulse[iPeak].mTime=mWF->GetBinCenter(peaks[iPeak]);
      mPulse[iPeak].mAbsAmp=mPolarity*mWF->GetBinContent(peaks[iPeak]);
      mPulse[iPeak].mAmp=mWF->GetBinContent(peaks[iPeak]);
      mPulse[iPeak].mBaseline=mBmu;
      mPulse[iPeak].mQ=charges[whichPulse[iPeak]];
      mPulse[iPeak].mWidth=widths[whichPulse[iPeak]];
      mPulse[iPeak].m50NanoAvg=fiftyNanoAvg[iPeak];
      mPulse[iPeak].mPeakRise=peakRise[iPeak];
      mPulse[iPeak].mAvgVar = avgVar;
      mPulse[iPeak].mPeakiness = peakiness2[iPeak];
    }
  }
  mNPulse = nPeaks;
  return 1;
}

int WaveformProcessor::findSmallPulse(int startingBin, int endingBin){
  //Check for noise
  int nlMax = 0;
  int lMax [1000];
  for (int iBin = startingBin; iBin <= endingBin; iBin++){
    if((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin+1)))){
      lMax[nlMax] = iBin;
      nlMax++;
    }
    else if ((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) == mPolarity*(mWF->GetBinContent(iBin+1)))){
      int jBin;
      for (jBin = iBin+1; mPolarity*(mWF->GetBinContent(jBin)) == mPolarity*(mWF->GetBinContent(iBin)); jBin++){
      }
      if ((mPolarity*(mWF->GetBinContent(jBin-1)) > mPolarity*(mWF->GetBinContent(jBin)))){
        for (int kBin = iBin; kBin < jBin; kBin++){
          lMax[nlMax] = kBin;
          nlMax++;
        }
        iBin = jBin-1;
      }
    }

  }
  
  // Takes as peak either the largest peak in the front half or the largest in the back half of the pulse
  // Only takes the one in the back half if it is > 1.5 times the height of the highest in front
  int highestFront = 0;
  int highestBack = 0;
  for (int iMax = 1; iMax < nlMax; iMax++){
    if ((lMax[iMax] <= ((startingBin+endingBin)/2)) && (mPolarity*(mWF->GetBinContent(lMax[iMax])) > mPolarity*(mWF->GetBinContent(lMax[highestFront])))) highestFront = iMax;
    else if ((lMax[iMax] > ((startingBin+endingBin)/2)) && (mPolarity*(mWF->GetBinContent(lMax[iMax])) > mPolarity*(mWF->GetBinContent(lMax[highestBack])))) highestBack = iMax;
  }
  int max;
  if ((mPolarity*(mWF->GetBinContent(lMax[highestBack])) > 1.5 * mPolarity*(mWF->GetBinContent(lMax[highestFront])))) max = lMax[highestBack];
  else max = lMax[highestFront];
  return max;
}

// Takes peaks and puts them in TGraph
TGraph* WaveformProcessor::getPulseGraph(){
    double* tTime = new double[mNPulse];
    double* tAmp = new double[mNPulse];
    for(int iPulse=0; iPulse<mNPulse; iPulse++){
        tTime[iPulse]=mPulse[iPulse].mTime;
        tAmp[iPulse]=mPulse[iPulse].mAmp;
    }
    TGraph* tG = new TGraph(mNPulse,tTime,tAmp); //TGraph(number of points, xArray, yArray)
    delete[] tTime;
    delete[] tAmp;
    return tG;
}

//adding test for exponential processing.
void WaveformProcessor::smoothWaveform(double lenNs, int flag){
  //Boxcar method
  int nBins=mWF->GetNbinsX();
  double secPerBin=mWF->GetBinWidth(1);
  double len=lenNs*1e-09/secPerBin;
  double value=0;
  
  if (flag==0){
    //Boxcar method
    if (!mSmoothedWF)
      mSmoothedWF = new Waveform(nBins-len*2,mWF->GetBinCenter(0)+mWF->GetBinWidth(1)*(len),mWF->GetBinCenter(nBins)-mWF->GetBinWidth(1)*len,"smoothed");
    for (int j=0;j<=len*2;j++)  //Calculate smoothed value for first bin
      value+=mWF->GetBinContent(j);
    mSmoothedWF->SetBinContent(0,value/(len*2.+1.));  //Set smoothed value for first bin
    for (int i=len+1;i<nBins-len;i++){  //Caculate smoothed values for other bins
      value-=mWF->GetBinContent(i-len-1); //Remove first bin from previous calculation
      value+=mWF->GetBinContent(i+len); //Add last bin to current calculation
      mSmoothedWF->SetBinContent(i-len,value/(len*2.+1.));
    }
  }

  else if (flag==1){
    //Exponential method
    double alpha=0.8; //weighting of smoothing; increase for less weight to previous values.
    if (!mSmoothedWF)
      mSmoothedWF = new Waveform(nBins,mWF->GetBinCenter(0)-mWF->GetBinWidth(1)*(len/2.),mWF->GetBinCenter(nBins)-mWF->GetBinWidth(1)*(len/2.),"smoothed");
      //Waveform(nBins-len*2,mWF->GetBinCenter(0)+mWF->GetBinWidth(1)*(len),mWF->GetBinCenter(nBins)-mWF->GetBinWidth(1)*len,"smoothed");

    value = mWF->GetBinContent(0); //Initialize the first value
    mSmoothedWF->SetBinContent(0,value/(len*2.+1.));  //Set smoothed value for first bin
    for (int i=len+1;i<nBins-len;i++){ //Calculate the weighting and add to the new value
      value = alpha*mWF->GetBinContent(i) + (1.-alpha)*value;
      mSmoothedWF->SetBinContent(i+len,value);
    }
  }

  else{
    // Triangle running average
    if (!mSmoothedWF)  mSmoothedWF = new Waveform(nBins,mWF->GetBinCenter(0)-mWF->GetBinWidth(1)*(len/2.),mWF->GetBinCenter(nBins)-mWF->GetBinWidth(1)*(len/2.),"smoothed");

    int weight=pow(flag+1,2);
//    for (int i=1;i<=flag;i++) weight+=2*i;
//    weight+=flag+1;

    for (int iBin=0;iBin<flag;iBin++){
      value=mWF->GetBinContent(iBin);
      mSmoothedWF->SetBinContent(iBin,value);
    }

    for (int iBin=nBins-flag;iBin==nBins;iBin++){
      value=mWF->GetBinContent(iBin);
      mSmoothedWF->SetBinContent(iBin,value);
    }

    for (int iBin=flag;iBin<nBins-flag;iBin++){
      value=0;
      for (int jBin=iBin-flag; jBin<=iBin+flag; jBin++){
        value+=mWF->GetBinContent(jBin)*(flag-fabs(iBin-jBin)+1);
      }
//      std::cout << mWF->GetBinContent(iBin) << " " << value/weight << " " << weight << std::endl;
      mSmoothedWF->SetBinContent(iBin,value/weight);
    }
  }

  mDataWF=mWF;
  mWF=mSmoothedWF;
}

void WaveformProcessor::calcDeriv(int binSep){  //Analogous to processBaseline, but for derivative                                                                                                           
  if (mHDeriv[binSep]) mHDeriv[binSep]->Delete();
  if (mDerivFit[binSep]) mDerivFit[binSep]->Delete();
  int nbins=mWF->GetNbinsX();
  char ID[20];
  sprintf(ID,"derivHist%i",binSep);
  mHDeriv[binSep] = new TH1D(ID,ID,120,mWF->GetMinimum(),mWF->GetMaximum());
  for (int i=1;i<=nbins-binSep;i++)
    mHDeriv[binSep]->Fill(mWF->GetBinContent(i+binSep)-mWF->GetBinContent(i));
  sprintf(ID,"derivFit%i",binSep);
  mDerivFit[binSep] = new TF1(ID,"gaus");
  mHDeriv[binSep]->Fit(mDerivFit[binSep],"Q");
  mDerivmu[binSep]=mDerivFit[binSep]->GetParameter(1);
  mDerivRMS[binSep]=mDerivFit[binSep]->GetParameter(2);
}


TH1D* WaveformProcessor::getDeriv(int binSep){  //Generates derivative waveform               
  if (mDeriv[binSep]) return mDeriv[binSep];
  int nbins=mWF->GetNbinsX();
  char ID[20];
  sprintf(ID,"deriv%i",binSep);
  if (binSep%2==1)
    mDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mWF->GetBinCenter(binSep/2.+0.5),mWF->GetBinCenter(nbins-binSep/2.+0.5));
  else
    mDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mWF->GetBinLowEdge(binSep/2.+1),mWF->GetBinLowEdge(nbins-binSep/2.+1));
  for (int i=1;i<=nbins-binSep;i++)
    mDeriv[binSep]->SetBinContent(i,mWF->GetBinContent(i+binSep)-mWF->GetBinContent(i));

  return mDeriv[binSep];
}

TH1D* WaveformProcessor::getSecDeriv(int binSepFirstDeriv,int binSep){  //Generates derivative waveform                                                                                    
  getDeriv(binSepFirstDeriv);
 if (mSecDeriv[binSep]) return mSecDeriv[binSep];
 int nbins=mDeriv[binSepFirstDeriv]->GetNbinsX();
 char ID[20];
 sprintf(ID,"deriv%i",binSep);
 if (binSep%2==1)
   mSecDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mDeriv[binSepFirstDeriv]->GetBinCenter(binSep/2.+0.5),mDeriv[binSepFirstDeriv]->GetBinCenter(nbins-binSep/2.+0.5));
 else
   mSecDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mDeriv[binSepFirstDeriv]->GetBinLowEdge(binSep/2.+1),mDeriv[binSepFirstDeriv]->GetBinLowEdge(nbins-binSep/2.+1));
 for (int i=1;i<=nbins-binSep;i++)
   mSecDeriv[binSep]->SetBinContent(i,mDeriv[binSepFirstDeriv]->GetBinContent(i+binSep)-mDeriv[binSepFirstDeriv]->GetBinContent(i));

 return mSecDeriv[binSep];
}


//----------------------------

double WaveformProcessor::findMin(int start, int end, int verbose){ //**Will find maximum of positive pulses**
  if (end<0)  //if end is given as <0, use entire histogram
    end=mWF->GetNbinsX();
  
  //variables
  double minHeight=100000;
  int minLoc=-1;
  
  //loop through histogram
  for (int i=start;i<end+1;i++)
    if (mWF->GetBinContent(i)*mPolarity*-1<minHeight){
      minHeight=mWF->GetBinContent(i)*mPolarity*-1;
      minLoc=i;
    }
  
  if (verbose>1)
    std::cout << "minLoc=" << minLoc << "\n";
  return minHeight;
}
//----------------------------
void WaveformProcessor::findClearPulses(TNtuple* ntp, int start, int end, double level, int verbose){
  mEv++;
  
  //variables
  if (start<0) start=1;
  if (end<0) end=mWF->GetNbinsX();
  double tCharge=0.;
  int iPulseStart=-1;
  int iPulseEnd=-1;
  double pulseWidth=-1.;
  double maxHeight=-1;
  int maxLoc=-1;
  int cCharge;
  double secPerBin=mWF->GetBinWidth(1);
  
  if (verbose>1){
    std::cout << "mBRMS=" << mBRMS << std::endl;
    std::cout << "mBmu=" << mBmu << std::endl;
  }
  
  //convert risetime and falltime from seconds to bins
  int nBinRiseTime = round(32e-09/mWF->GetBinWidth(1));
  int nBinFallTime = round(120e-09/mWF->GetBinWidth(1));
    
  //loop through histogram
  for (int i=start;i<end;i++){
    
    if ((mWF->GetBinContent(i)-mBmu)*mPolarity>mBRMS*level){  //Check amplitude; proceed below if amplitude above threshold
      if (verbose>2)
        std::cout << "i=" << i << std::endl;
      
      for (int j=i;mPolarity*mWF->GetBinContent(j)>mBmu*mPolarity && j>start;j--){ //Find beginning of pulse
        if ((mWF->GetBinContent(j)-mBmu)*mPolarity>maxHeight){ //record maximum point in peak
          maxHeight=(mWF->GetBinContent(j)-mBmu)*mPolarity;
          maxLoc=j;
        }
        iPulseStart=j;
      }
      if (verbose>3){
        std::cout << "(mWF->GetBinContent(maxLoc)-mBmu)*mPolarity=" << (mWF->GetBinContent(maxLoc)-mBmu)*mPolarity << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      for (int j=i;mPolarity*mWF->GetBinContent(j)>mBmu*mPolarity && j<end;j++){ //Find end of pulse
        if ((mWF->GetBinContent(j)-mBmu)*mPolarity>maxHeight){ //record maximum point in peak
          maxHeight=(mWF->GetBinContent(j)-mBmu)*mPolarity;
          maxLoc=j;
        }
        iPulseEnd=j;
      }
      if (verbose>3){
        std::cout << "(mWF->GetBinContent(maxLoc)-mBmu)*mPolarity=" << (mWF->GetBinContent(maxLoc)-mBmu)*mPolarity << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      
      pulseWidth=iPulseEnd-iPulseStart;
      
      // >>> Calculate the charge
      for (int j=maxLoc-nBinRiseTime; j<=maxLoc+nBinFallTime; j++){
        tCharge+=(mWF->GetBinContent(j)-mBmu)*mPolarity;
      }
      
      if (verbose>0 && verbose<3){
        std::cout << "pulseWidth=" << pulseWidth << std::endl;
        std::cout << "iPulseStart=" << iPulseStart << std::endl;
        std::cout << "iPulseEnd=" << iPulseEnd << std::endl;
        std::cout << "nBinRiseTime+nBinFallTime=" << nBinRiseTime+nBinFallTime<< std::endl;
        std::cout << "mBRMS=" << mBRMS << std::endl;
        std::cout << "mBmu=" << mBmu << std::endl;
        std::cout << "tCharge=" << tCharge << std::endl;
        std::cout << "iPulseStart*secPerBin*1e9=" << iPulseStart*secPerBin << std::endl;
        std::cout << "maxLoc=" << maxLoc << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      ntp->Fill(mEv,iPulseStart*secPerBin,maxHeight,tCharge,pulseWidth,getTriggerTime());
      i+=nBinRiseTime+nBinFallTime;
      //reset variables for next pulse candidate
      tCharge=0.;
      iPulseStart=-1;
      iPulseEnd=-1;
      pulseWidth=-1.;
      maxHeight=-1;
      maxLoc=-1;
    }
  }
}


