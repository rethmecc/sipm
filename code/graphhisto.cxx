/*******************************************************************************
* graphhisto.cxx
*
* Description:
* Reads a .txt file and produces a graph of the histogram information
* with the potential to fit gaussians to the peaks and itegrate
*
* History:
* 2015/02/26  Initial file (Erin Tonita, erin.tonita@yahoo.ca)
*******************************************************************************/

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <string>
#include <fstream>
using namespace std;

// Loading File Attempt

plot "/home/exo/Erin/DataWinter2015/F1histoarea_BS_-25_mppc_feb25_100000.txt" using 1:2

// Ideas

/*[]TF1 f1("func1","sin(x)/x",0,10)
[]f1.Draw()

[]THD1 h1("hist1","Histogram from a gaussian",100,-3,3)
[]h1.FillRandom("gaus",10000)
[]h1.Draw()
[]h1.Draw("e")
[]TF1 myfunc("myfunc","gaus",0,3)
[]myfunc.SetParameters(10.,1.0,0.5) //gaussian has 3 parameters
[]myfunc.Draw()
[]h2.FillRandom("myfunc",10000)*/




