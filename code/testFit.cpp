#include <sys/stat.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1D.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"

// set the values of the blind window and the cut for the pulsefinder
const double gapUpperBound = 12.e3,
gapLowerBound = 9.5e3,
GapStart = gapLowerBound,
GapEnd = gapUpperBound,
Starttime = 10.;
double DeadTime = 10.;//ns

double funcDN(double* x, double* p){
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart){
        return p[0]*(1.-p[1])*exp(-p[0]*(x[0]-DeadTime));
    }
    else{
        if(x[0]<GapEnd) return 0.;
        return p[0]*(1.-p[1])*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
    }
}
double funcDNAP2WGapRec(double* x, double* p){
  // p[0] = dark noise rate
  // p[1] = <nap1>
  // p[2] = tauAp1
  // p[3] = <nap2>
  // p[4] = tauAp2
  // p[5] = tauRec
  static int postGapCalc;
  static double noAP1PostGapCorr;
  static double noAP2PostGapCorr;
  if(x[0]<DeadTime) return 0.;
  if(x[0]<GapStart || x[0]>GapEnd){

    double noAP1=1.;
    double singleAP1=0.;
    double Poisson;
    double expTau1 = exp(-x[0]/p[2]);
    double expDeadTime1 = exp(-DeadTime/p[2]);
    double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
    double expTauRec1 = exp(-x[0]/tauRecEff1);
    double expDeadTimeRec1 = exp(-DeadTime/tauRecEff1);
    double factPar1 = 1.;
    double noAP2=1.;
    double singleAP2=0.;
    double expTau2 = exp(-x[0]/p[4]);
    double expDeadTime2 = exp(-DeadTime/p[4]);
    double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
    double expTauRec2 = exp(-x[0]/tauRecEff2);
    double expDeadTimeRec2 = exp(-DeadTime/tauRecEff2);
    double factPar2 = 1.;
    for(int iAP=1; iAP<5; iAP++) {
      Poisson=TMath::Poisson(iAP,p[1]);
      singleAP1 += Poisson*(iAP==1? 1. : (1.-factPar1));
      factPar1*=(expDeadTime1-expTau1-
         tauRecEff1/p[2]*(expDeadTimeRec1-expTauRec1));
      noAP1 -= Poisson*factPar1;
      Poisson=TMath::Poisson(iAP,p[3]);
      singleAP2 += Poisson*(iAP==1? 1. : (1.-factPar2));
      factPar2*=(expDeadTime2-expTau2-
         tauRecEff2/p[4]*(expDeadTimeRec2-expTauRec2));
      noAP2 -= Poisson*factPar2;
    }
    singleAP1*=expTau1/p[2]*(1.-exp(-x[0]/p[5]));
    singleAP2*=expTau2/p[4]*(1.-exp(-x[0]/p[5]));
    if(x[0]<GapStart){
      postGapCalc=0;
      return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
    }
    else{ //Subrtract out the
      if(postGapCalc==0){
    postGapCalc=1;
    double Poisson;
    noAP1PostGapCorr=0.; // probability that AP1 is in the gap
    double expTauStart1 = exp(-GapStart/p[2]);
    double expTauEnd1 = exp(-GapEnd/p[2]);
    double expDeadTime1 = exp(-DeadTime/p[2]);
    double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
    double expTauRecStart1 = exp(-GapStart/tauRecEff1);
    double expTauRecEnd1 = exp(-GapEnd/tauRecEff1);
    double expDeadTimeRec1 = exp(-DeadTime/tauRecEff1);
    double factPar1 = 1.;
    noAP2PostGapCorr=0.;
    double expTauStart2 = exp(-GapStart/p[4]);
    double expTauEnd2 = exp(-GapEnd/p[4]);
    double expDeadTime2 = exp(-DeadTime/p[4]);
    double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
    double expTauRecStart2 = exp(-GapStart/tauRecEff2);
    double expTauRecEnd2 = exp(-GapEnd/tauRecEff2);
    double expDeadTimeRec2 = exp(-DeadTime/tauRecEff2);
    double factPar2 = 1.;
    for(int iAP=1; iAP<5; iAP++) {
      Poisson=TMath::Poisson(iAP,p[1]);
      factPar1*=(expTauStart1-expTauEnd1-
             tauRecEff1/p[2]*(expTauRecStart1-expTauRecEnd1));
      noAP1PostGapCorr += Poisson*factPar1;
      Poisson=TMath::Poisson(iAP,p[3]);
      factPar2*=(expTauStart2-expTauEnd2-
             tauRecEff2/p[4]*(expTauRecStart2-expTauRecEnd2));
      noAP2PostGapCorr += Poisson*factPar2;
    }
    cout << x[0] << " " << noAP1 << " " << noAP1PostGapCorr << " "
         << noAP2 << " " << noAP2PostGapCorr << std::endl;
      }
      noAP1+=noAP1PostGapCorr;
      noAP2+=noAP2PostGapCorr;
      return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart)*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2));
    }
  }
  else{
    return 0.;
  }
}


double funcDNAP2WGapRecSimple(double* x, double* p){
  // p[0] = dark noise rate
  // p[1] = <nap1>
  // p[2] = tauAp1
  // p[3] = <nap2>
  // p[4] = tauAp2
  // p[5] = tauRec
  //static int postGapCalc;
  //static double noAP1PostGapCorr;
  //static double noAP2PostGapCorr;
  if(x[0]<DeadTime) return 0.;
  if(x[0]<GapStart || x[0]>GapEnd){
    double singleAP1 =p[1]/p[2]*exp(-x[0]/p[2])*(1.-exp(-x[0]/p[5]));
    double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
    double noAP1 = 1.- p[1]*(exp(-DeadTime/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-DeadTime/tauRecEff1)-exp(-x[0]/tauRecEff1)));
    double singleAP2 =p[3]/p[4]*exp(-x[0]/p[4])*(1.-exp(-x[0]/p[5]));
    double tauRecEff2 = p[2]*p[5]/(p[2]+p[5]);
    double noAP2 = 1.- p[3]*(exp(-DeadTime/p[4])-exp(-x[0]/p[2])-tauRecEff2/p[4]*(exp(-DeadTime/tauRecEff2)-exp(-x[0]/tauRecEff2)));
    if(x[0]<GapStart){
      //postGapCalc=-1;
      return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
    }
    else{ //Subtract out the
      //if(postGapCalc<=0){
    //postGapCalc=1;
      double noAP1PostGapCorr= p[1]*(exp(-GapStart/p[2])-exp(-GapEnd/p[2])-exp(-x[0]/p[2])-tauRecEff1/p[2]*(exp(-GapStart/tauRecEff1)-exp(-GapEnd/tauRecEff1)));
      double noAP2PostGapCorr= p[3]*(exp(-GapStart/p[4])-exp(-GapEnd/p[4])-tauRecEff2/p[4]*(exp(-GapStart/tauRecEff2)-exp(-GapEnd/tauRecEff2)));
    //cout << x[0] << " "
    //   << singleAP1 << " " << noAP1 << " " << noAP1PostGapCorr << " "
    //   << singleAP2 << " " << noAP2 << " " << noAP2PostGapCorr << endl;
    //}
      noAP1+=noAP1PostGapCorr;
      noAP2+=noAP2PostGapCorr;
      return exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
    }
  }
  else{
    return 0.;
  }
}
/**
 *
 * Fit function to extract the darknoise rate and the afterpulsingprobability
 * Requieres normalized data
 */
// par[0] = tau_n, par[1] = tau_ap, par[2] = P_ap
double fit(double *x, double *par)
{
    if(par[0] != 0 && par[1] != 0)
        return (((1.0 - par[2] * (1.0 - exp(-x[0]/par[1]))) * exp(-x[0]/par[0])/par[0])+ (par[2]/par[1] * exp(-x[0]/par[0]) * exp(-x[0]/par[1])));
    else return 1e-5;
}

// par[0] = tau_n
// par[1] = number of pulses
// par[2+2i] = tau_ap(i)
// par[3+2i] = P_ap(i)
double multiple_tc(double *x, double *par)
{
    double p_p = 0;
    double integral_pp =0;
    // n afterpulse tc's
    for(int tc=0; tc < par[1]; tc++)
    {
        if(par[2+2*tc] != 0)
        {
            p_p   += par[3+2*tc]/par[2+2*tc] *exp(-x[0]/par[2+2*tc]);
            integral_pp += par[3+2*tc]*(1- exp(-x[0]/par[2+2*tc]));
        }
        else
        {
            cerr << "wrong fit input" << endl;
        }
    }
    // darknoise
    double p_n = 0, integral_pn = 0;
    if(par[0]!=0)
    {

        p_n = ((exp(-x[0]/par[0]))/par[0]);
        integral_pn = (1 - exp(-x[0]/par[0]));
    }
    return (p_n * (1-integral_pp) +(1- integral_pn) * p_p);
}
// par[0] = tau_n
// par[1] = number of pulses
// par[2+2i] = tau_ap(i)
// par[3+2i] = P_ap(i)
double multiple_tc_gap(double *x, double *par)
{
    double p_p = 0;
    double integral_pp =0;
    // n afterpulse tc's
    for(int tc=0; tc < par[1]; tc++)
    {
        if(par[2+2*tc] != 0)
        {
            p_p   += par[3+2*tc]/par[2+2*tc] *exp(-x[0]/par[2+2*tc]);
            integral_pp += par[3+2*tc]*(1- exp(-x[0]/par[2+2*tc]));
        }
        else
        {
            cerr << "wrong fit input" << endl;
        }
    }
    // darknoise
    double p_n = 0, integral_pn = 0;
    if(par[0]!=0)
    {
        if(x[0] < gapLowerBound && x[0]>Starttime)
        {
            p_n = ((exp(-x[0]/par[0]))/par[0]);
            integral_pn = (1 - exp(-x[0]/par[0]));
        }
        else if(x[0]>gapUpperBound)
        {
            p_n = ((exp((-(x[0]-(gapUpperBound-gapLowerBound)))/par[0]))/par[0]);
            integral_pn = (1 -exp((-(x[0]-(gapUpperBound-gapLowerBound)))/par[0]));

        }
        else{
            p_n = 0;
            integral_pn = 1;
        }
    }

    return (p_n * (1-integral_pp) +(1- integral_pn) * p_p);
}

// par[0] = 1/tau, par[1] = normalization
double norm_exp(double *x, double *par)
{

    if(par[0]!=0)
    {
        // takes the gap into account!
        if(x[0] < gapLowerBound && x[0]>Starttime)
            return (par[1]*exp(-x[0]*par[0]));
        else if(x[0]>gapUpperBound)
            return (par[1]*exp((-(x[0]-(gapUpperBound-gapLowerBound)))*par[0]));
        else return 0;
    }
    else return 1;
}
double funcDNAP2Rec(double* x, double* p){
    // p[0] = dark noise rate
    // p[1] = <nap1>
    // p[2] = tauAp1
    // p[3] = <nap2>
    // p[4] = tauAp2
    // p[5] = tauRec
    if(x[0]<DeadTime) return 0.;
    if(x[0]<GapStart){
        double noAP1=1.;
        double singleAP1=0.;
        double Poisson;
        double expTau1 = exp(-x[0]/p[2]);
        double expDeadTime1 = exp(-DeadTime/p[2]);
        double tauRecEff1 = p[2]*p[5]/(p[2]+p[5]);
        double expTauRec1 = exp(-x[0]/tauRecEff1);
        double expDeadTimeRec1 = exp(-DeadTime/tauRecEff1);
        double factPar1 = 1.;
        double noAP2=1.;
        double singleAP2=0.;
        double expTau2 = exp(-x[0]/p[4]);
        double expDeadTime2 = exp(-DeadTime/p[4]);
        double tauRecEff2 = p[4]*p[5]/(p[4]+p[5]);
        double expTauRec2 = exp(-x[0]/tauRecEff2);
        double expDeadTimeRec2 = exp(-DeadTime/tauRecEff2);
        double factPar2 = 1.;
        for(int iAP=1; iAP<5; iAP++) {
            Poisson=TMath::Poisson(iAP,p[1]);
            singleAP1 += Poisson*(iAP==1? 1. : (1.-factPar1));
            factPar1*=(expDeadTime1-expTau1-
                       tauRecEff1/p[2]*(expDeadTimeRec1-expTauRec1));
            noAP1 -= Poisson*factPar1;
            Poisson=TMath::Poisson(iAP,p[3]);
            singleAP2 += Poisson*(iAP==1? 1. : (1.-factPar2));
            factPar2*=(expDeadTime2-expTau2-
                       tauRecEff2/p[4]*(expDeadTimeRec2-expTauRec2));
            noAP2 -= Poisson*factPar2;
        }
        singleAP1*=expTau1/p[2]*(1.-exp(-x[0]/p[5]));
        singleAP2*=expTau2/p[4]*(1.-exp(-x[0]/p[5]));
        return exp(-p[0]*(x[0]-DeadTime))*(noAP1*noAP2*p[0]+noAP2*singleAP1+noAP1*singleAP2);
    }
    else{// assume no after-pulse after gap
        if(x[0]<GapEnd) return 0.;
        return (1.-p[1])*p[0]*exp(-p[0]*(x[0]-DeadTime-GapEnd+GapStart));
    }
}
// just plotting helpers
void PlotDN_Ketek()
{
    c1 = new TCanvas("c1","Ketek Dark Noise Rate",200,10,700,500);
    Double_t x[5]  = {25.92, 26.42, 26.92, 27.42, 27.92};
    Double_t y[5]  = {15.21, 22.68, 28.74, 43.51, 52.54};
    Double_t ex[5] = {0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t ey[5] = {4e-11, 6e-11, 9e-11, 16e-11, 18e-11};
    TGraphErrors *gr = new TGraphErrors(5,x,y,ex,ey);
    gr->SetTitle("Ketek Dark Noise Rate");
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Operating Voltage [V]");
    gr->GetYaxis()->SetTitle("Dark Noise Rate [1/s]");

}
void PlotXT_Ketek()
{
    c1 = new TCanvas("c1","Ketek Crosstalk Probability",200,10,700,500);
    Double_t x[5]  = {25.92, 26.42, 26.92, 27.42, 27.92};
    Double_t y[5]  = {4.951, 7.450, 10.61, 14.51, 13.2};
    Double_t ex[5] = {0.05, 0.05, 0.05, 0.05, 0.05};
    Double_t ey[5] = {5.3e-2, 6.5e-2, 8.1e-2, 9.9e-2, 9.26e-2};
    TGraphErrors *gr = new TGraphErrors(5,x,y,ex,ey);
    gr->SetTitle("");
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle("Operating Voltage [V]");
    //gr->GetXaxis()->SetTitleFont(6);
    //gr->GetXaxis()->SetTitleOffset(0);
    gr->GetYaxis()->SetTitle("Crosstalk probability [%]");

}

void testFit()
{
    TCanvas *c = new TCanvas("c","c",900,600);
    c->cd();
    TFile *f = new TFile("test.root");
    TH1D * h =  (TH1D*) f->Get("TimeDist");

    c->SetLogx(1);
    c->SetLogy(1);
    c->SetGridx(1);
    c->SetGridy(1);

    cout << "expo tail fit" << endl;
    TF1* expo = new TF1("expo", norm_exp, 1e3, 1e6,2);
    expo->SetParameter(0,1e4);
    expo->SetParameter(1,1e-6);
    h->Draw("");
    h->Fit(expo,"R");
    gStyle->SetOptStat(1);


    cout << "long tc fit" << endl;
    //    // second stage fit the long timeconstant
    TF1 *finalFit= new TF1("finalFit", multiple_tc,30,5e3, 4);
    finalFit->FixParameter(0,1/expo->GetParameter(1));
    finalFit->FixParameter(1,1);
    finalFit->SetParameter(2,9e2);
    finalFit->SetParameter(3,0.06);

    h->Fit(finalFit,"R");
    h->Draw("");
    expo->SetLineColor(1);
    expo->Draw("same");
    c->SaveAs("test2.pdf");
    c->Clear();

    // third stage: fit the short time constants:
    cout << "short tc fit" << endl;
    TF1 *lastFit = new TF1("lastfit", multiple_tc,10,1e3,6);
    lastFit->FixParameter(0,finalFit->GetParameter(0));
    lastFit->FixParameter(1,2);
    lastFit->FixParameter(2,finalFit->GetParameter(2));
    lastFit->FixParameter(3,finalFit->GetParameter(3));
    lastFit->SetParameter(4,20);
    lastFit->SetParameter(5,0.05);

    h->Fit(lastFit,"R");
    lastFit->SetRange(10,1e6);

    c->Clear();
    h->Draw("");
    TF1 *drawFit = new TF1("drawFit", multiple_tc_gap,10,1e6,6);
    drawFit->FixParameter(0,lastFit->GetParameter(0));
    drawFit->FixParameter(1,lastFit->GetParameter(1));
    drawFit->FixParameter(2,lastFit->GetParameter(2));
    drawFit->FixParameter(3,lastFit->GetParameter(3));
    drawFit->FixParameter(4,lastFit->GetParameter(4));
    drawFit->FixParameter(5,lastFit->GetParameter(5));
    drawFit->Draw("same");

    lastFit->SetLineWidth(0.1);
    c->SaveAs("test3.pdf");

    // fabrice final fit:
    TF1 * DNAP2Rec = new TF1("DNAP2Rec",funcDNAP2Rec,10,1e6,6);
    DNAP2Rec->FixParameter(0,1./lastFit->GetParameter(0));
    DNAP2Rec->SetParameter(1,lastFit->GetParameter(5));
    DNAP2Rec->SetParameter(2,lastFit->GetParameter(4));
    DNAP2Rec->SetParameter(3,lastFit->GetParameter(3));
    DNAP2Rec->FixParameter(4,lastFit->GetParameter(2));
    DNAP2Rec->FixParameter(5,15);

    c->Clear();
    h->Fit(DNAP2Rec,"R");
    h->Draw();
    DNAP2Rec->Draw("same");
    //    drawFit->Draw("same");
    DNAP2Rec->SetLineWidth(1);
    //lastFit->SetLineColor(kTeal+3);
    //drawFit->SetLineColor(kCyan-4);
    cout << DNAP2Rec->GetChisquare()/DNAP2Rec->GetNDF() << endl;
    c->SaveAs("finalFit.pdf");
    Double_t x[53], y[53];
    Int_t n = 53;
    for(int i = 0; i< 53; ++i)
    {
        x[i] = h->GetBinCenter(i);
        y[i] = h->GetBinContent(i)-DNAP2Rec->Eval(x[i]);
        //        cout << i <<"\t"<< h->GetBinContent(i) <<"\t"<< y[i] << endl;
    }
    TGraph *gr = new TGraph(n,x,y);
    c->Clear();
    c->SetLogy(0);
    gr->GetHistogram()->SetMaximum(1e-5);
    gr->GetHistogram()->SetMinimum(-1e-5);
    gr->Draw("AC*");
}






void DTime(const char* aFileName){
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    int nEntries = ntp->GetEntries();
    //float ntpCont[20];
    float* ntpCont = ntp->GetArgs();
    char tHName[20];
    TH1D* tH = (TH1D*) gROOT->FindObjectAny("HDTime");
    if(tH) tH->Delete();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<1e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;
    HDTime = new TH1D("HDTime","HDTime",nBin,bin);





    int iEntry=0;
    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               fabs(ntpCont[8]+0.02)>0.01 && //single PE bump
               fabs(ntpCont[9]-1e-9)<1e-9); // trigger pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[9]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                }while(iEntry<nEntries &&
                       ntpCont[8]>-0.01); // At least 1 PE
                if(iEntry<nEntries){
                    HDTime->Fill((ntpCont[9]+ntpCont[1]-tTriggerPulseTime)*1e9);
                }
            }
        }
    }


    double scale = HDTime->GetEntries();
    HDTime->Sumw2();
    for(int iBin=1; iBin<=HDTime->GetNbinsX(); iBin++){
        HDTime->SetBinContent(iBin,HDTime->GetBinContent(iBin)/HDTime->GetBinWidth(iBin)/scale);
        HDTime->SetBinError(iBin,HDTime->GetBinError(iBin)/HDTime->GetBinWidth(iBin)/scale);
    }

    HDTime->Draw();
}
void FitDN(TH1F h)
{
    TF1 dn = new TF1("dn",funcDN, 1e7, 1e10,2);
    dn->SetParameter(0,1e9);
    dn->SetParameter(1,0.2);
    h->Fit(dn,"R");
    h->Draw("");
    dn->Draw("same");
    cout << "test";
}

void PFTime(const char* aFileName)
{
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<1000e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;
    TH1D * alt_TimeDist = new TH1D("alt_TimeDist","alt_TimeDist",nBin,bin);  //Creates timing distribution histogram
    int nEntries = ntp->GetEntries();
    double speupper =  0.045,//-0.03,//
            spelower = 0.03; //-0.05;//
    double delt_min = 1e-9;
    int bad = 0, good = 0;
    // read in the ntuple
    int counter = 0;
    double tPrev=-1;
    int testcount = 0;
    double Good = 0;

    int iEntry=0;
    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               abs(ntpCont[5]+0.0375)<0.0075 && //single PE bump
               abs(ntpCont[6])>15e-9); // trigger pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[6]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                }while(iEntry<nEntries &&
                       ntpCont[5]<0.03); // At least 1 PE
                if(iEntry<nEntries){
                    alt_TimeDist->Fill((ntpCont[6]+ntpCont[1]-tTriggerPulseTime)*1e9);
                }
            }
        }
    }


    double BinContents = alt_TimeDist->GetEntries();
    alt_TimeDist->Sumw2();
    for(int iBin=1; iBin<=alt_TimeDist->GetNbinsX(); iBin++)
    {
        alt_TimeDist->SetBinContent(iBin,alt_TimeDist->GetBinContent(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
        alt_TimeDist->SetBinError(iBin,alt_TimeDist->GetBinError(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
    }

    alt_TimeDist->Draw("");


}
void CalcXT(int Nt, int Ns)
{
    f = new TF1("f","1-[0]/[1]",0,1);
    df = new TF1("df","sqrt((1-[0]/[1])*([0]/[1])/[1])",0,1);
    df->SetParameter(0,Ns);
    f->SetParameter(0,Ns);
    df->SetParameter(1,Nt);
    f->SetParameter(1,Nt);
    cout <<" XTalk "<< f->Eval(0) << " +/- " << df->Eval(0) <<endl;
}
void FitTime(const char* aFileName)
{
    TFile* tFile = (TFile*) gROOT->GetListOfFiles()->FindObject(aFileName);
    if(!tFile) tFile = new TFile(aFileName);

    //Reads NTuple from .fanat# file in ntp/
    TNtuple* ntp = (TNtuple*) tFile->Get("ntp");
    float* ntpCont = ntp->GetArgs();
    int nBinMax=1001;
    double bin[1000];
    int nBin=0;
    double upperBound=pow(10,nBin/20.);
    while(upperBound<1e9 && nBin<nBinMax){
        bin[nBin]= upperBound;
        nBin++;
        upperBound = pow(10,nBin/20.);
    }
    bin[nBin]=upperBound;

    TH1D * alt_TimeDist = new TH1D("alt_TimeDist","alt_TimeDist",nBin,bin);
    int nEntries = ntp->GetEntries();
    double speupper =  -0.025,
            spelower = -0.032;
    double trigger = 2e-9;
    // read in the ntuple
    int iEntry=0;
    while(iEntry<nEntries){
        // >>> Look for trigger
        do{
            ntp->GetEntry(iEntry);
            iEntry++;
        }while(iEntry<nEntries &&
               (ntpCont[8] > speupper) && (ntpCont[8] < spelower) && //single PE bump
               abs(ntpCont[9])>trigger); // trigger pulse
        if(iEntry<nEntries){
            double tTriggerTime = ntpCont[1];
            double tTriggerPulseTime = ntpCont[9]+ntpCont[1];
            // >>> Check the next pulse
            ntp->GetEntry(iEntry);
            if(ntpCont[1]>=tTriggerTime){ // Do not account for the events at the sequence boundary
                do{
                    ntp->GetEntry(iEntry);
                    iEntry++;
                }while(iEntry<nEntries &&
                       (ntpCont[8] > speupper)); // At least 1 PE
                if(iEntry<nEntries){
                    alt_TimeDist->Fill((ntpCont[9]+ntpCont[1]-tTriggerPulseTime)*1e9);
                }
            }
        }
    }


    double BinContents = alt_TimeDist->GetEntries();
    alt_TimeDist->Sumw2();
    for(int iBin=1; iBin<=alt_TimeDist->GetNbinsX(); iBin++)
    {
        if(iBin > 78 && iBin <84)
        {
            alt_TimeDist->SetBinContent(iBin,0);
            alt_TimeDist->SetBinError(iBin,0);
        }
        else{
            alt_TimeDist->SetBinContent(iBin,alt_TimeDist->GetBinContent(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
            alt_TimeDist->SetBinError(iBin,alt_TimeDist->GetBinError(iBin)/alt_TimeDist->GetBinWidth(iBin)/BinContents);
        }
    }
    if(true)
    {
    TF1 * dn = new TF1("dn",funcDN, 1e7, 1e10,2);
    dn->SetParameter(0,5e-8);
    dn->SetParameter(1,0.2);
    alt_TimeDist->Fit(dn,"R");
    alt_TimeDist->Draw("");





    }
}
