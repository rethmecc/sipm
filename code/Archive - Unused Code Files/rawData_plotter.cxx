/** ****************************************************************
 *
 *  @abstract   macro to plot rawData
 *  @author     Lennart Huth <huth@physi.uni-heidelberg.de>
 *              Lloyd James <lloyd.thomas.james@gmail.com>
 *  @date       Sep 2014
 *
 *
 *
 *
 ** ****************************************************************/



#include <iostream>
#include <string>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <istream>

//root libraries
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include "TF1.h"
#include "TNtuple.h"

using namespace std;
struct NtpCont{
        float evt; //event number
        float tt;
        float blmu; //baseline mean for waveform
        float blRMS; //baseline RMS
        float np; //number of pulses in an event
        float pa; //pulse amplitude
        float pt; //pulse time
        float tchi2; //SPTemplateChi2
        float fa; //fit amplitude
        float ft; //fit time
        float frt; //fit rise time
        float fft; //fit fall time
        float fblmu; //fit baseline mean
        float fchi2; //chi2
        float ndf; //number degrees of freedom
        float frchi2; //chi2 for refit
        float frndf; //number degrees of freedom for refit
    };

char* getFileName(int mRun, char * mRunInfo, int mFitType)
{
    char FileNames[2][100];
    char* activename = new char[1024];
    char* outFileName = new char[1024];

    ifstream iFile;
    iFile.open(mRunInfo);
    char buffer [100];
    int runnum;
    char* newbuff;
    for (int i = 0; i < 10; i++){
        iFile >> buffer;
    }
    while(!iFile.eof()){
        iFile >> FileNames[0];
        iFile >> FileNames[1];
        sscanf(FileNames[0], "%d", &runnum);
        if((runnum == mRun)){
            newbuff = FileNames[1];
            sprintf(activename, "%s", newbuff);
        }
        for (int i = 0; i < 8; i++){
            iFile >> buffer;
        }
    }

    iFile.close();
    int slashIndex=0;
    for(int index=0; index<strlen(activename); index++){
        if(strncmp(activename+index,"/",1)==0) slashIndex=index;
    }
    sprintf(outFileName,"ntp%s.fanat%i",activename+slashIndex,mFitType);
    cout << outFileName <<endl;
    return outFileName;
}


int main(int argc, char * argv[])

{
    int run = argc > 1 ?atoi(argv[1]) : -1;

    char * filename = getFileName(run,"RunInfo.txt",101);

    TFile f(filename, "READ");
    //TFile f("ntp/_huth-100C_hmt_58-69_2us_1ns_1e6.hdf5.fanat101_1","read");
    TNtuple * ntp = (TNtuple *) f.Get("ntp");
    NtpCont* cont = (NtpCont*) ntp->GetArgs();
    // plot the stuff

    char outfile[200];
    TCanvas *c1 = new TCanvas("c1","c1",1600,900);

    ntp->Draw("fa:ft","fa < 0.01  && fa >- 0.1 && 1.6>(frchi2/frndf)");
    sprintf(outfile,"run_%i_fa_ft.png",run);
    c1->SaveAs(outfile);

    ntp->Draw("fa:ft","fa < 0.01  && fa >- 0.1 && ft<3e-9 && ft>-0.1e-9 && 1.6>(frchi2/frndf)");
    sprintf(outfile,"run_%i_fa_ft_zoom.png",run);
    c1->SaveAs(outfile);




    TH1D * p1 = new TH1D("p1","p1",200,-0.2,0.05);
    TH1D * p2 = new TH1D("p2","p2",200,-0.2,0.05);
    TH1D * p3 = new TH1D("p3","p3",200,-0.2,0.05);
    sprintf(outfile,"run_%i_fa.pdf",run);

    for(int i = 0; i<ntp->GetEntries();++i)
    {
        ntp->GetEntry(i);
        p2->Fill(cont->fa);
        if(cont->np<4)
            p1->Fill(cont->fa);
        else p3->Fill(cont->fa);


    }

    int min = 10e8;
    int bin = -1;

    for(int i = p1->FindBin(-0.035); i < p1->FindBin(-0.01); ++i)
    {
        if(p1->GetBinContent(i) < min)
        {
            bin     = i;
            min = p1->GetBinContent(i);
        }
    }
    p1->Fit("gaus","","",-0.05,-0.02);
    p1->SetTitle("fitted Peak Amplitude");
    p1->GetXaxis()->SetTitle("Peak Amplitude [V]");
    p1->GetYaxis()->SetTitle("Entries");
    p2->SetLineColor(3);
    p3->SetLineColor(1);
    p2->Draw("");
    p3->Draw("same");
    p1->Draw("same");
    c1->SetLogy(1);
    c1->SaveAs(outfile);
    c1->SetLogy(0);

    ntp->Draw("fa:evt","fa < 0.01  && fa >- 0.1 && 1.6>(frchi2/frndf)");
    sprintf(outfile,"run_%i_fa_evt.png",run);
    c1->SaveAs(outfile);
}
