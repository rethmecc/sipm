#ifndef WaveformProcessor_h
#define WaveformProcessor_h

class TH1F;
class TH1D;
class TGraph;
class DataFile;
class TF1;
class Waveform;
class TNtuple;

struct Pulse{
    double mBaseline;
    double mAbsAmp;
    double mAmp;
    double mTime;
    double mQ;
    double mWidth;
    double mSPTemplateChi2;
    double mFitLowLimit;
    double mFitHighLimit;
    double mFitBaseline;
    double mFitTime;
    double mFitAmp;
    double mFitRiseTime;
    double mFitFallTime;
    double mFitPulseRatio;   // Ratio between 2 TCs
    double mFitChi2;
    double mFitNDF;
    double mRefitChi2;
    double mRefitNDF;
    int mFirstPulseInGroup;
    double m50NanoAvg;
    double mPeakRise;
    double mPeakRiseExtrap;
    double mPeakRiseFit;
    double mAvgVar;
};

struct EventData{
    double pulseBounds[100][3];
    double peaks[100];
    int peakBins[100];
    double peakiness[100];
    double lb;
    double ub;
    double nPulse;
    double nPeak;
    double fitPars[100][4];
  double lMax[1000][2];
  int nlMax;
};

struct BinData{
  bool islMax;
  bool isProbPeak;
  bool isPeak;
  bool edge;
  double rise;
  double slopecheck;
  double peakiness;
};

class WaveformProcessor{
public:
//   WaveformProcessor(const char* aSetupFileName, int aRun,  int numPulsesFit, int aChannel, bool isForMacro);
  WaveformProcessor(const char* aSetupFileName, int aRun,  int numPulsesFit, int aChannel);
    ~WaveformProcessor();
    static WaveformProcessor* instanceForRootMacro(const char* aSetupFileName, int aRun,  int numPulsesFit=6, int aChannel=1);

    // ---
    char* getFileName(){return mFileName;}
    int getWaveformCount();
    double getTriggerTime();
    int getPolarity() {return mPolarity;}

    // --- Access  to waveform
    void readNextWaveform();
    void readWaveform(int aIndex);
    void readWaveform(const char* aHName);
    int getCurrentWaveformIndex();
    Waveform* getCurrentWaveform(){return mWF;}

    // --- baseline
    int processBaseline();
    void calcDeriv(int binSep);
    TH1F* getBaselineHisto(){return mHBaseline;}
    TH1D* getDeriv(int binSep);
    TH1D* getSecDeriv(int binSepFirstDeriv, int binSep);
    TH1D* getDerivHisto(int binSep){return mHDeriv[binSep];}
    double getBaselineMu(){return mBmu;}
    double getBaselineRMS(){return mBRMS;}

    // --- pulse finding
    void smoothWaveform(double lenNs=30);
    int findPulse();
    int findPulseGeneric(double nSSCR, double nSSCF, double dAFR, double dAFF, double lBLF, double mPW, double pT);
    int findSmallPulse(int startingBin, int endingBin);
    Waveform* getUnsmoothedWF(){return mDataWF;}

    EventData mEventData;
    BinData* mBins;
    bool mForMacro;
    //double getnPulse(){return mEventData.nPulse;}
    //double* getpulseBounds(){return mEventData.pulseBounds;}

    bool findPulseOld(int start, int end);
    void findPulse2(TNtuple* ntp, int start, int end, double level, int verbose);
    int getNPulse(){return mNPulse;}
    double getPulseTime(int aPulse){return mPulse[aPulse].mTime;}
    double getPulseAbsAmplitude(int aPulse){return mPulse[aPulse].mAbsAmp;}
    double getPulseAmplitude(int aPulse){return mPulse[aPulse].mAmp;}
    double getPulseBaseline(int aPulse){return mPulse[aPulse].mBaseline;}
    double getPulseCharge(int aPulse){return mPulse[aPulse].mQ;}
    double getPulseWidth(int aPulse){return mPulse[aPulse].mWidth;}
    double get50NanoAvg(int aPulse){return mPulse[aPulse].m50NanoAvg;}
    double getPeakRise(int aPulse){return mPulse[aPulse].mPeakRise;}
    double getPeakRiseExtrap(int aPulse){return mPulse[aPulse].mPeakRiseExtrap;}
    double getPeakRiseFit(int aPulse){return mPulse[aPulse].mPeakRiseFit;}
    double getAvgVar(int aPulse){return mPulse[aPulse].mAvgVar;}
    TGraph* getPulseGraph();
    bool checkZero(int start=0, int end=-1, double level=8, bool simple=false, int verbose=0);
    void findClearPulses(TNtuple* ntp, int start, int end, double level, int verbose);
    double findMin(int start, int end, int verbose=0);

    // --- fit Pulse
    void calcSinglePulseTemplateChi2();
    void setFitOption(const char* aOption);
    void fit(int aType);
    double checkNoise();

    //
    double getSPTemplateChi2(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mSPTemplateChi2;}
    double getFitBaseline(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitBaseline;}
    double getFitRiseTime(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitRiseTime;}
    double getFitFallTime(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitFallTime;}
    double getFitAmplitude(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitAmp;}
    double getFitTime(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitTime;}
    double getChi2(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitChi2;}
    double getPulseRatio(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitPulseRatio;}
    double getNDF(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mFitNDF;}
    double getChi2Refit(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mRefitChi2;}
    double getNDFRefit(int aPulse=0){return mPulse[mPulseIndex[aPulse]].mRefitNDF;}
    double get_blrms(){return mDiffAmpThresh;}
    TF1* getFitFunction();
    TF1* getFitFunction2();

    TH1F* getPulseHisto();
    void setPulseHisto(int nPulseBins, double preFactor);
    void setQuietMode(bool mode= true);

private:
    static WaveformProcessor* mInstanceForRootMacro;
    static int mRun;
		static int mChannel;
    static int mEv;
    void init(const char* aSetupFileName, int aRun, int numPulsesFit, int aChannel);
    void clear();
    void setForMacro(bool flag){mForMacro=flag;}
    void reFitMulti(); // controlled by type
    // >>> parameters
    char mSetupFileName[200];
    char mFileName[200];
    double mWFAmpBining;
    double mAmpMin;
    double mAmpMax;
    double mNoise1Bin;
    double mDiffAmpThresh;
    double mRiseTimeSig;
    double mFallTimeTau;
    double mPulseRatio;
    int mPolarity;
    double mMinChi2ForRefit;
    double mMaxSPTemplateChi2;

    // >>> Raw data
    DataFile* mDataFile;
    Waveform* mWF;

    // >>> Tools
    TH1F* mHBaseline;
    TH1D* mHDeriv[50];
    TH1D* mDeriv[50];
    TH1D* mSecDeriv[50];
    TF1* mDerivFit[50];
    TF1* mHBaselineFit;
    double mBmu;
    double mBRMS;
    double mDerivRMS[50];
    double mDerivmu[50];
    Waveform* mSmoothedWF;
    Waveform* mDataWF;

    // >>> Pulse
    int mNPulse;
    int mNPulseMax;
    Pulse* mPulse;
    int* mPulseIndex;
    double* mPulseTime;//for sorting only
    bool mPulseFound;

    // >>> Fit
    TF1* mFExpGaus;
    char mFitOption[10];
    int mNFitPulseMax;

    TH1F* pulseHisto; // follow convention
    int fitstatus; // follow convention
    bool quietMode;// follow convention

};

#endif
