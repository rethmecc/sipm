#include "TTree.h"
#include "TFile.h"
#include "TROOT.h"
#include "TNtuple.h"
#include "TProfile.h"
#include "TH2.h"

#include "V1729Data.h"
#include "Waveform.h"

#include "SetupParameter.h"

#include <iostream>

int V1729Data::mTimeBinShift = 50;
//int V1729Data::mLowVernier = 1792;
int V1729Data::mLowVernier = 1414;
//int V1729Data::mHighVernier = 3442;
int V1729Data::mHighVernier = 2234;
double V1729Data::mVernierWidth = 20.;

int V1729Data::mV1729WFSize=2561;
int V1729Data::mWFTimeOffsetDefault=50;
int V1729Data::mWFIdealHistMax=2048;

V1729Data::V1729Data(int aRun, int aNEventMax, char* aDir):mRun(aRun),mDefaultIndex(0),mWFDiff(0),mVernierProfile(0){

  // --- Open file
  char tFileName[200];
  sprintf(tFileName,"%s/run%05d.root",aDir,aRun);
  mFile = (TFile*)gROOT->GetListOfFiles()->FindObject(tFileName);
  if (!mFile) {
    mFile = new TFile(tFileName);
  }
  mFile->cd();
  mTree = (TTree*) mFile->Get("Trigger");
  mTree->SetBranchAddress("FWAV",&mWFData);
  mNEvent = mTree->GetEntries();
  if(mNEvent > aNEventMax) mNEvent = aNEventMax;
  mTree->GetEntry(0);
  mNWF = mWFData.NBin/mV1729WFSize;  
  std::cout << "V1729Data opening file " << tFileName << std::endl
	    << " N events = " << mNEvent << std::endl
	    << " N bins = " << mWFData.NBin << std::endl
	    << " N waveforms " << mNWF << std::endl;


  // --- Build Waveform
  mWF = new Waveform*[mNWF];
  for(int iWF=0; iWF<mNWF; iWF++){
    char tWFName[50];
    sprintf(tWFName,"WF%i",iWF);
    mWF[iWF] = (Waveform*) gROOT->FindObjectAny(tWFName);
    if(mWF[iWF]) delete mWF[iWF];
    mWF[iWF] = new Waveform(mWFIdealHistMax, 0., mWFIdealHistMax*1., 
			    tWFName);  
    //mWF[iWF]->setVernierRange(mLowVernier,mHighVernier,mVernierWidth);
    //mWF[iWF]->setVernierRange(1414,-0.0121*2.);//2613
    //mWF[iWF]->setVernierRange(1795,-0.0121);
    mWF[iWF]->setVernierRange(1400,-0.025);
    mWF[iWF]->resetAverage();

    // --- set the offset for  up to 4 channel
    for(int ti=0; ti<4; ti++) {
      mWFTimeOffset[ti] = mWFTimeOffsetDefault;
    }
  }
}

V1729Data::~V1729Data(){
  for(int iWF=0; iWF<mNWF; iWF++){
    delete mWF[iWF]; 
  }
  delete[] mWF;
  if(mWFDiff) delete mWFDiff;
  //mFile->Delete();
  if(mVernierProfile) mVernierProfile->Delete();
}

void V1729Data::getEntry(int aIndex){
  mTree->GetEntry(aIndex);
  for(int iWF=0; iWF<mNWF; iWF++){    
    mWF[iWF]->update(mWFData.WF+mWFTimeOffset[iWF]+mV1729WFSize*iWF,
    		     mWFData.WF[1]);
  }
  if(mWFDiff) {
    for(int ti=1; ti<=mWFDiff->GetNbinsX(); ti++){
      mWFDiff->SetBinContent(ti,mWF[miWFDiffP]->GetBinContent(ti));
      //    mWFDiff->update(mWFData.WF+mWFTimeOffset[miWFDiffP]+mV1729WFSize*miWFDiffP,
      //	    mWFData.WF[1]);
    }
    mWFDiff->Add(mWF[miWFDiffN],-1.);
  }
}


Waveform* V1729Data::getWaveform(){
  //if(mDefaultIndexDiff!=-1){
  //mWF[mDefaultIndex]->Add(mWF[mDefaultIndexDiff],-1.);
  //}
  return mWFDiff? mWFDiff : mWF[mDefaultIndex];
}

void  V1729Data::setDiffWF(int aIndexP, int aIndexN){
  miWFDiffP = aIndexP; 
  miWFDiffN = aIndexN;
  mWFDiff = new Waveform(mWFIdealHistMax, 0., mWFIdealHistMax*1., "WFDiff"); 
  mWFDiff->resetAverage();
}

void  V1729Data::setSecondOrderVernier(){
  double tPTimeMean = (SetupParameter::instance()->PTimeMax+SetupParameter::instance()->PTimeMin)/2.;
  mVernierProfile = new TProfile("VerProf","VerProf",900,1400,2300,
				 SetupParameter::instance()->PTimeMin-tPTimeMean,
				 SetupParameter::instance()->PTimeMax-tPTimeMean);
  char tFAvName[50];
  sprintf(tFAvName,"AverageWF/Av%i.root",getRun());
  TFile* tFAv  = (TFile*) gROOT->GetListOfFiles()->FindObject(tFAvName);
  if(!tFAv) tFAv = new TFile(tFAvName);
  if( tFAv){
    TNtuple* tNtp = (TNtuple*) tFAv->Get("ntp");
    char tCom[50];
    sprintf(tCom,"Ptime-%f:Ver",tPTimeMean);
    //tNtp->Project("VerProf",tCom,"int>500 && int<10000","prof"); // does not work for unknown reason
    float* tArg = tNtp->GetArgs();
    for(int ti=0; ti<tNtp->GetEntries(); ti++){
      tNtp->GetEntry(ti);
      if(tArg[2]>500 && tArg[2]<10000){
	mVernierProfile->Fill(tArg[5],tArg[9]-tPTimeMean);
      }
    }
    for(int iWF=0; iWF<mNWF; iWF++){    
      mWF[iWF]->setSecondOrderVernier(mVernierProfile);
    }
  }
}
