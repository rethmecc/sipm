/*******************************************************************************
* Distribution.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "DataFile.h"
//#include "WfmFile.h"
#include "LecroyFile.h"
#include "LecroyHdfFile.h"
#include "WaveformProcessor.h"
#include "Waveform.h"


#include "TFile.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"
#include "TNtuple.h"

//___________________________________________________________________
// --- Fit function
// Standard gaussian exponential convolution
double FuncExpGausMulti(double* x, double*p){
    // p[0]: baseline
    // p[1]: gaussian sig
    // p[2]: exponential decay constant
    // p[3]: number of pulses
    // p[4+2*i]: pulse[i] amplitude
    // p[5+2*i]: pulse[i] time
    // convolution of an exponential and a gaussian, formula found in a
    // chromatography paper on exponentially modified gaussians.
    // http://www.springerlink.com/content/yx7554182g164612/
    double val=p[0];
    for(int iPulse=0; iPulse<p[3]; iPulse++){
        double time = x[0]-p[5+2*iPulse];
        if(((p[1]*p[1]/p[2]/2.-time)/p[2])<700){ //otherwise exponential explodes
	                                             //Not sure of the need for this - LJ
            val+=p[4+2*iPulse]/2.*//p[2]/2.*
                    exp((p[1]*p[1]/p[2]/2.-time)/p[2])*
                    TMath::Erfc((p[1]*p[1]/p[2]-time)/sqrt(2)/p[1]);
        }
        //exp(1/2*p[1]*p[1]/p[2]/p[2]-time/p[2])*
        //(TMath::Erf(1/sqrt(2)*(p[5+2*iPulse]/p[1]+p[1]/p[2]))+
        // TMath::Erf(1/sqrt(2)*(time/p[1]-p[1]/p[2])));
    }
    return val;
}

//___________________________________________________________________
// --- Constuctor
// Constuctor for multi channel
// WaveformProcessor::WaveformProcessor(const char* aSetupFileName, int aRun,  int numPulsesFit, int aChannel, bool isForMacro){
//   init(aSetupFileName,aRun,numPulsesFit,aChannel,isForMacro);
// }

WaveformProcessor::WaveformProcessor(const char* aSetupFileName, int aRun,  int numPulsesFit, int aChannel){
  init(aSetupFileName,aRun,numPulsesFit,aChannel);
}


// ---
void WaveformProcessor::init(const char* aSetupFileName, int aRun,  int numPulsesFit, int aChannel){
		mChannel=aChannel;
    strcpy(mSetupFileName,aSetupFileName);
    // --- Open run info file
    std::ifstream parFile(aSetupFileName);
    char buf[200];
    for(int iBuf=0; iBuf<11; iBuf++) parFile >> buf;
    //mWFAmpBining >> mAmpMin >> mAmpMax not currently used in this code. See WaveformProcessor::processBaseline()
    parFile >> mRun >> mFileName >>  mWFAmpBining >> mAmpMin >> mAmpMax
            >> mNoise1Bin >> mRiseTimeSig >> mFallTimeTau >> mPolarity >> mMinChi2ForRefit;
    while(!parFile.eof() && mRun!=aRun){
        parFile >> mRun >> mFileName >>  mWFAmpBining >> mAmpMin >> mAmpMax
                >> mNoise1Bin >> mRiseTimeSig >> mFallTimeTau>> mPolarity >> mMinChi2ForRefit;
    }
    
    mMaxSPTemplateChi2 = 1e9;
    if(parFile.eof()){
        std::cerr << "Could not find run requested " << aRun << std::endl;
        exit(0);
    }
    // --- Open input file
    //>>> check file extension
    int iChar=strlen(mFileName)-1;
    while(iChar>0 && strncmp(mFileName+iChar,".",1)!=0) iChar--;
    if(strncmp(mFileName+iChar+1,"root",4)==0){
        mDataFile = new LecroyFile(mFileName);
    }
    else{
        if(strncmp(mFileName+iChar+1,"hdf5",4)==0){
            mDataFile = new LecroyHdfFile(mFileName,aChannel);
        }
        else{
            std::cerr << "cannot tell file type. Aborting " << std::endl;
            exit(1);
        }
    }
    //if(quietMode == false) std::cout << "Waveform found : " << mDataFile->getWaveformCount() << std::endl;

    // --- prepare vector of pulses
    mNFitPulseMax = numPulsesFit;
    mNPulse=0;
    mNPulseMax= mNFitPulseMax*2;
    mPulse = new Pulse[mNPulseMax];
    mPulseIndex = new int[mNPulseMax];
    mPulseTime = new double[mNPulseMax];

    // --- Function for pulse fitting
    mFExpGaus = (TF1*) gROOT->FindObjectAny("FExpGaus");
    if(mFExpGaus) mFExpGaus->Delete();

    // I do not think, that this line makes sense, why should one do this after 126
    mNFitPulseMax=mNPulseMax;
    mFExpGaus = new TF1("FExpGaus",FuncExpGausMulti,
                        0.,0.5e-6,4+mNFitPulseMax*2); // range reset later
    mFExpGaus->SetParLimits(1,0.,1e-6);// sigma
    mFExpGaus->SetParLimits(2,0.,1e-6);// tau
    strcpy(mFitOption,"QR0");

    mSmoothedWF=NULL;
    mDataWF=NULL;
    mBins=NULL;

    mForMacro = false;
    
}

// ---
WaveformProcessor* WaveformProcessor::mInstanceForRootMacro=0;
int WaveformProcessor::mRun=-1;
int WaveformProcessor::mChannel=-1;
int WaveformProcessor::mEv=-1;
WaveformProcessor* WaveformProcessor::instanceForRootMacro(const char* aSetupFileName, int aRun, int numPulsesFit, int aChannel){
    if(!mInstanceForRootMacro){ //!!!! Must remain a singleton. Force init if needed somewhere else
      mInstanceForRootMacro = new WaveformProcessor(aSetupFileName, aRun, numPulsesFit, aChannel);
      mInstanceForRootMacro->setForMacro(true);
    }	
    else{
        if(mRun!=aRun || aChannel!=mChannel){
            mInstanceForRootMacro->clear();
            mInstanceForRootMacro->init(aSetupFileName, aRun, numPulsesFit, aChannel);
            mInstanceForRootMacro->setForMacro(true);
        }
    }
    
    return mInstanceForRootMacro;
}

// ---
int WaveformProcessor::getWaveformCount(){
    return mDataFile->getWaveformCount();
}

//___________________________________________________________________
// ---
WaveformProcessor::~WaveformProcessor(){
    clear();
}
void WaveformProcessor::clear(){
    delete mDataFile;
    if(mHBaseline) mHBaseline->Delete();
    if(mFExpGaus) mFExpGaus->Delete();
    if(mSmoothedWF) mSmoothedWF->Delete();
    delete[] mPulse;
    delete[] mPulseIndex;
    delete[] mPulseTime;
}

//___________________________________________________________________
// --- Access the waveform
void WaveformProcessor::readNextWaveform(){
    mWF = mDataFile->getNextWaveform(); // index is not used!
    mWF->setQuietMode(quietMode);
}
void WaveformProcessor::readWaveform(int aIndex){
    mWF = mDataFile->getWaveform(aIndex);
    mWF->setQuietMode(quietMode);
}
void WaveformProcessor::readWaveform(const char* aHName){
    mWF = mDataFile->getWaveform(aHName);
    mWF->setQuietMode(quietMode);
}
int WaveformProcessor::getCurrentWaveformIndex(){
    char ciwf[50];
    strcpy(ciwf,mWF->GetName()+7);
    return atoi(ciwf);
}

//___________________________________________________________________
// --- baseline
// Calculates baseline parameters
int WaveformProcessor::processBaseline(){
    // --- Create histogram for baseline calculation
    mHBaseline = (TH1F*) gROOT->FindObjectAny("HBaseline");
    mHBaselineFit = (TF1*) gROOT->FindObjectAny("HBaselineFit");
    if(mHBaseline) mHBaseline->Delete();
    if(mHBaselineFit) mHBaselineFit->Delete();
    mAmpMin = mWF->GetMinimum();
    mAmpMax = mWF->GetMaximum();
    int nBinsBaseline = 500;
    //int nBinsBaseline = (mAmpMax-mAmpMin)/mWFAmpBining;
    mHBaseline = new TH1F("HBaseline","HBaseline",nBinsBaseline,mAmpMin,mAmpMax);
    mHBaselineFit = new TF1("HBaselineFit","gaus");
    mHBaseline->Reset("C");
    for(int iBin=1; iBin <= mWF->GetNbinsX(); iBin++){
        mHBaseline->Fill(mWF->GetBinContent(iBin));
    }
    int blMostProbBin=0;
    int blMostProbCont=0;
    for(int iBin=1; iBin <= mHBaseline->GetNbinsX(); iBin++){
        if(mHBaseline->GetBinContent(iBin) > blMostProbCont){
            blMostProbCont = mHBaseline->GetBinContent(iBin);
            blMostProbBin = iBin;
        }
    }
    mBmu=mHBaseline->GetBinCenter(blMostProbBin);
    mHBaseline->Fit(mHBaselineFit,"Q");
    mBRMS=mHBaselineFit->GetParameter(2);
//     mBRMS=mHBaseline->GetRMS();
    //std::cout << "The baseline mu is: " << mBmu << std::endl;
    //std::cout << "The number of baseline bins is: " << nBinsBaseline << std::endl;

    return mBRMS<5*mNoise1Bin; // Otherwise noisy. i.e. pick up
}

void WaveformProcessor::calcDeriv(int binSep){  //Analogous to processBaseline, but for derivative
  if (mHDeriv[binSep]) mHDeriv[binSep]->Delete();
  if (mDerivFit[binSep]) mDerivFit[binSep]->Delete();
  int nbins=mWF->GetNbinsX();
  char ID[20];
  sprintf(ID,"derivHist%i",binSep);
  mHDeriv[binSep] = new TH1D(ID,ID,120,mWF->GetMinimum(),mWF->GetMaximum());
  for (int i=1;i<=nbins-binSep;i++)
    mHDeriv[binSep]->Fill(mWF->GetBinContent(i+binSep)-mWF->GetBinContent(i));
  sprintf(ID,"derivFit%i",binSep);
  mDerivFit[binSep] = new TF1(ID,"gaus");
  mHDeriv[binSep]->Fit(mDerivFit[binSep],"Q");
  mDerivmu[binSep]=mDerivFit[binSep]->GetParameter(1);
  mDerivRMS[binSep]=mDerivFit[binSep]->GetParameter(2);
}

TH1D* WaveformProcessor::getDeriv(int binSep){  //Generates derivative waveform  
  if (mDeriv[binSep]) return mDeriv[binSep];
  int nbins=mWF->GetNbinsX();
  char ID[20];
  sprintf(ID,"deriv%i",binSep);
  if (binSep%2==1)
    mDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mWF->GetBinCenter(binSep/2.+0.5),mWF->GetBinCenter(nbins-binSep/2.+0.5));
  else
    mDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mWF->GetBinLowEdge(binSep/2.+1),mWF->GetBinLowEdge(nbins-binSep/2.+1));
  for (int i=1;i<=nbins-binSep;i++)
    mDeriv[binSep]->SetBinContent(i,mWF->GetBinContent(i+binSep)-mWF->GetBinContent(i));
  
  return mDeriv[binSep];
}

TH1D* WaveformProcessor::getSecDeriv(int binSepFirstDeriv,int binSep){  //Generates derivative waveform
  getDeriv(binSepFirstDeriv);
  if (mSecDeriv[binSep]) return mSecDeriv[binSep];
  int nbins=mDeriv[binSepFirstDeriv]->GetNbinsX();
  char ID[20];
  sprintf(ID,"deriv%i",binSep);
  if (binSep%2==1)
    mSecDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mDeriv[binSepFirstDeriv]->GetBinCenter(binSep/2.+0.5),mDeriv[binSepFirstDeriv]->GetBinCenter(nbins-binSep/2.+0.5));
  else
    mSecDeriv[binSep] = new TH1D(ID,ID,nbins-binSep,mDeriv[binSepFirstDeriv]->GetBinLowEdge(binSep/2.+1),mDeriv[binSepFirstDeriv]->GetBinLowEdge(nbins-binSep/2.+1));
  for (int i=1;i<=nbins-binSep;i++)
    mSecDeriv[binSep]->SetBinContent(i,mDeriv[binSepFirstDeriv]->GetBinContent(i+binSep)-mDeriv[binSepFirstDeriv]->GetBinContent(i));
  
  return mSecDeriv[binSep];
}

//___________________________________________________________________
// --- pulse finding
// Looks for peaks based on deviation from the baseline
int WaveformProcessor::findPulse(){
    mPulseFound = false;
    int nBinRiseTime = round(mRiseTimeSig*3/mWF->GetBinWidth(1));
    int nBinFallTime = round(mFallTimeTau*3/mWF->GetBinWidth(1));

    mNPulse=0;
    for(int iBin=nBinRiseTime+1; iBin<=mWF->GetNbinsX(); iBin++){ //Loop through WF

        // first  condition: pulse is more than 4 sigma away from baseline
        // second condition: pulse is negative and has a certain height
        // third  condition: a width of at least 3 bins over threshold
        if((mPolarity*(mWF->GetBinContent(iBin)-mBmu)>3*mNoise1Bin) &&
                (mPolarity*(mWF->GetBinContent(iBin)-
                            mWF->GetBinContent(iBin-nBinRiseTime))>5*mNoise1Bin) )
        {
            mPulseFound = true;

            // >>> Find maximum amplitude
            double pulseAbsAmp=mPolarity*mWF->GetBinContent(iBin);
            iBin++;
            while(iBin <= mWF->GetNbinsX() &&
                  pulseAbsAmp <= mPolarity*(mWF->GetBinContent(iBin)-mBmu)){ //Find peak height
                pulseAbsAmp=mPolarity*(mWF->GetBinContent(iBin)-mBmu);
                iBin++;
            }
            pulseAbsAmp+=(mBmu*mPolarity);  //Add baseline
            pulseAbsAmp*=mPolarity;

            // >>> Calculate the baseline before the pulse
            double pulseBaseline=0.;
            for(int iBinBase=iBin-nBinRiseTime-12;
                iBinBase<(iBin-nBinRiseTime-2);
                iBinBase++){
                pulseBaseline+=mWF->GetBinContent(iBinBase);
            }
            pulseBaseline/=10.;
            double pulseAmp=(pulseAbsAmp-pulseBaseline)*mPolarity;

            // >>> Calculate the charge (for removing noise)
            double tCharge=0.;
            int iPulseStart=-1;
            int iPulseEnd=-1;
            double pulseWidth=-1.;
            for(int iBinInt=iBin-nBinRiseTime;
                iBinInt<iBin+nBinFallTime; iBinInt++){
                tCharge+=mWF->GetBinContent(iBinInt);
                tCharge-=mBmu;
                if(mPolarity*(mWF->GetBinContent(iBinInt)-pulseBaseline)/pulseAmp>0.25){
                    if(iPulseStart<0) iPulseStart=iBinInt;
                    iPulseEnd=iBinInt;
                }
                else{
                    if(iPulseStart>=0 && pulseWidth<0.) pulseWidth=iPulseEnd-iPulseStart;
                }
            }
            tCharge*=mPolarity;
            if(pulseWidth<0.) pulseWidth=iPulseEnd-iPulseStart;

            if(iBin < mWF->GetNbinsX() && // Do not keep pulses that are at the edge of the waveform
                    tCharge>3*sqrt(nBinRiseTime+nBinFallTime)*mNoise1Bin //&&
                    //pulseWidth>(nBinRiseTime+nBinFallTime)/2
                    ){
                iBin--;
                if(mNPulse<mNPulseMax){
                    mPulse[mNPulse].mTime=mWF->GetBinCenter(iBin);
                    mPulse[mNPulse].mAbsAmp=pulseAbsAmp;
                    mPulse[mNPulse].mAmp=pulseAmp;
                    mPulse[mNPulse].mBaseline=pulseBaseline;
                    mPulse[mNPulse].mQ=tCharge;
                    mPulse[mNPulse].mWidth=pulseWidth;
                    mPulseIndex[mNPulse]=mNPulse;
                    mNPulse++;
                }
            }
            iBin+=nBinFallTime;
        }
    }
    return mNPulse<mNPulseMax;
}

//Generic Pulse Finder by Lloyd
int WaveformProcessor::findPulseGeneric(double nSSCR, double nSSCF, double dAFR, double dAFF, double lBLF, double mPW, double pT){
   //Parameters
  ////////////////////////////////
  double nSecSlopeCheckRise = nSSCR;
  double nSecSlopeCheckFall = nSSCF;
  double diffAmpFactorRise = dAFR;
  double diffAmpFactorFall = dAFF;
  double lowerBLFactor = lBLF;
  double minPulseWidth = mPW;
  double peakinessThresh = pT;
  bool fitting = false;          //
  ////////////////////////////////

  double avgVar = 0;
  int rIntersects [100]; //Indices of intersections between lower bound and data on rising slopes
  int nrIntersects = 0;
  int fIntersects [100]; //Indices of intersections between lower bound and data on falling slopes            
  int nfIntersects = 0;
  int lMax [1000]; //Indices of local maxima above lower bound
  int nlMax = 0;
  int missedMax [100];
  int nMissedMax = 0;
  int peaks [100]; //Indices of local maxima above noise upper bound with minimum 'peakiness'
  int nPeaks = 0;
  int probablePeaks [100]; //Indices of local maxima above noise upper bound
  int nProbablePeaks = 0;
  int edge [1000];// 0 -> rising edge, 1 -> falling edge
  int smallPulseRegions [100] [2];
  int nSmallPulseRegions = 0;

  if ((!mBins) && (mForMacro)) mBins = new BinData [mWF->GetNbinsX()];
  if (mForMacro){
    for (int iBin = 0; iBin < mWF->GetNbinsX(); iBin++){
      mBins[iBin].islMax = false;
      mBins[iBin].isProbPeak = false;
      mBins[iBin].isPeak = false;
      mBins[iBin].edge = 0;
      mBins[iBin].rise = 0.0;
      mBins[iBin].slopecheck = 0.0;
      mBins[iBin].peakiness = 0.0;
    }
  }

  //Caclulates the average magnitude of variation between bins in the waveform
  for(int iBin = 1; iBin <= mWF->GetNbinsX(); iBin++){
    avgVar += fabs(mWF->GetBinContent(iBin) - mWF->GetBinContent(iBin-1));
  }
  avgVar /= mWF->GetNbinsX();

  //Applies smoothing to the waveform
  smoothWaveform(1);

  double lb = mBmu + lowerBLFactor*avgVar;  //Lower baseline defined thusly
  avgVar = 0;
  for(int iBin = 1; iBin <= mWF->GetNbinsX(); iBin++){
    avgVar += fabs(mWF->GetBinContent(iBin) - mWF->GetBinContent(iBin-1));
  }
  avgVar /= mWF->GetNbinsX();
  
  //Checks for radio noise
  for (int iBin = mWF->FindBin(-50e-9); iBin < mWF->FindBin(50e-9); iBin++){
    if (((mPolarity == 1) && (mWF->GetBinContent(iBin) < -2*lb)) || ((mPolarity == -1) && (mWF->GetBinContent(iBin) > 2*lb))){
      
      mNPulse = 0;
      return 0;
    }
  }

  //Finds the intersections between the waveform and the lower baseline.                                                                                                                                     
  for(int iBin = 1; iBin <= mWF->GetNbinsX(); iBin++){
    if((mPolarity*mWF->GetBinContent(iBin+1) > lb) && (mPolarity*mWF->GetBinContent(iBin) < lb)){ //Finds pulse starts                                                                                       
      if ((nrIntersects == 100)){
        mNPulse = 0;
        return 0;
      }
      rIntersects[nrIntersects] = iBin;
      nrIntersects++;
    }
    else if((mPolarity*mWF->GetBinContent(iBin+1) < lb) && (mPolarity*mWF->GetBinContent(iBin) > lb)){ //Finds pulse ends                                                                                    
      if ((nfIntersects == 100)){
        mNPulse = 0;
        return 0;
      }
      fIntersects[nfIntersects] = iBin;
      nfIntersects++;
    }
  }

  
  //Finds all local maxima that lie above the lower baseline                                                                                                                                                                         
  for(int iBin = rIntersects[0]; iBin < fIntersects[nfIntersects-1]; iBin++){
    if((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin+1))) && (mPolarity*mWF->GetBinContent(iBin) > lb)){
      if ((nlMax == 1000)){
        mNPulse = 0;
        return 0;
      }
      lMax[nlMax] = iBin;
      if (mForMacro) mBins[iBin].islMax = true;
      nlMax++;
    }
    else if ((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) == mPolarity*(mWF->GetBinContent(iBin+1))) && (mPolarity*mWF->GetBinContent(iBin) > lb)){
      int jBin;
      for (jBin = iBin+1; mPolarity*(mWF->GetBinContent(jBin)) == mPolarity*(mWF->GetBinContent(iBin)); jBin++){
      }
      if ((mPolarity*(mWF->GetBinContent(jBin-1)) > mPolarity*(mWF->GetBinContent(jBin)))){
	for (int kBin = iBin; kBin < jBin; kBin++){
	  if ((nlMax == 1000)){
	    mNPulse = 0;
	    return 0;
	  }
	  lMax[nlMax] = kBin;
	  if (mForMacro) mBins[kBin].islMax = true;
	  nlMax++;
	}
	iBin = jBin-1;
      }
    }
  }

  int pulses[100][2];
  int nPulses = 0;
  int pulseMax [100];

  //Finds the regions made by pulses                                                                                               
  int iInt0;
  if((rIntersects[0] < fIntersects[0])) iInt0 = 0;
  else iInt0 = 1;

  for (int iInt = iInt0; iInt < nfIntersects; iInt++){
    //std::cout<< mWF->GetBinCenter(rIntersects[iInt-iInt0]) << " " <<mWF->GetBinCenter(fIntersects[iInt]) << " ";
    bool hasMax = false;
    bool hasLargeMax = false;
    for (int iMax = 0; iMax < nlMax; iMax++){
      if((lMax[iMax] > rIntersects[iInt-iInt0]) && (lMax[iMax] < fIntersects[iInt]) && (mPolarity*mWF->GetBinContent(lMax[iMax]) > 2*lb)){
        hasLargeMax = true;
        break;
      }
      else if ((lMax[iMax] > rIntersects[iInt-iInt0]) && (lMax[iMax] < fIntersects[iInt])){
	hasMax = true;
      }
    }
    if ((hasLargeMax) && ((mWF->GetBinCenter(fIntersects[iInt]) - mWF->GetBinCenter(rIntersects[iInt-iInt0])) > minPulseWidth)){
      pulses[nPulses][0] = rIntersects[iInt-iInt0];
      pulses[nPulses][1] = fIntersects[iInt];
      nPulses++;
    }
     else if (hasMax){/*
      int jInt;
      for (jInt = iInt+1; jInt < nfIntersects; jInt++){
	if ((mWF->GetBinCenter(fIntersects[jInt]) - mWF->GetBinCenter(fIntersects[jInt-1]) > minPulseWidth)){
	  jInt--;
	  break;
	}
      }
      int max = findSmallPulse(rIntersects[iInt-iInt0], fIntersects[jInt]);
      probablePeaks[nProbablePeaks] = max;
      if (mForMacro) mBins[max].isProbPeak = true;
      nProbablePeaks++;
      smallPulseRegions[nSmallPulseRegions][0] = rIntersects[iInt-iInt0];
      smallPulseRegions[nSmallPulseRegions][1] = fIntersects[jInt];
      nSmallPulseRegions++;
      iInt = jInt;*/
      }
  }

  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    pulseMax[iPulse] = pulses[iPulse][0];
    for (int iBin = pulses[iPulse][0]+1; iBin < pulses[iPulse][1]; iBin++){
      if ((mPolarity*mWF->GetBinContent(iBin) > mPolarity*mWF->GetBinContent(pulseMax[iPulse]))){
	pulseMax[iPulse] = iBin;
      }
    }
  }

  //Recheck rising edge for peaks
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    for (int iBin = mDataWF->FindBin(mWF->GetBinCenter(pulseMax[iPulse])-3e-9); iBin > mDataWF->FindBin(mWF->GetBinCenter(pulses[iPulse][0])); iBin--){
      if ((mDataWF->FindBin(mWF->GetBinCenter(pulseMax[iPulse])) - iBin > 6e-9/mDataWF->GetBinWidth(1)) && (mPolarity*mDataWF->GetBinContent(iBin) - mPolarity*mDataWF->GetBinContent(iBin-5e-9/mDataWF->GetBinWidth(1)) < 0)) break;
      bool preex = false;
      // std::cout << iBin << " " << mWF->FindBin(mDataWF->GetBinCenter(iBin)) << " ";
      for (int iMax = 0; iMax < nlMax; iMax++){
	if ((mWF->FindBin(mDataWF->GetBinCenter(iBin)) == lMax[iMax]) || (mWF->FindBin(mDataWF->GetBinCenter(iBin+1)) == lMax[iMax])) preex = true;
     }
      //std::cout << preex << std::endl;
	if((mPolarity*(mDataWF->GetBinContent(iBin)) > mPolarity*(mDataWF->GetBinContent(iBin-1))) && (mPolarity*(mDataWF->GetBinContent(iBin)) > mPolarity*(mDataWF->GetBinContent(iBin+1))) && (mPolarity*mDataWF->GetBinContent(iBin) > lb) && !preex){
	missedMax[nMissedMax] = iBin;
	if (mForMacro) mBins[mWF->FindBin(mDataWF->GetBinCenter(iBin))].islMax = true;
	nMissedMax++;
	}
	else if ((mDataWF->GetBinContent(iBin)==mDataWF->GetBinContent(iBin+1)) && (mPolarity*(mDataWF->GetBinContent(iBin)) > mPolarity*(mDataWF->GetBinContent(iBin-1))) && (mPolarity*(mDataWF->GetBinContent(iBin)) > mPolarity*(mDataWF->GetBinContent(iBin+2)))&& (mPolarity*mDataWF->GetBinContent(iBin) > lb) && !preex){ 
	  missedMax[nMissedMax] = iBin+1;
	  if (mForMacro) mBins[mWF->FindBin(mDataWF->GetBinCenter(iBin+1))].islMax = true;
	  nMissedMax++;
	}

    }
  }


  //Identifies points that are likely peaks
  for (int iMax = 0; iMax < nlMax; iMax++){
    for (int iPulse = 0; iPulse < nPulses; iPulse++){
      if ((lMax[iMax] < pulses[iPulse][1]) && (lMax[iMax] > pulses[iPulse][0]) && (lMax[iMax] < pulseMax[iPulse])){
	edge[iMax] = 0;
	if (mForMacro) mBins[lMax[iMax]].edge = 0;
	break;
      }

      else if ((lMax[iMax] < pulses[iPulse][1]) && (lMax[iMax] > pulses[iPulse][0]) && (lMax[iMax] >= pulseMax[iPulse])){
	edge[iMax] = 1;
	if (mForMacro) mBins[lMax[iMax]].edge = 1;
	break;
      }
      else edge[iMax] = 2;
    }
  }

  int nBinsSlopeCheckRise = nSecSlopeCheckRise / mWF->GetBinWidth(2);
  int nBinsSlopeCheckFall = nSecSlopeCheckFall / mWF->GetBinWidth(2);
  if ((nBinsSlopeCheckRise == 0)) nBinsSlopeCheckRise++;
  if ((nBinsSlopeCheckFall == 0)) nBinsSlopeCheckFall++;
  for (int iMax = 0; iMax < nlMax; iMax++){
    if ((edge[iMax] == 0)){
      double ampDiffRise = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]+nBinsSlopeCheckRise);
      double ampDiffFall = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]-nBinsSlopeCheckFall);
      ampDiffRise /= nBinsSlopeCheckRise;
      ampDiffFall /= nBinsSlopeCheckFall;
      if (mForMacro) mBins[lMax[iMax]].slopecheck = ampDiffRise / avgVar;
      
      if ((ampDiffRise / avgVar > diffAmpFactorRise) && (ampDiffFall / avgVar > diffAmpFactorFall) && (lMax[iMax] - probablePeaks[nProbablePeaks-1] != 1)){
	probablePeaks[nProbablePeaks] = lMax[iMax];
	//std::cout << "pP0: " << lMax[iMax] << std::endl;
	edge[nProbablePeaks] = edge[iMax];
	if (mForMacro) mBins[lMax[iMax]].isProbPeak = true;
	nProbablePeaks++;
      }
    }
    else if ((edge[iMax] == 1)){
      double ampDiffFall = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]-nBinsSlopeCheckFall);
      double ampDiffRise;
      if ((mWF->GetBinContent(lMax[iMax])==mWF->GetBinContent(lMax[iMax]+1))){
	ampDiffRise = mPolarity*mWF->GetBinContent(lMax[iMax]+1) - mPolarity*mWF->GetBinContent(lMax[iMax]+nBinsSlopeCheckRise+1);
      }
      else ampDiffRise = mPolarity*mWF->GetBinContent(lMax[iMax]) - mPolarity*mWF->GetBinContent(lMax[iMax]+nBinsSlopeCheckRise);
      ampDiffFall /= nBinsSlopeCheckFall;
      ampDiffRise /= nBinsSlopeCheckRise;
      if (mForMacro) mBins[lMax[iMax]].slopecheck = ampDiffFall / avgVar;
      //std::cout << ampDiffFall / avgVar << " " << diffAmpFactorFall << " " << ampDiffRise / avgVar << " " << diffAmpFactorRise << " " << ((lMax[iMax] - probablePeaks[nProbablePeaks-1] != 1) || (nProbablePeaks == 0)) << std::endl;
      if ((ampDiffFall / avgVar > diffAmpFactorFall) && (ampDiffRise / avgVar > diffAmpFactorRise) && ((lMax[iMax] - probablePeaks[nProbablePeaks-1] != 1) || (nProbablePeaks == 0))){
	probablePeaks[nProbablePeaks] =lMax[iMax];
	//std::cout << "pP1: " << lMax[iMax] << std::endl;
	edge[nProbablePeaks] = edge[iMax];
	if (mForMacro) mBins[lMax[iMax]].isProbPeak = true;
	nProbablePeaks++;
      }
    }
  }

  for (int iMax = 0; iMax < nMissedMax; iMax++){

    double ampDiffRise = mPolarity*mDataWF->GetBinContent(missedMax[iMax]) - mPolarity*mDataWF->GetBinContent(missedMax[iMax]+nBinsSlopeCheckRise);
    double ampDiffFall = mPolarity*mDataWF->GetBinContent(missedMax[iMax]) - mPolarity*mDataWF->GetBinContent(missedMax[iMax]-nBinsSlopeCheckFall);
    ampDiffRise /= nBinsSlopeCheckRise;
    ampDiffFall /= nBinsSlopeCheckFall;
    if (mForMacro) mBins[mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax]))].slopecheck = ampDiffRise / avgVar;
    if ((ampDiffFall / avgVar > diffAmpFactorFall) && (ampDiffRise / avgVar > diffAmpFactorRise) && (missedMax[iMax] - probablePeaks[nProbablePeaks-1] != 1)){
      probablePeaks[nProbablePeaks] = mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax]));
      //std::cout << "pP2: " << mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax])) << std::endl;
      edge[nProbablePeaks] = 0;
      if (mForMacro) mBins[mWF->FindBin(mDataWF->GetBinCenter(missedMax[iMax]))].isProbPeak = true;
      nProbablePeaks++;
    }
  }

  //Puts peaks in order                                                                                                                                                                                     
  int buffPeaks [100];
  int lowest = 0;
  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    if ((probablePeaks[iPeak] < probablePeaks[lowest])) lowest = iPeak;
  }
  buffPeaks[0] = probablePeaks[lowest];

  for (int iPeak = 1; iPeak < nProbablePeaks; iPeak++){
    for (int jPeak = 0; jPeak < nProbablePeaks; jPeak++){
      if ((probablePeaks[jPeak] > buffPeaks[iPeak-1])){
	lowest = jPeak;
	break;
      }
    }
    for (int jPeak = 0; jPeak < nProbablePeaks; jPeak++){
      if ((probablePeaks[jPeak] < probablePeaks[lowest]) && (probablePeaks[jPeak] > buffPeaks[iPeak-1])) lowest=jPeak;
    }
    buffPeaks[iPeak] = probablePeaks[lowest];
  }

  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    probablePeaks[iPeak] = buffPeaks[iPeak];
  }
      

  //Calculates the approximate rise of a probable peak on the falling edge above the pulse.                               
  double probPeakRise[100];

  int preBin;
  for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
    for(preBin = probablePeaks[iPeak]-1; mPolarity*mWF->GetBinContent(preBin-1)<=mPolarity*mWF->GetBinContent(preBin) && preBin > 0; preBin--)
      preBin--;

    probPeakRise[iPeak] = mPolarity*mWF->GetBinContent(probablePeaks[iPeak]) - mPolarity*mWF->GetBinContent(preBin);
    if (mForMacro) mBins[probablePeaks[iPeak]].rise = probPeakRise[iPeak];
  }

   //Estimates certainty that a probable peak is a peak. Currently only looks as maximum vertical drop between a peak and its neighbours, but code exits for using horizontal separations if that is used in future.
   double peakiness[100];
   double peakiness2[100];
   double peakRise[100];
   for (int iPeak = 0; iPeak < nProbablePeaks; iPeak++){
     bool isPulseMax = false;
     for (int iPulse = 0; iPulse < nPulses; iPulse++){
       if ((mWF->GetBinContent(probablePeaks[iPeak]) == mWF->GetBinContent(pulseMax[iPulse]))) isPulseMax = true;
     }
     bool isFirst = true;
     for (int iPulse = 0; iPulse < nPulses; iPulse++){
       if ((probablePeaks[iPeak] < pulses[iPulse][1]) && (probablePeaks[iPeak] > pulses[iPulse][0]) && (nPeaks > 0)){
	 for (int jPeak = 0; jPeak < nPeaks; jPeak++){
	   if ((peaks[jPeak] > pulses[iPulse][0])){
	     isFirst = false;
	     break;
	   }
	 }
       }
     }
     bool isSmall = false;
     for (int iSPR = 0; iSPR < nSmallPulseRegions; iSPR++){
       if ((probablePeaks[iPeak] < smallPulseRegions[iSPR][1]) && (probablePeaks[iPeak] >= smallPulseRegions[iSPR][0])) isSmall = true;
     }

     if ((isPulseMax)) peakiness[iPeak] = -1.0;
     else if ((nProbablePeaks == 1)) peakiness[iPeak] = -1.0;
     else if ((isFirst) && (mPolarity*mWF->GetBinContent(probablePeaks[iPeak]) > 1.5*lb)) peakiness[iPeak] = -1.0;
     else if ((isSmall)) peakiness[iPeak] = -1.0;
     else{
       int prevPeak;
       for (prevPeak = iPeak-1; prevPeak >= 0; prevPeak--){
	 if ((peakiness[prevPeak]==-1.0) || (peakiness[prevPeak] > peakinessThresh)) break;
       }
       double horiz = 20+log(mWF->GetBinCenter(probablePeaks[iPeak]) - mWF->GetBinCenter(probablePeaks[prevPeak]));
       double vert = (probPeakRise[iPeak]/(mPolarity*mWF->GetBinContent(probablePeaks[prevPeak])));
       peakiness[iPeak] = vert / horiz;
       //     std::cout << horiz << " " << vert << " " << probPeakRise[iPeak] << " " << mWF->GetBinContent(probablePeaks[prevPeak]) << std::endl;
     }
     if (mForMacro) mBins[probablePeaks[iPeak]].peakiness = peakiness[iPeak];

     //Decides finally upon what points are peaks based upon the 'peakiness' measurements above.
     if ((peakiness[iPeak] >= peakinessThresh) || (peakiness[iPeak] == -1.0)){
       peaks[nPeaks] = probablePeaks[iPeak];
       peakiness2[nPeaks] = peakiness[iPeak];
       peakRise[nPeaks] = probPeakRise[iPeak];
       if (mForMacro) mBins[probablePeaks[iPeak]].isPeak = true;
       if (mForMacro) mBins[peaks[nPeaks]].rise = peakRise[nPeaks];
       nPeaks++;
     }
   }

   //Calculates the approximate rise of a peak above the pulse.
   //double peakRise[100];
   //double peakRiseExtrap[100];
   //double peakRiseFit[100];
   //double derivative;

   //double fitPars[100][4];
      /*
   for (int iPeak = 0; iPeak < nPeaks; iPeak++){
     for(preBin = peaks[iPeak]-1; mPolarity*mWF->GetBinContent(preBin-1)<=mPolarity*mWF->GetBinContent(preBin) && preBin > 0; preBin--)
     preBin--;
     if ((preBin <=0)){
       peakRise[iPeak] = mPolarity*mWF->GetBinContent(peaks[iPeak]);
       peakRiseExtrap[iPeak] = mPolarity*mWF->GetBinContent(peaks[iPeak]);
       //peakRiseFit[iPeak] = mPolarity*mWF->GetBinContent(peaks[iPeak]);
     }
     else{
       peakRise[iPeak] = mPolarity*mWF->GetBinContent(peaks[iPeak]) - mPolarity*mWF->GetBinContent(preBin);
       derivative = (mPolarity*mWF->GetBinContent(preBin)-mPolarity*mWF->GetBinContent(preBin-1));
       peakRiseExtrap[iPeak] = peakRise[iPeak] - derivative * (peaks[iPeak] - preBin);
       /*if ((mPolarity*mWF->GetBinContent(2*preBin - peaks[iPeak]) > mPolarity*mWF->GetBinContent(preBin)) && (fitting)){
	 TF1* fit = new TF1("fit", "expo", mWF->GetBinCenter(2*preBin - peaks[iPeak]), mWF->GetBinCenter(preBin));
	 mWF->Fit("fit", "Q");
	 peakRiseFit[iPeak] = mPolarity*mWF->GetBinContent(peaks[iPeak]) - mPolarity*fit->Eval(mWF->GetBinCenter(peaks[iPeak]));
       
	 fitPars[iPeak][0] = mWF->GetBinCenter(2*preBin - peaks[iPeak]);
	 fitPars[iPeak][1] = mWF->GetBinCenter(preBin);
	 fitPars[iPeak][2] = fit->GetParameter(0);
	 fitPars[iPeak][3] = fit->GetParameter(1);
       }
       else{
	 peakRiseFit[iPeak] = peakRiseExtrap[iPeak];
	 fitPars[iPeak][0] = mWF->GetBinCenter(2*preBin - peaks[iPeak]);
	 fitPars[iPeak][1] = mWF->GetBinCenter(preBin);
	 fitPars[iPeak][2] = derivative / mWF->GetBinWidth(preBin);
	 fitPars[iPeak][3] = 0.0;
	 }*/
      //}

      //if (mForMacro) mBins[peaks[iPeak]].rise = peakRise[iPeak];
      //}

  //Places info about pulses into an EventData struct for each of access of macros
  mEventData.lb = mPolarity*lb;
  mEventData.nPulse = nPulses;
  mEventData.nPeak = nPeaks;
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    mEventData.pulseBounds[iPulse][0] = mWF->GetBinCenter(pulses[iPulse][0]);
    mEventData.pulseBounds[iPulse][1] = mWF->GetBinCenter(pulses[iPulse][1]);
    mEventData.pulseBounds[iPulse][2] = mWF->GetBinCenter(pulseMax[iPulse]);
  }
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    mEventData.peaks[iPeak] = mWF->GetBinCenter(peaks[iPeak]);
    mEventData.peakiness[iPeak] = peakiness2[iPeak];
    mEventData.peakBins[iPeak] = peaks[iPeak];
    //mEventData.fitPars[iPeak][0] = fitPars[iPeak][0];
    //mEventData.fitPars[iPeak][1] = fitPars[iPeak][1];
    //mEventData.fitPars[iPeak][2] = fitPars[iPeak][2];
    //mEventData.fitPars[iPeak][3] = fitPars[iPeak][3];
  }
  mEventData.nlMax = nlMax;

   // >>> Calculate the charge and width of each pulse
  double charges[100];
  int widths[100];
  int pulseNPeaks[100];
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    charges[iPulse] = 0;
    for (int iBin = pulses[iPulse][0]; iBin <= pulses[iPulse][1]; iBin++){
      charges[iPulse] += (mPolarity*mWF->GetBinContent(iBin) - mBmu)*mWF->GetBinWidth(iBin);
    }
    widths[iPulse] = pulses[iPulse][1] - pulses[iPulse][0];
  }

  //Calculates the number of peaks in each pulse, and assigns each peak to the pulse it lies on
  int whichPulse[100];
  for (int i = 0; i < 100; i++) whichPulse[i] = -1;
  int peakCount[100];
  for (int iPulse = 0; iPulse < nPulses; iPulse++) peakCount[iPulse] = 0;
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    for (int iPulse = 0; iPulse < nPulses; iPulse++){
      if((peaks[iPeak] > pulses[iPulse][0]) && (peaks[iPeak] < pulses[iPulse][1])){
	whichPulse[iPeak] = iPulse;
	peakCount[iPulse]++;
	break;
      }
    }
  }
  
  //Calculates the shortest and longest spacings between adjacent peaks in a pulse
  double spacings[100][10];
  int spacingcounts[100];
  for (int i=0; i<100; i++) spacingcounts[i] = 0;
  for (int iPeak = 1; iPeak < nPeaks; iPeak++){
    if((whichPulse[iPeak] == whichPulse[iPeak-1]) && (whichPulse[iPeak] != -1)){
      spacings[whichPulse[iPeak]][spacingcounts[whichPulse[iPeak]]] = (peaks[iPeak] - peaks[iPeak-1])*mWF->GetBinWidth(peaks[iPeak]);
      spacingcounts[whichPulse[iPeak]]++;
    }
  }
  double shortestSpacing[100];
  double longestSpacing[100];
  for (int iPulse = 0; iPulse < nPulses; iPulse++){
    if ((peakCount[iPulse]==1)){
      shortestSpacing[iPulse] = spacings[iPulse][0];
      longestSpacing[iPulse] = spacings[iPulse][0];
    }
    else{
      double shortest = spacings[iPulse][0];
      double longest = spacings[iPulse][0];
      for (int iSpac = 0; iSpac < spacingcounts[iPulse]; iSpac++){
	if((spacings[iPulse][iSpac] < shortest)) shortest = spacings[iPulse][iSpac];
	else if((spacings[iPulse][iSpac] > longest)) longest = spacings[iPulse][iSpac];
      }
      shortestSpacing[iPulse] = shortest;
      longestSpacing[iPulse] = longest;
    }
  }
  
  //Computes the average of bins within 50ns of each peak
  double fiftyNanoAvg[100];
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    fiftyNanoAvg[iPeak] = 0;
    for (int iBin = peaks[iPeak]; iBin < peaks[iPeak] + (50e-9 / mWF->GetBinWidth(peaks[iPeak])); iBin++){
      fiftyNanoAvg[iPeak] += mWF->GetBinContent(iBin);
    }
    fiftyNanoAvg[iPeak] /= (50e-9 / mWF->GetBinWidth(peaks[iPeak]));
  }
  
  
  //Sticks all computed information into Pulse struct for PulseFinder to access
  for (int iPeak = 0; iPeak < nPeaks; iPeak++){
    if((iPeak<mNPulseMax) && (whichPulse[iPeak] != -1)){
      mPulse[iPeak].mTime=mWF->GetBinCenter(peaks[iPeak]);
      mPulse[iPeak].mAbsAmp=mPolarity*mWF->GetBinContent(peaks[iPeak]);
      mPulse[iPeak].mAmp=mWF->GetBinContent(peaks[iPeak]);
      mPulse[iPeak].mBaseline=mBmu;
      mPulse[iPeak].mQ=charges[whichPulse[iPeak]];
      mPulse[iPeak].mWidth=widths[whichPulse[iPeak]];
      mPulse[iPeak].m50NanoAvg=fiftyNanoAvg[iPeak];
      mPulse[iPeak].mPeakRise=peakRise[iPeak];
      //mPulse[iPeak].mPeakRiseExtrap=peakRiseExtrap[iPeak];
      //mPulse[iPeak].mPeakRiseFit=peakRiseFit[iPeak];
      mPulse[iPeak].mAvgVar = avgVar;
    }
  }
  mNPulse = nPeaks;
  return 1;
}

int WaveformProcessor::findSmallPulse(int startingBin, int endingBin){
  //Check for noise
  int nlMax = 0;
  int lMax [1000];
  for (int iBin = startingBin; iBin <= endingBin; iBin++){
    if((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin+1)))){
      lMax[nlMax] = iBin;
      nlMax++;
    }
    else if ((mPolarity*(mWF->GetBinContent(iBin)) > mPolarity*(mWF->GetBinContent(iBin-1))) && (mPolarity*(mWF->GetBinContent(iBin)) == mPolarity*(mWF->GetBinContent(iBin+1)))){
      int jBin;
      for (jBin = iBin+1; mPolarity*(mWF->GetBinContent(jBin)) == mPolarity*(mWF->GetBinContent(iBin)); jBin++){
      }
      if ((mPolarity*(mWF->GetBinContent(jBin-1)) > mPolarity*(mWF->GetBinContent(jBin)))){
        for (int kBin = iBin; kBin < jBin; kBin++){
          lMax[nlMax] = kBin;
          nlMax++;
        }
        iBin = jBin-1;
      }
    }

  }
  int highestFront = 0;
  int highestBack = 0;
  for (int iMax = 1; iMax < nlMax; iMax++){
    if ((lMax[iMax] <= ((startingBin+endingBin)/2)) && (mPolarity*(mWF->GetBinContent(lMax[iMax])) > mPolarity*(mWF->GetBinContent(lMax[highestFront])))) highestFront = iMax;
    else if ((lMax[iMax] > ((startingBin+endingBin)/2)) && (mPolarity*(mWF->GetBinContent(lMax[iMax])) > mPolarity*(mWF->GetBinContent(lMax[highestBack])))) highestBack = iMax;
  }
  int max;
  if ((mPolarity*(mWF->GetBinContent(lMax[highestBack])) > 1.5 * mPolarity*(mWF->GetBinContent(lMax[highestFront])))) max = lMax[highestBack];
  else max = lMax[highestFront];
  return max;
}


// ---
// Takes peaks and puts them in TGraph
TGraph* WaveformProcessor::getPulseGraph(){
    double* tTime = new double[mNPulse];
    double* tAmp = new double[mNPulse];
    for(int iPulse=0; iPulse<mNPulse; iPulse++){
        tTime[iPulse]=mPulse[iPulse].mTime;
        tAmp[iPulse]=mPulse[iPulse].mAmp;
    }
    TGraph* tG = new TGraph(mNPulse,tTime,tAmp); //TGraph(number of points, xArray, yArray)
    delete[] tTime;
    delete[] tAmp;
    return tG;
}

void WaveformProcessor::smoothWaveform(double lenNs){
  //Boxcar method
  int nBins=mWF->GetNbinsX();
  double secPerBin=mWF->GetBinWidth(1);
  double len=lenNs*1e-09/secPerBin;
  double value=0;
  if (!mSmoothedWF)
    mSmoothedWF = new Waveform(nBins-len*2,mWF->GetBinCenter(0)+mWF->GetBinWidth(1)*(len),mWF->GetBinCenter(nBins)-mWF->GetBinWidth(1)*len,"smoothed");
  for (int j=0;j<=len*2;j++)  //Calculate smoothed value for first bin
    value+=mWF->GetBinContent(j);
  mSmoothedWF->SetBinContent(0,value/(len*2.+1.));  //Set smoothed value for first bin
  for (int i=len+1;i<nBins-len;i++){  //Caculate smoothed values for other bins
    value-=mWF->GetBinContent(i-len-1); //Remove first bin from previous calculation
    value+=mWF->GetBinContent(i+len); //Add last bin to current calculation
    mSmoothedWF->SetBinContent(i-len,value/(len*2.+1.));
  }
  mDataWF=mWF;
  mWF=mSmoothedWF;
}

//----------------------------
bool WaveformProcessor::checkZero(int start, int end, double level, bool simple, int verbose){
  if (end<0)  //if end is given as <0, use entire histogram
    end=mWF->GetNbinsX();
  
  //variables
  double tCharge=0.;
  int iPulseStart=-1;
  int iPulseEnd=-1;
  double pulseWidth=-1.;
  double maxHeight=-1;
  int maxLoc=-1;
  int cCharge;
  
  if (verbose>1){
    std::cout << "mBRMS=" << mBRMS << std::endl;
    std::cout << "mBmu=" << mBmu << std::endl;
  }
  
  //convert risetime and falltime from seconds to bins
  int nBinRiseTime = round(mRiseTimeSig/mWF->GetBinWidth(1));
  int nBinFallTime = round(mFallTimeTau/mWF->GetBinWidth(1));
  int tPulseWidth=nBinRiseTime+nBinFallTime;
  double localBaseline=0;
  for (int i=start-tPulseWidth; i<start; i++){
    if (i<0)
      localBaseline+=mBmu;
    else
      localBaseline+=mWF->GetBinContent(i);
  }
  localBaseline/=tPulseWidth;
    
  //loop through histogram
  for (int i=start;i<=end;i++){
    localBaseline+=mWF->GetBinContent(i)/tPulseWidth;
    localBaseline+=mWF->GetBinContent(i-tPulseWidth)/tPulseWidth;
    
    if ((mWF->GetBinContent(i)-localBaseline)*mPolarity>mBRMS*level){  //Check amplitude; proceed below if amplitude above threshold
      if (verbose>2)
        std::cout << "i=" << i << std::endl;
      if (simple)
        return false;
      
      for (int j=i;mPolarity*mWF->GetBinContent(j)>mBmu*mPolarity;j--){ //Find beginning of pulse
        if ((mWF->GetBinContent(j)-mBmu)*mPolarity>maxHeight){ //record maximum point in peak
          maxHeight=(mWF->GetBinContent(j)-mBmu)*mPolarity;
          maxLoc=j;
        }
        iPulseStart=j;
      }
      if (verbose>3){
        std::cout << "(mWF->GetBinContent(maxLoc)-mBmu)*mPolarity=" << (mWF->GetBinContent(maxLoc)-mBmu)*mPolarity << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      for (int j=i;mPolarity*mWF->GetBinContent(j)>mBmu*mPolarity;j++){ //Find end of pulse
        if ((mWF->GetBinContent(j)-mBmu)*mPolarity>maxHeight){ //record maximum point in peak
          maxHeight=(mWF->GetBinContent(j)-mBmu)*mPolarity;
          maxLoc=j;
        }
        iPulseEnd=j;
      }
      if (verbose>3){
        std::cout << "(mWF->GetBinContent(maxLoc)-mBmu)*mPolarity=" << (mWF->GetBinContent(maxLoc)-mBmu)*mPolarity << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      pulseWidth=iPulseEnd-iPulseStart;
      
      // >>> Calculate the charge (for removing noise)
      for (int j=maxLoc-nBinRiseTime; j<=maxLoc+nBinFallTime; j++){
        tCharge+=(mWF->GetBinContent(j)-mBmu)*mPolarity;
      }
      
      cCharge = 0.5*level*mBRMS*(nBinRiseTime+nBinFallTime);  //Use traingle approximation with rise and fall time to calculate theoretical charge
      
      if (verbose>2){
        std::cout << "pulseWidth=" << pulseWidth << std::endl;
        std::cout << "iPulseStart=" << iPulseStart << std::endl;
        std::cout << "iPulseEnd=" << iPulseEnd << std::endl;
        std::cout << "nBinRiseTime+nBinFallTime=" << nBinRiseTime+nBinFallTime<< std::endl;
        std::cout << "mBRMS=" << mBRMS << std::endl;
        std::cout << "mBmu=" << mBmu << std::endl;
        std::cout << "tCharge=" << tCharge << std::endl;
        std::cout << "cCharge=" << cCharge << std::endl;
        std::cout << "maxLoc=" << maxLoc << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      
      if ((tCharge>cCharge && pulseWidth>(nBinRiseTime+nBinFallTime)*0.7) //numbers are tuned, but should work in general
          || (tCharge>0.8*cCharge && pulseWidth>(nBinRiseTime+nBinFallTime)*0.9)){ //for small pulses
        if (verbose>0 && verbose<3){
          std::cout << "pulseWidth=" << pulseWidth << std::endl;
          std::cout << "iPulseStart=" << iPulseStart << std::endl;
          std::cout << "iPulseEnd=" << iPulseEnd << std::endl;
          std::cout << "nBinRiseTime+nBinFallTime=" << nBinRiseTime+nBinFallTime<< std::endl;
          std::cout << "mBRMS=" << mBRMS << std::endl;
          std::cout << "mBmu=" << mBmu << std::endl;
          std::cout << "tCharge=" << tCharge << std::endl;
          std::cout << "cCharge=" << cCharge << std::endl;
          std::cout << "maxLoc=" << maxLoc << std::endl;
          std::cout << "maxHeight=" << maxHeight << std::endl;
        }
        return false; //pulse found, so no "zero"
      }
      else{ //reset variables for next pulse candidate
        i=iPulseEnd+1;
        tCharge=0.;
        iPulseStart=-1;
        iPulseEnd=-1;
        pulseWidth=-1.;
        maxHeight=-1;
        maxLoc=-1;
      }
    }
  }
  return true;  //no pulse found, so "zero" found
}

//----------------------------

double WaveformProcessor::findMin(int start, int end, int verbose){ //**Will find maximum of positive pulses**
  if (end<0)  //if end is given as <0, use entire histogram
    end=mWF->GetNbinsX();
  
  //variables
  double minHeight=100000;
  int minLoc=-1;
  
  //loop through histogram
  for (int i=start;i<end+1;i++)
    if (mWF->GetBinContent(i)*mPolarity*-1<minHeight){
      minHeight=mWF->GetBinContent(i)*mPolarity*-1;
      minLoc=i;
    }
  
  if (verbose>1)
    std::cout << "minLoc=" << minLoc << "\n";
  return minHeight;
}
/*
double WaveformProcessor::findMax(int start, int end, int verbose){
  if (end<0)  //if end is given as <0, use entire histogram
    end=mWF->GetNbinsX();
  
  //variables
  double maxHeight=-100000;
  int maxLoc=-1;
  
  //loop through histogram
  for (int i=start;i<end;i++)
    if (mWF->GetBinContent(i)>maxHeight){
      maxHeight=mWF->GetBinContent(i);
      maxLoc=i;
    }
  
  if (verbose>1)
    std::cout << "maxLoc=" << maxLoc << "\n";
  return maxHeight;
}
*/
//----------------------------
void WaveformProcessor::findClearPulses(TNtuple* ntp, int start, int end, double level, int verbose){
  mEv++;
  
  //variables
  if (start<0) start=1;
  if (end<0) end=mWF->GetNbinsX();
  double tCharge=0.;
  int iPulseStart=-1;
  int iPulseEnd=-1;
  double pulseWidth=-1.;
  double maxHeight=-1;
  int maxLoc=-1;
  int cCharge;
  double secPerBin=mWF->GetBinWidth(1);
  
  if (verbose>1){
    std::cout << "mBRMS=" << mBRMS << std::endl;
    std::cout << "mBmu=" << mBmu << std::endl;
  }
  
  //convert risetime and falltime from seconds to bins
  int nBinRiseTime = round(mRiseTimeSig/mWF->GetBinWidth(1));
  int nBinFallTime = round(mFallTimeTau/mWF->GetBinWidth(1));
    
  //loop through histogram
  for (int i=start;i<end;i++){
    
    if ((mWF->GetBinContent(i)-mBmu)*mPolarity>mBRMS*level){  //Check amplitude; proceed below if amplitude above threshold
      if (verbose>2)
        std::cout << "i=" << i << std::endl;
      
      for (int j=i;mPolarity*mWF->GetBinContent(j)>mBmu*mPolarity && j>start;j--){ //Find beginning of pulse
        if ((mWF->GetBinContent(j)-mBmu)*mPolarity>maxHeight){ //record maximum point in peak
          maxHeight=(mWF->GetBinContent(j)-mBmu)*mPolarity;
          maxLoc=j;
        }
        iPulseStart=j;
      }
      if (verbose>3){
        std::cout << "(mWF->GetBinContent(maxLoc)-mBmu)*mPolarity=" << (mWF->GetBinContent(maxLoc)-mBmu)*mPolarity << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      for (int j=i;mPolarity*mWF->GetBinContent(j)>mBmu*mPolarity && j<end;j++){ //Find end of pulse
        if ((mWF->GetBinContent(j)-mBmu)*mPolarity>maxHeight){ //record maximum point in peak
          maxHeight=(mWF->GetBinContent(j)-mBmu)*mPolarity;
          maxLoc=j;
        }
        iPulseEnd=j;
      }
      if (verbose>3){
        std::cout << "(mWF->GetBinContent(maxLoc)-mBmu)*mPolarity=" << (mWF->GetBinContent(maxLoc)-mBmu)*mPolarity << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      
      pulseWidth=iPulseEnd-iPulseStart;
      
      // >>> Calculate the charge
      for (int j=maxLoc-nBinRiseTime; j<=maxLoc+nBinFallTime; j++){
        tCharge+=(mWF->GetBinContent(j)-mBmu)*mPolarity;
      }
      
      if (verbose>0 && verbose<3){
        std::cout << "pulseWidth=" << pulseWidth << std::endl;
        std::cout << "iPulseStart=" << iPulseStart << std::endl;
        std::cout << "iPulseEnd=" << iPulseEnd << std::endl;
        std::cout << "nBinRiseTime+nBinFallTime=" << nBinRiseTime+nBinFallTime<< std::endl;
        std::cout << "mBRMS=" << mBRMS << std::endl;
        std::cout << "mBmu=" << mBmu << std::endl;
        std::cout << "tCharge=" << tCharge << std::endl;
        std::cout << "iPulseStart*secPerBin*1e9=" << iPulseStart*secPerBin << std::endl;
        std::cout << "maxLoc=" << maxLoc << std::endl;
        std::cout << "maxHeight=" << maxHeight << std::endl;
      }
      ntp->Fill(mEv,iPulseStart*secPerBin,maxHeight,tCharge,pulseWidth,getTriggerTime());
      i+=nBinRiseTime+nBinFallTime;
      //reset variables for next pulse candidate
      tCharge=0.;
      iPulseStart=-1;
      iPulseEnd=-1;
      pulseWidth=-1.;
      maxHeight=-1;
      maxLoc=-1;
    }
  }
}

void WaveformProcessor::findPulse2(TNtuple* ntp, int start, int end, double level, int verbose){
  //Record WF#
  mEv++;
  
  //variables
  if (start<0) start=1;
  if (end<0) end=mWF->GetNbinsX();
  double tCharge=0.;
  double pulseWidth=-1.;
  double pHeight=-1;
  int maxLoc=-1;
  int cCharge;
  double secPerBin=mWF->GetBinWidth(1);
  int consec;
  int ilPeak;
  int endPeak;
  bool first=true;
  
  if (verbose>2){
    std::cout << "mBRMS=" << mBRMS << std::endl;
    std::cout << "mBmu=" << mBmu << std::endl;
  }
  
  //convert risetime and falltime from seconds to bins
  int nBinRiseTime = round(mRiseTimeSig/mWF->GetBinWidth(1));
  int nBinFallTime = round(mFallTimeTau/mWF->GetBinWidth(1));
  
  calcDeriv(3);
  getDeriv(3);
    
  //loop through histogram
  for (int i=start+3;i<end;i++){
    
    if (mDeriv[3]->GetBinContent(i-3)*mPolarity>mDerivRMS[3]*7)
    /*if ((mWF->GetBinContent(i)-mBmu)*mPolarity>mBRMS*level)*/{  //Check amplitude; proceed below if amplitude above threshold
      //Now need to check that candidate has minimum width above threshold
      
      //Check total width of pulse above threshold
      consec=0;
      while ((mWF->GetBinContent(i+consec)-mBmu)*mPolarity>mBRMS*level && i+consec<end)
        consec++;
      
      //Record location where peak first went above baseline
      ilPeak=i;
      while ((mWF->GetBinContent(ilPeak)-mBmu)*mPolarity>(mWF->GetBinContent(ilPeak-1)-mBmu)*mPolarity && ilPeak>start)
        ilPeak--;
      
      //Find end of peak
      endPeak=i;
      while (mDeriv[3]->GetBinContent(endPeak-3)>mDerivmu[3] && endPeak<end)  //Get off rising edge
        endPeak++;
      while (mDeriv[3]->GetBinContent(endPeak-3)<mDerivmu[3]*3.5 && (mWF->GetBinContent(endPeak)-mBmu)*mPolarity>mBRMS*level && endPeak<end)  //Follow falling edge till another pulse appears, or the falling edge goes below the threshold
        endPeak++;
      
      //Find width of peak above baseline
      for (int j=endPeak;mDeriv[3]->GetBinContent(j-3)*mPolarity<mDerivmu[3]*3.5 && (mWF->GetBinContent(j)-mBmu)*mPolarity>mBRMS && j<end;j++)
        pulseWidth=j-ilPeak;
      
      //Record peak height
      pHeight=-1e9;
      for (int j=ilPeak;j<endPeak;j++)
        if (mWF->GetBinContent(j)*mPolarity>pHeight){
          pHeight=mWF->GetBinContent(j)*mPolarity;
          maxLoc=j;
        }
      
      //Integrate peak
      tCharge=mWF->Integral(ilPeak,ilPeak+pulseWidth)*mPolarity;
      /*
      consec++;
      ilPeak=i;
      detect=i;
      pHeight=mBRMS*level;
      while ((mWF->GetBinContent(ilPeak)-mBmu)*mPolarity>(mWF->GetBinContent(ilPeak-1)-mBmu)*mPolarity)
        ilPeak--;
      while (candidate){
        if ((mWF->GetBinContent(ilPeak+consec)-mBmu)*mPolarity>pHeight)
          pHeight=(mWF->GetBinContent(ilPeak+consec)-mBmu)*mPolarity;
        if ((mWF->GetBinContent(ilPeak+consec)-mBmu)*mPolarity>mBRMS*level)
          consec++;
        else{
          consec=0;
          pHeight=0;
        }
        if (consec>5e-9/secPerBin){
          consec=0;
          inpeak=true;
    }
      */
    //TNtuple* ntp = new TNtuple("ntp","ntp","W#:detectBin:detectTime:startPeak:height:charge:width:tAbove:endPeak:maxLoc:first:binWidth");
    ntp->Fill(mEv,i,mWF->GetBinCenter(i)*1e9,ilPeak,pHeight,tCharge,pulseWidth,consec,endPeak,maxLoc,first,secPerBin);
    if (verbose>0)
      std::cout << "Pulse found: W#=" << mEv << " detectBin=" << i << " detectTime=" << mWF->GetBinCenter(i)*1e9 << " startPeak=" << ilPeak << " height=" << pHeight << " charge=" << tCharge << " width=" << pulseWidth << " tAbove=" << consec << " endPeak=" << endPeak << " maxLoc=" << maxLoc << " first=" << first << " secPerBin=" << secPerBin << std::endl;
    i=endPeak;
    //Record whether pulse was the first in the region
    first=false;
    }
  }
}
//----------------------------
bool WaveformProcessor::findPulseOld(int start, int end){
  if (end<=0)  //if end is given as <0, use entire histogram
    end=mDataFile->getWaveformCount();
  if (start<0)
    start=0;
    
  int nBins=mWF->GetNbinsX();
  
  //variables
  bool candidate=false;
  bool inPeak=false;
  double tCharge=0.;
  int iPulseStart=-1;
  int iPulseEnd=-1;
  double pulseWidth=-1.;
  double maxHeight=-1;
  int maxLoc=-1;
  
  //convert risetime and falltime from seconds to bins
  int nBinRiseTime = round(mRiseTimeSig/mWF->GetBinWidth(1));
  int nBinFallTime = round(mFallTimeTau/mWF->GetBinWidth(1));
  
  //loop through histogram
  for (int i=0;i<nBins;i++){
    if ((mWF->GetBinContent(i)-mBmu)*mPolarity<mBRMS*4)
      inPeak=false; //Finish scanning peak for maximum height
    
    if ((mWF->GetBinContent(i)-mBmu)*mPolarity>mBRMS*4){
      candidate=true; //peak candidate found
      inPeak=true;  //start scanning for maximum
    }
    
    if (inPeak && mWF->GetBinContent(i)*mPolarity>maxHeight){ //record maximum point in peak
      maxHeight=mWF->GetBinContent(i)*mPolarity;
      maxLoc=i;
    }
    
    if (candidate && !inPeak){
      // >>> Calculate the charge (for removing noise)
      for(int i=maxLoc-nBinRiseTime; i<maxLoc+nBinFallTime; i++){
        tCharge+=mWF->GetBinContent(i);
        tCharge-=mBmu;
//         std::cout << "mPolarity*(mWF->GetBinContent(i))" << mPolarity*(mWF->GetBinContent(i)) << std::endl;
        if(mPolarity*(mWF->GetBinContent(i)-mBmu)/(maxHeight-mBmu)>0.05){ //for pulse width
            if(iPulseStart<0) iPulseStart=i;
            iPulseEnd=i;
        }
        else{ //calculate pulse width
            if(iPulseStart>=0 && pulseWidth<(iPulseEnd-iPulseStart)) pulseWidth=iPulseEnd-iPulseStart;
            iPulseStart=-1; //reset pulse width variables in case noise caued pulse width calculation to end prematurely
            iPulseEnd=-1;
        }
      }
      tCharge*=mPolarity;
      if(pulseWidth<0.) pulseWidth=iPulseEnd-iPulseStart;
      /*
      std::cout << "pulseWidth=" << pulseWidth << std::endl;
      std::cout << "mBmu=" << mBmu << std::endl;
      std::cout << "nBinRiseTime+nBinFallTime=" << nBinRiseTime+nBinFallTime<< std::endl;
      std::cout << "iPulseStart=" << iPulseStart << std::endl;
      std::cout << "iPulseEnd=" << iPulseEnd << std::endl;
      std::cout << "tCharge=" << tCharge << std::endl;
      std::cout << "maxLoc=" << maxLoc << std::endl;
      */
      if (tCharge>3*(nBinRiseTime+nBinFallTime)*mNoise1Bin && pulseWidth>(nBinRiseTime+nBinFallTime)/3) //numbers are tuned, but should work in general
        return false; //pulse found, so no "zero"
      else{ //reset variables for next pulse candidate
        candidate=false;
        inPeak=false;
        tCharge=0.;
        iPulseStart=-1;
        iPulseEnd=-1;
        pulseWidth=-1.;
        maxHeight=-1;
        maxLoc=-1;
      }
    }
  }
  return true;  //no pulse found, so "zero" found
}

//
// --- Single PE checker
// Compares pulse fitted accorinding to template to pulse in WF
// Not useful because template method not effective
void WaveformProcessor:: calcSinglePulseTemplateChi2(){
    // >>> set the function parameter
    mFExpGaus->SetParameter(0,mBmu);
    mFExpGaus->SetParameter(1,mRiseTimeSig);
    mFExpGaus->SetParameter(2,mFallTimeTau);
    mFExpGaus->SetParameter(3,1);
    for(int iPulse=1; iPulse<mNFitPulseMax; iPulse++){
        mFExpGaus->FixParameter(4+2*iPulse,0);
        mFExpGaus->FixParameter(5+2*iPulse,0);
    }
    for(int iPulse=0; iPulse<mNPulse; iPulse++){
        // >>>
        mFExpGaus->SetParameter(4,mPulse[iPulse].mAmp-mBmu);
        mFExpGaus->SetParameter(5,mPulse[iPulse].mTime);
        // >>>
        int iFirstBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mTime-mRiseTimeSig*5);
        if(iFirstBin<1) iFirstBin=1;
        int iLastBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mTime+mFallTimeTau*3);
        if(iLastBin>mWF->GetNbinsX()) iLastBin=mWF->GetNbinsX();

        // >>>
        mPulse[iPulse].mSPTemplateChi2=0;
        for(int iBin=iFirstBin; iBin<=iLastBin; iBin++){
            double val = (mWF->GetBinContent(iBin)-mFExpGaus->Eval(mWF->GetBinCenter(iBin)))/mNoise1Bin;
            mPulse[iPulse].mSPTemplateChi2+= (val*val);
        }
        mPulse[iPulse].mSPTemplateChi2/=(iLastBin-iFirstBin);
    }
}

//___________________________________________________________________
// --- fit Pulse
// Move to header...
void WaveformProcessor::setFitOption(const char* aOption){
    strcpy(mFitOption,aOption);
}
// Primary fit function
void WaveformProcessor::fit(int aType){
    // aType = multiFit + 10* templateCheck . multiFit=2 for refiting 1 otherwise
    int tFitMulti = aType/100;
    int tTemplateCheck = (aType%100)/10;
    int tFitType = (aType%100)%10;
    //std::cout << "FIT TYPES: " << tFitMulti << " " << tTemplateCheck << " " << tFitType << std::endl;

    // >>> Template calculation (always do it for checking result)
    // Check if single pulse is good enough
    calcSinglePulseTemplateChi2();

    // >>> Set Histo error
    for(int iBin=1; iBin<=mWF->GetNbinsX(); iBin++){
        mWF->SetBinError(iBin,mNoise1Bin);
    }

    if(aType>0){
        int iPulse=0;
        while(iPulse<mNPulse){
            // --- Skip fit if template comparison was good (i.e. if there was only one pulse)
            if(tTemplateCheck && mPulse[iPulse].mSPTemplateChi2<mMaxSPTemplateChi2){
                mPulse[iPulse].mFitChi2 = mPulse[iPulse].mSPTemplateChi2;
                mPulse[iPulse].mFitNDF = -1;
                iPulse++;
            }
            else{

                // >>> Calculate fit limits
                // >>> Low limit given by first pulse
                mPulse[iPulse].mFitLowLimit = mPulse[iPulse].mTime-mRiseTimeSig*5-5*mWF->GetBinWidth(1); // fit range (low limit). Add 5 bins for baseline
                if(mPulse[iPulse].mFitLowLimit<mWF->GetXaxis()->GetXmin())
                    mPulse[iPulse].mFitLowLimit = mWF->GetXaxis()->GetXmin();
                // >>> Pulses may be merged in a group to be fitted together. The limit is adjusted accordingly
                int tNPulseInGroup=1; // total number of pulses fitted together
                mPulse[iPulse].mFitHighLimit =
                        mPulse[iPulse].mTime+mFallTimeTau*3+mRiseTimeSig*5+5*mWF->GetBinWidth(1);
                while(iPulse+tNPulseInGroup<mNPulse-1 &&
                      mPulse[iPulse].mFitHighLimit>mPulse[iPulse+tNPulseInGroup].mTime){
                    mPulse[iPulse].mFitHighLimit = mPulse[iPulse+tNPulseInGroup].mTime+mFallTimeTau*3+mRiseTimeSig*5+5*mWF->GetBinWidth(1);
                    tNPulseInGroup++;
                }
                if(mPulse[iPulse].mFitHighLimit>mWF->GetBinLowEdge(mWF->GetNbinsX()))
                    mPulse[iPulse].mFitHighLimit = mWF->GetBinLowEdge(mWF->GetNbinsX());
                for(int iFitPulse=iPulse; iFitPulse<iPulse+tNPulseInGroup; iFitPulse++){
                    mPulse[iFitPulse].mFirstPulseInGroup = iPulse;
                    mPulse[iFitPulse].mFitLowLimit=mPulse[iPulse].mFitLowLimit;
                    mPulse[iFitPulse].mFitHighLimit=mPulse[iPulse].mFitHighLimit;
                }

                // --- Set fit paremeters
                mFExpGaus->FixParameter(3,tNPulseInGroup);
                switch(tFitType){
                case 1: // fixed time constants but free baseline
                    mFExpGaus->ReleaseParameter(0); // testing: fix the baseline
                    mFExpGaus->FixParameter(0,mBmu);
                    mFExpGaus->FixParameter(1,mRiseTimeSig);
                    mFExpGaus->FixParameter(2,mFallTimeTau);
                    break;
                case 2: // free time constant but fixed baseline
                    mFExpGaus->FixParameter(0,mBmu);
                    mFExpGaus->ReleaseParameter(1);
                    mFExpGaus->SetParameter(1,mRiseTimeSig);
                    mFExpGaus->ReleaseParameter(2);
                    mFExpGaus->SetParameter(2,mFallTimeTau);
                    break;
                case 3: // free time constant but with tight limits
                    mFExpGaus->ReleaseParameter(0);
                    mFExpGaus->SetParameter(0,mBmu);
                    mFExpGaus->ReleaseParameter(1);
                    mFExpGaus->SetParameter(1,mRiseTimeSig);
                    mFExpGaus->SetParLimits(1,mRiseTimeSig*0.1, mRiseTimeSig*10);
                    mFExpGaus->ReleaseParameter(2);
                    mFExpGaus->SetParameter(2,mFallTimeTau);
                    mFExpGaus->SetParLimits(2,mFallTimeTau*0.1, mFallTimeTau*10);
                    break;
                }
                for(int iFitPulse=0; iFitPulse<tNPulseInGroup; iFitPulse++){
                    mFExpGaus->ReleaseParameter(4+2*iFitPulse); // Release amplitude
                    mFExpGaus->SetParameter(4+2*iFitPulse,(mPulse[iFitPulse+iPulse].mAmp-mBmu));  // Set amplitude
                    mFExpGaus->ReleaseParameter(5+2*iFitPulse); //Release time
                    mFExpGaus->SetParameter(5+2*iFitPulse,mPulse[iFitPulse+iPulse].mTime); // Set time
                    mFExpGaus->SetParLimits(5+2*iFitPulse,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit); // bad things happen when pulses are allowed in front.
                }
                for(int iFitPulse=tNPulseInGroup; iFitPulse<mNFitPulseMax; iFitPulse++){  // Set remaining pulses in array to 0
                    mFExpGaus->FixParameter(4+2*iFitPulse,0);
                    mFExpGaus->FixParameter(5+2*iFitPulse,0);
                }
                mFExpGaus->SetRange(mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                //std::cout << "fit limits: " << mPulse[iPulse].mFitLowLimit << "\t" << mPulse[iPulse].mFitHighLimit << std::endl;
                mWF->Fit("FExpGaus",mFitOption);

                //>>> Store the fit information
                for(int iFitPulse=0; iFitPulse<tNPulseInGroup; iFitPulse++){
                    mPulse[iFitPulse+iPulse].mFitAmp = mFExpGaus->GetParameter(4+2*iFitPulse);
                    mPulse[iFitPulse+iPulse].mFitTime = mFExpGaus->GetParameter(5+2*iFitPulse);
                    mPulse[iFitPulse+iPulse].mFitBaseline = mFExpGaus->GetParameter(0);
                    mPulse[iFitPulse+iPulse].mFitRiseTime = mFExpGaus->GetParameter(1);
                    mPulse[iFitPulse+iPulse].mFitFallTime = mFExpGaus->GetParameter(2);
                    mPulse[iFitPulse+iPulse].mFitChi2 = mFExpGaus->GetChisquare();
                    mPulse[iFitPulse+iPulse].mFitNDF = mFExpGaus->GetNDF();
                    mPulse[iFitPulse+iPulse].mRefitChi2 = -1;
                    mPulse[iFitPulse+iPulse].mRefitNDF = -1;
                   
                    //std::cout<< "Fit reduced chi2:"<< mFExpGaus->GetChisquare()/mFExpGaus->GetNDF() << std::endl;
                }
                iPulse+=tNPulseInGroup;
            }
        }
    }
    if(tFitMulti) reFitMulti();
    // sorting of pulses should happen here....

    // >>> Sort
    if(mNPulse>1){
        for(int iPulse=0; iPulse<mNPulse; iPulse++){
            mPulseIndex[iPulse]=iPulse;
            mPulseTime[iPulse]=mPulse[iPulse].mFitTime;
        }
        TMath::Sort(mNPulse, mPulseTime, mPulseIndex, false); // root sorts from high to low per default
    }
}



// ---
// Not all that useful
void WaveformProcessor::reFitMulti(){
    //    std::cout << "WaveformProcessor::reFitMulti  " << mNPulse<< std::endl;
    int iPulse=0;
    while(iPulse<mNPulse){
        if(mPulse[iPulse].mFirstPulseInGroup==iPulse &&
                mPulse[iPulse].mFitNDF>0 &&
                (mPulse[iPulse].mFitChi2/mPulse[iPulse].mFitNDF)>mMinChi2ForRefit){ // refit this group

            //            std::cout << "Refitting " << iPulse << " "
            //                      << mPulse[iPulse].mFitLowLimit << " " << mPulse[iPulse].mFitHighLimit << std::endl;

            // >>>
            mFExpGaus->SetRange(mPulse[iPulse].mFitLowLimit, mPulse[iPulse].mFitHighLimit);
            mFExpGaus->ReleaseParameter(0);
            mFExpGaus->SetParameter(0,mBmu);
            mFExpGaus->FixParameter(1,mRiseTimeSig);
            mFExpGaus->FixParameter(2,mFallTimeTau);
            mPulse[iPulse].mRefitChi2 = mPulse[iPulse].mFitChi2;
            mPulse[iPulse].mRefitNDF = mPulse[iPulse].mFitNDF;

            double tMinTimeDiff=1.; // minimum time between two pulses
            int tNFitPulse=1;
            while(tNFitPulse<(mNFitPulseMax-1) && //can add one more pulse due to n pulse fit limit
                  mNPulse<(mNPulseMax-1) &&  //can add one more pulse due to total n pulse limit
                  mPulse[iPulse].mRefitNDF>0 &&
                  (mPulse[iPulse].mRefitChi2/mPulse[iPulse].mRefitNDF)>mMinChi2ForRefit &&
                  tMinTimeDiff>2e-9)
            {

                // >>> Set fit function starting parameters without additional pulse
                int iFitParameter=0;
                for(int iFitPulse=iPulse; iFitPulse<mNPulse; iFitPulse++){
                    if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                        mFExpGaus->ReleaseParameter(4+2*iFitParameter);
                        // numeric issue in root: If the FitParameter is exactly at the Range edge
                        // it will create an fit error and it should never be larger
                        if(mPulse[iFitPulse].mFitAmp < mNoise1Bin &&mPulse[iFitPulse].mFitAmp > -10)
                            mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp);
                        else if(mPulse[iFitPulse].mFitAmp == mNoise1Bin)
                            mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000.);
                        else if(mPulse[iFitPulse].mFitAmp == mNoise1Bin)
                            mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp+mNoise1Bin/100000.);

                        //    mFExpGaus->SetParameter(4+2*iFitParameter,mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000.);
                        mFExpGaus->SetParLimits(4+2*iFitParameter,-10.,mNoise1Bin);
                        mFExpGaus->ReleaseParameter(5+2*iFitParameter);
                        mFExpGaus->SetParameter(5+2*iFitParameter,mPulse[iFitPulse].mFitTime);
                        mFExpGaus->SetParLimits(5+2*iFitParameter,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                        iFitParameter++;

                        //                        if(mPulse[iFitPulse].mFitAmp-mNoise1Bin/100000. > mNoise1Bin || mPulse[iFitPulse].mFitAmp < -10.)
                        //                            std::cout << "Amp: " <<mPulse[iFitPulse].mFitAmp << " "<< mNoise1Bin <<std::endl;
                        //                        if(mPulse[iFitPulse].mFitTime > mPulse[iPulse].mFitHighLimit || mPulse[iFitPulse].mFitTime < mPulse[iPulse].mFitLowLimit)
                        //                            std::cout << "time" << std::endl;

                    }
                }
                // baseline set to fixed value - will increase the speed and makes sure, that the right baseline is used.
                mFExpGaus->FixParameter(0,mBmu);
                //std::cout<<"MuBaseline Parameter " << mBmu<<std::endl;
                mFExpGaus->FixParameter(3,tNFitPulse);

                // >>> Look for most likely position of next pulse
                int tLastBin = mWF->GetXaxis()->FindBin(mPulse[iPulse].mFitHighLimit)-1;
                int iMaxDiff;
                double maxDiff=0.;
                double newPulseTime=0.;
                double newPulseAmp=0;
                double tDiff;
                for(int iBin=mWF->GetXaxis()->FindBin(mPulse[iPulse].mFitLowLimit)+1;
                    iBin<=tLastBin; iBin++){
                    tDiff=mPolarity*(mWF->GetBinContent(iBin)-
                                     mFExpGaus->Eval(mWF->GetBinCenter(iBin)));
                    if(maxDiff<tDiff){
                        maxDiff=tDiff;
                        newPulseTime =  mWF->GetBinCenter(iBin);
                        newPulseAmp = mWF->GetBinContent(iBin)-mBmu;
                    }
                }

                // >>> Abort if too close to an existing pulse
                tMinTimeDiff=1.;
                for(int iFitPulse=0; iFitPulse<mNPulse; iFitPulse++){
                    if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                        tDiff=fabs(mPulse[iFitPulse].mFitTime-newPulseTime);
                        if(tMinTimeDiff>tDiff) tMinTimeDiff=tDiff;
                    }
                }
                if(tMinTimeDiff>2e-9){
                    // >>> Add one more pulse
                    mFExpGaus->ReleaseParameter(4+2*tNFitPulse);
                    mFExpGaus->SetParameter(4+2*tNFitPulse,newPulseAmp);
                    mFExpGaus->SetParLimits(4+2*tNFitPulse,-10.,mNoise1Bin);
                    mFExpGaus->ReleaseParameter(5+2*tNFitPulse);
                    mFExpGaus->SetParameter(5+2*tNFitPulse,newPulseTime);
                    mFExpGaus->SetParLimits(5+2*tNFitPulse,mPulse[iPulse].mFitLowLimit,mPulse[iPulse].mFitHighLimit);
                    tNFitPulse++;
                    mFExpGaus->FixParameter(3,tNFitPulse);
                    mPulse[mNPulse].mFirstPulseInGroup=iPulse;
                    mNPulse++;
                    // debug mode -> find the source of the error:
                    //Error in ROOT::Math::ParameterSettings>: Invalid lower/upper bounds - ignoring the bounds
                    if(newPulseTime <   mPulse[iPulse].mFitLowLimit|| newPulseTime > mPulse[iPulse].mFitHighLimit)
                    {
                        std::cout << "new t: " << newPulseTime<< "   " << mPulse[iPulse].mFitLowLimit
                                  <<"  " << mPulse[iPulse].mFitHighLimit<< std::endl;
                    }
                    if(newPulseAmp > mNoise1Bin || newPulseAmp < (-10.0) )
                    {
                        std::cout << "new a: " << newPulseAmp<< "   " << mNoise1Bin <<std::endl;
                    }
                    // >>> Fit
                    mWF->Fit("FExpGaus",mFitOption);



                    // >>> Copy fit information
                    iFitParameter=0;
                    for(int iFitPulse=iPulse; iFitPulse<mNPulse; iFitPulse++){
                        if(mPulse[iFitPulse].mFirstPulseInGroup==iPulse){
                            mPulse[iFitPulse].mFitAmp = mFExpGaus->GetParameter(4+2*iFitParameter);
                            mPulse[iFitPulse].mFitTime = mFExpGaus->GetParameter(5+2*iFitParameter);
                            mPulse[iFitPulse].mFitBaseline = mFExpGaus->GetParameter(0);
                            mPulse[iFitPulse].mFitRiseTime = mFExpGaus->GetParameter(1);
                            mPulse[iFitPulse].mFitFallTime = mFExpGaus->GetParameter(2);
                            mPulse[iFitPulse].mRefitChi2 = mFExpGaus->GetChisquare();
                            mPulse[iFitPulse].mRefitNDF = mFExpGaus->GetNDF();
                            //std::cout << "Refit: " << mFExpGaus->GetChisquare()/mFExpGaus->GetNDF() << std::endl;
                            iFitParameter++;
                        }
                    }
                }
            }
        }
        iPulse++;
    }
    // previous sorting was here -> moved to end of fitting

}


double WaveformProcessor::getTriggerTime(){
    return mDataFile->getTriggerTime();
}

TF1* WaveformProcessor::getFitFunction() {
    // include all pulses and full range of the waveform
    mFExpGaus->SetLineColor(2);
    mFExpGaus->SetRange(mWF->GetXaxis()->GetXmin(),
                        mWF->GetXaxis()->GetXmax());
    int tNFitPulse;
    if(mNPulse>mNFitPulseMax){
        std::cout << "WARNING, number of pulses exceed max allowed for function " << mNPulse << " vs "
                  << mNFitPulseMax << " allowed" << std::endl;
        tNFitPulse = mNFitPulseMax;
    }
    else{
        tNFitPulse = mNPulse;
    }
    mFExpGaus->SetParameter(3,tNFitPulse);

    for(int iPulse=0; iPulse<tNFitPulse; iPulse++){
        mFExpGaus->SetParameter(4+2*iPulse,mPulse[iPulse].mFitAmp);
        mFExpGaus->SetParameter(5+2*iPulse,mPulse[iPulse].mFitTime);
        //        std::cout<< "getFitFunction: " << mPulseIndex[iPulse] << " "
        //                 << mPulse[mPulseIndex[iPulse]].mFitTime << " " << mPulse[mPulseIndex[iPulse]].mFitAmp << std::endl;
    }
    for(int iPulse=tNFitPulse; iPulse<mNFitPulseMax; iPulse++){
        mFExpGaus->FixParameter(4+2*iPulse,0);
        mFExpGaus->FixParameter(5+2*iPulse,0);
    }
    return mFExpGaus;
}

TH1F* WaveformProcessor::getPulseHisto(){
    return pulseHisto;
}
void WaveformProcessor::setQuietMode(bool mode){
    quietMode = mode;
}
double WaveformProcessor::checkNoise()
{
    //std::cout << mWF->getMax() << "\t" << mWF->getMeanBaseline() << std::endl;

    return mWF->getMax();

}



