#include "PulseFinder.h"
#include "DataFile.h"

#include "TROOT.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TGraph.h"
#include "TMath.h"

#include <iostream>
#include <cmath>
#include <cstdlib>

// __________________________________________________________________________
// __________________________________________________________________________
// __________________________________________________________________________
// --- Instantiate and initialize
PulseFinder* PulseFinder::mInstance = 0;
PulseFinder* PulseFinder::instance(int aNFitPulseMax){
  if(!mInstance) mInstance = new PulseFinder(aNFitPulseMax);
  return mInstance;
}
void fcn(Int_t &npar, Double_t *gin, 
    Double_t &f, Double_t *par, Int_t iflag){
  PulseFinder::instance()->minuitFunction(npar,gin,f,par,iflag);
}
PulseFinder::PulseFinder(int aNFitPulseMax):mNFitPulseMax(aNFitPulseMax),mPulses(aNFitPulseMax),mMaskMin(0),mMaskMax(0),mLowFitRange(-1),mRiseTime(3){  
  mMinuit = new TMinuit(aNFitPulseMax*2+2);  
  Double_t arglist[10];
  Int_t ierflg = 0;
  arglist[0] = 1;
  //gMinuit->mnexcm("SET ERR", arglist ,1,ierflg);
  mMinuit->SetFCN(fcn);
  mMinuit->SetPrintLevel(-2);
  mRespMax=1;
}
// __________________________________________________________________________
// __________________________________________________________________________
// __________________________________________________________________________ 
double myFunc(double* x, double* par){
  double tt=x[0]-par[0];
  if(tt<0.) return 0.;
  return par[1]/par[2]*tt*exp(-tt/par[2]+1.);//normalize to max
}

// >>> Initialize response function
void PulseFinder::calcResp(DataFile* dataFile, 
    int aFirstBin,
    int aLastBin,
    double aMinInt,
    double aMaxInt,
    double aPTimeMin,
    double aPTimeMax,
    int aNoTimeAdjustment,
    int aFirstEvent,
    int aLastEvent){
  // --- TF1 for fit
  TF1 fResp("fResp",myFunc,aFirstBin,aLastBin,3);

  // --- Output
  char tRespFileName[50];
  sprintf(tRespFileName,"AverageWF/Av%i.root",dataFile->getRun());
  sprintf(tRespFileName,"AverageWF/Av%i.root",1);
  TFile* tRespFile = new TFile(tRespFileName,"RECREATE");  
  TNtuple* tNtp = (TNtuple*) gROOT->FindObjectAny("ntp");
  if(tNtp) tNtp->Delete();
  tNtp = new TNtuple("ntp","","ievt:intB:int:imax:max:Ver:Bmu:Bsig:Bval:Ptime:PQ:Avtime:Ftau:FQ");

  // --- Input Waveform
  Waveform* tWF = dataFile->getWaveform(0);

  // --- Event loop
  int nAv=0;
  int iEvent=aFirstEvent;
  int lastEvent = dataFile->getWaveformCount()-1;
  if(aLastEvent!=0 && aLastEvent<lastEvent) lastEvent = aLastEvent;
  while(iEvent<=lastEvent && nAv<20000){
    if(iEvent%1000==0) std::cout << "Av calc " << iEvent << std::endl;
    tWF = dataFile->getWaveform(iEvent);
    tWF->subtractBaseline(0,0);
    double tChargeB=0.;
    for(int ti= aFirstBin-(aLastBin-aFirstBin)-1; ti<=aLastBin-(aLastBin-aFirstBin)-1; ti++){
      tChargeB += tWF->GetBinContent(ti);
    }

    double tCharge =0.;
    double tMax=-1e6;    
    double tAvTime=0.;
    int tiMax;
    for(int ti= aFirstBin; ti<=aLastBin; ti++){
      tCharge += tWF->GetBinContent(ti);
      tAvTime += (tWF->GetBinContent(ti)*ti);
      if(tMax<tWF->GetBinContent(ti)) {
        tMax = tWF->GetBinContent(ti);
        tiMax=ti;
      }
    }
    tAvTime/=tCharge;
    double tPeakCharge=0.;
    for(int ti=tiMax-2; ti<tiMax+3; ti++) {
      tPeakCharge += tWF->GetBinContent(ti);
    }
    tPeakCharge/=5.;
    double tTime =0.;
    double tNorm=0.;
    double tDiff;
    for(int ti=tiMax-6; ti<=tiMax; ti++){
      tDiff= tWF->GetBinContent(ti)- tWF->GetBinContent(ti-1);
      tNorm+=tDiff;
      tTime+=(tDiff*ti);
    }
    tTime/=tNorm;

    double tFTime=-1.;
    double tFQ=-1.;
    double tFTau=-1.;
    if(iEvent%1000==0) {
      tWF->Write();
      std::cout << "tChargeB: " << tChargeB << " : tCharge: " << tCharge
        << std::endl;
      std::cout << "aMinInt: " << aMinInt << " : aMaxInt: " << aMaxInt <<
        std::endl;
      std::cout << "tTime: " << tTime << " : aPTimeMin: " << aPTimeMin <<
        " : aPTimeMax: " << aPTimeMax << std::endl;
      std::cout << iEvent << " : " << tWF->validBaseline() << " : " <<
        (int)(tChargeB<aMinInt) << " : " << (int)(tCharge>aMinInt) << " : " <<
        (int)(tCharge<aMaxInt) << " : " << (int)(tTime>aPTimeMin) << " : " <<
        (int)(tTime<aPTimeMax) << std::endl;
    }
    if(tWF->validBaseline() && 
        tChargeB<aMinInt &&
        tCharge>aMinInt && 
        tCharge<aMaxInt && 
        tTime>aPTimeMin && 
        tTime<aPTimeMax){
      if(aNoTimeAdjustment){
        tWF->fillAverage(1.,0.);
        nAv++;
      }
      else{
        //double cfdTime = tWF->findCFDTime(tWF->GetXaxis()->FindBin(aPTimeMin), 
        //			  tWF->GetXaxis()->FindBin(aPTimeMax));
        //double ltc6400Time = tWF->findTimeLTC6400(aFirstBin,aLastBin,0);
        //if(ltc6400Time>0.){
        if(tWF->timeShiftLTC6400(aFirstBin,aLastBin,(aPTimeMax+aPTimeMin)/2.)){
          //tWF->timeShift(ltc6400Time-(aPTimeMax+aPTimeMin)/2.);
          tWF->fillAverage(1.,0.);//(aPTimeMax+aPTimeMin)/2.-tTime);
          nAv++;
        }
        //std::cout << (aPTimeMax+aPTimeMin)/2.-tiMax << std::endl;
      }

    }

    // TODO: figure out what the getVernier did
    //tNtp->Fill(iEvent,tChargeB,tCharge,tiMax,tMax,aRespDataReader->getVernier(),
    tNtp->Fill(iEvent,tChargeB,tCharge,tiMax,tMax,1,
        tWF->getMeanBaseline(), tWF->getSigmaBaseline(),
        tWF->validBaseline(),tTime,tPeakCharge,tAvTime,tFTau,tFQ);

    iEvent++;
  }
  if(nAv<10000) std::cerr << "WARNING!!! Only " << nAv <<  " events used for averaging " << std::endl;
  //tWF->getAverage()->Scale(1./nAv);
  /*
  tNtp->Write();
  tRespFile->Close();
  exit(0);
  */
  mWFResp = tWF->getAverage();
  mWFResp->Write();
  // TODO: mWFResp2D has too high resolution... this causes everything to crash.
  //TH2D* mWFResp2D = tWF->getAverage2D();
  //mWFResp2D->Write();
  tNtp->Write();
  tRespFile->Close(); 
}

//________________________________________________________________________
void PulseFinder::calcRespPass2(DataFile* dataFile,
    int aFirstBin,
    int aLastBin,
    double aMinInt,
    double aMaxInt,
    double aPTimeMin,
    double aPTimeMax){

  // --- Read response function
  readResp(dataFile->getRun(),1.);
  buildRespInterpolater(aFirstBin,aLastBin);

  // --- Output
  char tRespFileName[50];
  sprintf(tRespFileName,"AverageWF/Av%iPass2.root",dataFile->getRun());
  sprintf(tRespFileName,"AverageWF/Av%iPass2.root",1);
  TFile* tRespFile = new TFile(tRespFileName,"RECREATE");  
  TNtuple* tNtp = (TNtuple*) gROOT->FindObjectAny("ntp");
  if(tNtp) tNtp->Delete();
  tNtp = new TNtuple("ntp","","ievt:int:imax:max:Ver:Bmu:Bsig:Bval:Ptime:AvTime:NPulse:Ftime:FQ:FChi2:FNDof");


  // --- Input Waveform
  Waveform* tWF = dataFile->getWaveform(0);

  // --- Event loop
  int nAv=0;
  int iEvent=0;
  while(iEvent<dataFile->getWaveformCount() && nAv<20000){
    if(iEvent%1000==0) std::cout << "Av calc " << iEvent << std::endl;
    tWF = dataFile->getWaveform(iEvent);
    tWF->subtractBaseline(0,0);
    double tCharge =0.;
    double tMax=-1e6;    
    double tAvTime=0.;
    int tiMax;
    for(int ti= aFirstBin; ti<=aLastBin; ti++){
      tCharge += tWF->GetBinContent(ti);
      tAvTime += (tWF->GetBinContent(ti)*ti);
      if(tMax<tWF->GetBinContent(ti)) {
        tMax = tWF->GetBinContent(ti);
        tiMax=ti;
      }
    }
    tAvTime/=tCharge;
    double tTime =0.;
    double tNorm=0.;
    double tDiff;
    for(int ti=tiMax-6; ti<=tiMax; ti++){
      tDiff= tWF->GetBinContent(ti)- tWF->GetBinContent(ti-1);
      tNorm+=tDiff;
      tTime+=(tDiff*ti);
    }
    tTime/=tNorm;
    // --- 
    if(tWF->validBaseline() && 
        tCharge>aMinInt && 
        tCharge<aMaxInt && 
        tTime>aPTimeMin && 
        tTime<aPTimeMax){
      mPulses.clear();
      mPulses.add(new Pulse(tWF->GetBinContent(tiMax-mRiseTime),
            tiMax,
            tMax,
            tiMax-mRiseTime,
            tMax/mRespMax,
            tWF->Integral(tiMax,tiMax+30),
            0));
      mBaseline=0.;
      mNoise = tWF->getSigmaBaseline();
      int tNPulse = findPulseFit(tWF);
      // TODO: vernier
      //tNtp->Fill(iEvent,tCharge,tiMax,tMax,aRespDataReader->getVernier(),
      tNtp->Fill(iEvent,tCharge,tiMax,tMax,1,
          tWF->getMeanBaseline(), tWF->getSigmaBaseline(),
          tWF->validBaseline(),tTime,tAvTime,
          tNPulse,
          mPulses[0].tFit, mPulses[0].hFit,
          mPulses[0].chi2, mPulses[0].NDF);
      if(tNPulse!=1) {
        std::cerr << " WARNING More than 1 pulse found in resp func refit" 
          << std::endl;
      }
      else{
        //tWF->timeShift((aPTimeMax+aPTimeMin)/2.-mPulses[0].tFit-mRiseTime+2);
        //tWF->fillAverage(1.,0.);
        tWF->fillAverage(1.,(aPTimeMax+aPTimeMin)/2.-mPulses[0].tFit-mRiseTime);
        nAv++;
      }
    }
    iEvent++;
  }
  if(nAv<10000) std::cerr << "WARNING!!! Only " << nAv <<  " events used for averaging " << std::endl;
  tWF->getAverage()->Scale(1./nAv);
  mWFResp = tWF->getAverage();
  tRespFile->cd();
  mWFResp->Write();
  tNtp->Write();
}

//________________________________________________________________________
double PulseFinder::getPulseTime(Waveform* aWF, int aFirstBin, int aLastBin){
  aWF->subtractBaseline(0,0);
  double tCharge =0.;
  double tMax=-1e6;    
  double tAvTime=0.;
  int tiMax;
  for(int ti= aFirstBin; ti<=aLastBin; ti++){
    tCharge += aWF->GetBinContent(ti);
    tAvTime += (aWF->GetBinContent(ti)*ti);
    if(tMax<aWF->GetBinContent(ti)) {
      tMax = aWF->GetBinContent(ti);
      tiMax=ti;
    }
  }
  tAvTime/=tCharge;
  double tTime =0.;
  double tNorm=0.;
  double tDiff;
  for(int ti=tiMax-6; ti<=tiMax; ti++){
    tDiff= aWF->GetBinContent(ti)- aWF->GetBinContent(ti-1);
    tNorm+=tDiff;
    tTime+=(tDiff*ti);
  }
  tTime/=tNorm;
  TF1* tGauss = (TF1*) gROOT->FindObjectAny("Gauss");
  if(!tGauss) tGauss = new TF1("Gauss","gaus",tiMax-mRiseTime*3,tiMax+1);
  tGauss->SetRange(tiMax-mRiseTime*3,tiMax+1);
  aWF->Fit("Gauss","R0");  
  return tGauss->GetParameter(1);
}

//________________________________________________________________________
void PulseFinder::readResp(int aAvRun, double aScale, int aPass){
  char tRespFileName[50];
  if(aPass==1){
    sprintf(tRespFileName,"AverageWF/Av%i.root",aAvRun);
  }
  if(aPass==2){
    sprintf(tRespFileName,"AverageWF/Av%iPass2.root",aAvRun);
  }
  TFile* tRespFile =
    (TFile*)gROOT->GetListOfFiles()->FindObject(tRespFileName);
  if (!tRespFile) { 
    tRespFile = new TFile(tRespFileName);  
    // necessary so that scale only applied once
    mWFResp = (TH1D*) tRespFile->Get("WF0Av");
    if(!mWFResp) mWFResp = (TH1D*) tRespFile->Get("WF1Av");
    if(!mWFResp) mWFResp = (TH1D*) tRespFile->Get("WFDiffAv");
    if(!mWFResp) mWFResp = (TH1D*) tRespFile->Get("HWF0Av");
    mWFResp->Scale(aScale);
  }
  else{
    mWFResp=0;
    mWFResp = (Waveform*) tRespFile->Get("WF0Av");
    if(!mWFResp) mWFResp = (TH1D*) tRespFile->Get("WF1Av");
    if(!mWFResp) mWFResp = (TH1D*) tRespFile->Get("WFDiffAv");
    if(!mWFResp) mWFResp = (TH1D*) tRespFile->Get("HWF0Av");
  }


}

//________________________________________________________________________
// >>> calculate interpoltion vector
void PulseFinder::buildRespInterpolater(int aFirstBin,
    int aLastBin){
  // calculate rise time and start of the pulse
  double tMax=-1e12;
  int tiMax=0;
  for(int ti=aFirstBin; ti<aLastBin; ti++){
    if(tMax<mWFResp->GetBinContent(ti)){
      tiMax=ti;
      tMax=mWFResp->GetBinContent(ti);
    }      
  }
  int tPulseStart=tiMax;
  while(mWFResp->GetBinContent(tPulseStart)>tMax/100. && tPulseStart>0) tPulseStart--;
  if(tPulseStart==0){
    std::cout << "ERROR!!! Something wrong with the response function baseline" << std::endl;
    exit(0);
  }
  mRiseTime = tiMax-tPulseStart;
  double tBaseline=0.;
  for(int ti=aFirstBin; ti<tPulseStart; ti++) tBaseline+=mWFResp->GetBinContent(ti);
  tBaseline/=(tPulseStart-aFirstBin);

  // build interpolator
  mNNegBinItrp=2048;
  for(int ti=0; ti<4096; ti++) mYItrp[ti]=0.;
  int tAvNBin = aLastBin-tPulseStart;
  mRespMax=0.;
  for(int ti=0; ti<tAvNBin; ti++){
    mYItrp[ti+mNNegBinItrp] = mWFResp->GetBinContent(ti+tPulseStart)-tBaseline;
    if(mRespMax<mYItrp[ti+mNNegBinItrp]) mRespMax=mYItrp[ti+mNNegBinItrp];
  }

  // calculate 1PE scaling variable
  Double_t arglist[10];
  Int_t ierflg = 0;
  arglist[0] = 2000;
  arglist[1] = 0.1; 
  mNFitPulse=1;
  mFitLowBin = aFirstBin;
  mFitHighBin = aLastBin;
  mFitWaveform = mWFResp;
  mNoise = 0.01;
  mMinuit->mncler();
  mMinuit->mnparm(0,"Base",0.,0.01,0,0,ierflg);
  mMinuit->mnparm(1,"Amp0",1.,0.05,0,0,ierflg);
  mMinuit->mnparm(2,"t0",tPulseStart, 0.1,mFitLowBin,mFitHighBin,ierflg);
  mMinuit->FixParameter(0);
  mMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);
  double tErr;
  double tScale;
  mMinuit->GetParameter(1,tScale,tErr);
  mRespMax/=tScale;
  double tPos;
  mMinuit->GetParameter(2,tPos,tErr);
  std::cout << "Response function interpolator built." << std::endl
    << "Rise time " << mRiseTime << std::endl
    << "1PE amplitude = " << mRespMax << " scaled by " << tScale  << std::endl
    << "Peak pos " <<  tPos << " vs " << tPulseStart << std::endl
    << "baseline = " <<  tBaseline << std::endl;
}

// __________________________________________________________________________
// >>> Interpolate response
double PulseFinder::getResp(double aX){
  int ti = (int) floor(aX); // 1 ns binning is implicit
  double tDelta = aX-ti;
  ti+=mNNegBinItrp; // 0 of interp function is at gNNegBinItrp
  return tDelta*mYItrp[ti+1]+(1.-tDelta)*mYItrp[ti];
}


// __________________________________________________________________________
// __________________________________________________________________________
// __________________________________________________________________________
//__________________________________________________________________________
// >>> Calculate and fit waveforms 
double PulseFinder::calcWF(double* x, double* par){
  double tVal=par[0]; // baseline
  for(int tj=0; tj< mNFitPulse; tj++){
    tVal+=fabs(par[2*tj+1])*getResp(x[0]-par[2*tj+2]);
  }
  return tVal;
}
// >>> Chi2 function for Minuit
void PulseFinder::minuitFunction(Int_t &npar, Double_t *gin, 
    Double_t &f, Double_t *par, Int_t iflag){
  f=0.;
  double tVal;
  double tX[1];

  for(int ti=mFitLowBin; ti<mFitHighBin; ti++){
    tX[0] =  mFitWaveform->GetBinLowEdge(ti);
    tVal = mFitWaveform->GetBinContent(ti)-calcWF(tX,par);
    f+=tVal*tVal;
  }
  //std::cout << par[0] << " " << par[1] << " " << par[2] << " " << f << std::endl;
  f/=mNoise;
  f/=mNoise;  
}
int PulseFinder::getMaxChi2Bin(double* par){
  double tVal;
  double tVal2Max=-1.;
  double tX[1];
  int iMaxBin;
  for(int ti=mFitLowBin; ti<mFitHighBin; ti++){
    tX[0] =  mFitWaveform->GetBinLowEdge(ti);
    tVal = mFitWaveform->GetBinContent(ti)-calcWF(tX,par);
    if(tVal2Max<tVal*tVal) {
      tVal2Max = tVal*tVal;
      iMaxBin=ti;
    }
  }
  return iMaxBin;
}



//__________________________________________________________________________
// >>> Build histogram from fit function
TH1D* PulseFinder::histFitFunction(char* aName){
  TH1D* HFF = (TH1D*) gROOT->FindObjectAny(aName);
  if(HFF) HFF->Delete();
  HFF = new TH1D(*((TH1D*) mFitWaveform));
  HFF->SetName(aName);
  HFF->SetLineColor(2);
  double tPar[500];
  tPar[0]=0.;//mBaseline;
  std::cout << "Build fit function with parameters below " << std::endl;
  Pulse* tPulse;
  for(int ti=0; ti<mPulses.size(); ti++){
    tPar[2*ti+1]=mPulses[ti].hFit;
    tPar[2*ti+2]=mPulses[ti].tFit;
    std::cout << ti << " " << tPar[2*ti+1] << " " << tPar[2*ti+2] << " " << mPulses[ti].CL << " " << mPulses[ti].fit << " " << mPulses[ti].chi2 << " " << mPulses[ti].NDF << " " << mPulses[ti].baseline << std::endl;
  }
  double tX;
  mNFitPulse = mPulses.size();
  for(int ti=1; ti<=HFF->GetNbinsX();ti++){
    tX = HFF->GetBinCenter(ti);
    HFF->SetBinContent(ti,calcWF(&tX,tPar));
  }
  for(int ti=0; ti<mPulses.size(); ti++){
    int tjFirst = (int) mPulses[ti].tFit-mLowFitRange*5;
    if(tjFirst<=0) tjFirst=1;
    int tj = tjFirst;
    while(tj < (int) mPulses[ti].tFit+mHighFitRange*5 &&
        ((ti<mPulses.size()-1 && tj<(int) mPulses[ti+1].tFit-mLowFitRange*5) 
         || ti==mPulses.size()-1)) tj++;
    int tjLast=tj;
    if(tjLast>HFF->GetNbinsX()) tjLast=HFF->GetNbinsX();
    for(int tj=tjFirst; tj<tjLast; tj++){
      HFF->SetBinContent(tj,HFF->GetBinContent(tj)+mPulses[ti].baseline);
    }
  }
  return HFF;
}


TGraph*  PulseFinder::getPulseGraph(){
  if(mPulses.size()>0){
    //cout << mPulses.size() << endl;
    double* tX = new double[mPulses.size()];
    double* tY = new double[mPulses.size()];
    for(int ti=0; ti<mPulses.size(); ti++){
      tX[ti] = mPulses[ti].tPF;
      tY[ti] = mPulses[ti].hPF;
      //cout << tX[ti] << " " << tY[ti] << endl;
    }
    TGraph* tG = (TGraph*) gROOT->FindObjectAny("GPulse");
    if(tG) tG->Delete();
    tG = new TGraph(mPulses.size(),tX,tY);
    tG->SetName("GPulse");
    tG->SetMarkerStyle(23);
    tG->SetMarkerColor(2);
    delete[] tX;
    delete[] tY;
    return tG;
  }
  else{
    return 0;
  }
}

// __________________________________________________________________________
// __________________________________________________________________________
// __________________________________________________________________________
// --- Pulse finder
int PulseFinder::setParameters(int aLowFitRange,
    int aHighFitRange,
    double aNoiseCut,
    double aCLMin){
  mLowFitRange = aLowFitRange;
  mHighFitRange = aHighFitRange;
  mNoiseCut = aNoiseCut;
  mCLMin = aCLMin;
}
//____________________________________________________________________________
int PulseFinder::findPulseSimple(Waveform* aWF){
  mPulses.clear();
  mWF = aWF;
  // __________________________________________________________
  // Step 0:
  aWF->subtractBaseline(0,0);
  if(!aWF->validBaseline()) {
    std::cerr << "WARNING!!! Skip event because of bad baseline " << aWF->getMeanBaseline() << " " << aWF->getSigmaBaseline() << std::endl;  
    return 0;
  }
  mBaseline=0.;
  mNoise = aWF->getSigmaBaseline();
  //cout <<  "Noise " << mNoise << endl;

  // __________________________________________________________
  // Step 1: Find the easy pulses
  int tNBin = aWF->GetNbinsX();

  for(int ti=mLowFitRange+mHighFitRange+1; 
      ti<tNBin-mHighFitRange+1; 
      ti++){

    if( aWF->GetBinContent(ti)>mNoiseCut*mNoise && 
        aWF->GetBinContent(ti)-aWF->GetBinContent(ti-mRiseTime)>mNoiseCut*mNoise){
      double tPeakMax=aWF->GetBinContent(ti);
      int tiPeak = ti;
      int tj=ti;
      while(tj<=(tNBin-mLowFitRange) && 
          aWF->GetBinContent(tj)>mNoiseCut*mNoise && 
          aWF->GetBinContent(tj)-aWF->GetBinContent(tj-mRiseTime)>mNoiseCut*mNoise){
        if(tPeakMax<aWF->GetBinContent(tj)){
          tPeakMax=aWF->GetBinContent(tj);
          tiPeak=tj;
        }
        tj++;
      }
      //cout << iPeak << " " << tj-ti << " " << mNoiseCut*mNoise << endl;
      if(tj-ti>2 && tiPeak<mMaskMin || tiPeak>mMaskMax){
        mPulses.add(new Pulse(aWF->GetBinContent(ti-mRiseTime-2),
              tiPeak,
              tPeakMax,//aWF->GetBinContent(iPeak),
              tiPeak-mRiseTime,
              aWF->GetBinContent(tiPeak)/mRespMax,
              aWF->Integral(tiPeak-mRiseTime,tiPeak+30),
              0));
      }
      //cout << iPeak-mRiseTime << " ";
      //}
      ti=tj;
  }    
}
//cout << " | N pulse = " << mPulses.size() << endl;

return mPulses.size();
}

int PulseFinder::findPulse(Waveform* aWF){
  findPulseSimple(aWF);  
  findPulseFit(aWF);
}

int PulseFinder::findPulseFit(Waveform* aWF){
  mWF=aWF;
  mFitWaveform = (TH1D*) mWF;
  int tNBin = aWF->GetNbinsX();
  // __________________________________________________________
  // Step 2: Fit using pulse found in first search
  double tPar[500];
  Double_t arglist[10];
  Int_t ierflg = 0;
  arglist[0] = 2000;
  arglist[1] = 0.1; 
  Pulse* tPulse;

  mFitLowBin=1;
  mFitHighBin=aWF->GetNbinsX();

  int iPulse=0;
  while(iPulse<mPulses.size()){
    if(mPulses[iPulse].fit==0 ||
        (mPulses[iPulse].fit>0 && mPulses[iPulse].fit<5 && 
         mPulses[iPulse].chi2/mPulses[iPulse].NDF>mCLMin )){       
      if(mPulses[iPulse].fit>0 &&
          mPulses[iPulse].tFit>mFitLowBin && 
          mPulses[iPulse].tFit<mFitHighBin){	
        int tiPos = getMaxChi2Bin(tPar);
        if(tiPos<tNBin){
          mPulses.add(new Pulse(mPulses[iPulse].baseline,
                -666,-666.,tiPos,
                aWF->GetBinContent(tiPos)/mRespMax,
                aWF->Integral(tiPos,tiPos+30),
                10),1);
        }
      }

      // >>> Calculate fit boundaries
      mFitLowBin = (int) mPulses[iPulse].tFit-mLowFitRange;
      int tFirstFitPulse=iPulse;
      while(tFirstFitPulse>0 &&
          mPulses[tFirstFitPulse-1].tFit-mFitLowBin>0.){
        tFirstFitPulse--;
        mFitLowBin=(int) (mPulses[tFirstFitPulse].tFit)-
          mLowFitRange;

      }
      if(mFitLowBin<1) mFitLowBin=1;

      mFitHighBin = (int) mPulses[iPulse].tFit+mHighFitRange;
      int tLastFitPulse=iPulse;
      while(tLastFitPulse<mPulses.size()-1 && 
          mFitHighBin-mPulses[tLastFitPulse+1].tFit>0){
        tLastFitPulse++;
        mFitHighBin= ((int) (mPulses[tLastFitPulse].tFit))+
          mHighFitRange;

      }
      if(mFitHighBin>tNBin) mFitHighBin=tNBin;

      // >>> Set Minuit parameters
      mMinuit->mncler();
      //mMinuit->mnparm(0,"Base",mPulses[iPulse].baseline,0.01,0,0,ierflg);
      mMinuit->mnparm(0,"Base",0.,0.01,0,0,ierflg);
      mMinuit->FixParameter(0);
      mNFitPulse=0;
      int tj=tFirstFitPulse;
      while(tj<=tLastFitPulse && mNFitPulse<mNFitPulseMax){
        char tName1[50];
        sprintf(tName1,"Amp%i",tj);      
        mMinuit->mnparm(2*mNFitPulse+1, tName1,mPulses[tj].hFit,
            0.05,0,0,ierflg);
        //0.05,0.,500.,ierflg);
        char tName2[50];
        sprintf(tName2,"time%i",tj);      
        mMinuit->mnparm(2*mNFitPulse+2, tName2,mPulses[tj].tFit,
            0.1,mFitLowBin,mFitHighBin,ierflg);
        //0.2,tPulse->tFit-3.,tPulse->tFit+3.,ierflg);
        mNFitPulse++;
        tj++;
      }
      if(mNFitPulse==mNFitPulseMax) {
        std::cerr << "-------------------------------------- " 
          << "too many pulses to fit " 
          << " -------------------------------------" << std::endl;
        for(int tj=tFirstFitPulse; tj<=tLastFitPulse; tj++){ 
          mPulses[tj].chi2 = -1.;
          mPulses[tj].NDF=1;
        }
        iPulse=tLastFitPulse+1;
        //exit(0);
      }
      else{                     
        // >>> Minimize
        mMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

        // >>> Evaluate chi2 after the fit
        double tParErr;
        for(int tj=0; tj<mNFitPulse*2+1; tj++){
          mMinuit->GetParameter(tj,tPar[tj],tParErr);           
        }
        int tNPar;
        double tChi2;
        minuitFunction(tNPar,0, tChi2, tPar, 0);
        int tk=0;
        //cout << "Fit " << tFirstFitPulse << " " << tLastFitPulse << " " 
        //<< mFitLowBin << " " << mFitHighBin << " " << tChi2 
        //<< " " << mFitHighBin-mFitLowBin-2*mNFitPulse-1 << endl;
        for(int tj=tFirstFitPulse; tj<=tLastFitPulse; tj++){  	
          if(tk<mNFitPulse){
            mPulses[tj].baseline = tPar[0];
            mPulses[tj].tFit = tPar[2*tk+2];
            mPulses[tj].hFit = fabs(tPar[2*tk+1]);
            mPulses[tj].chi2 = tChi2;
            mPulses[tj].NDF = mFitHighBin-mFitLowBin-2*mNFitPulse-1;
            mPulses[tj].CL = TMath::Prob(tChi2,mPulses[tj].NDF);
            //if(!(mPulses[tj].fit==0 && tj!=iPulse)) mPulses[tj].fit++;
            mPulses[tj].fit++;
            tk++;
          }
        }      
        mPulses.sort();
        iPulse=0;
      }
    }
    else{
      iPulse++;
    }
  }
  return mPulses.size();
}

