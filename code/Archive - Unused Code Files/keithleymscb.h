
#ifndef KEITHLEYMSCB_HPP
#define KEITHLEYMSCB_HPP
#include <iostream>
#include <string>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <istream>
#include <vector>

class KeithleyMSCB
{
public:
    KeithleyMSCB(const std::string mscbDevice,
                 const int node,
                 const std:: string mscbPath,
                 int writeAdd, int readAdd);

    // communication functions:
    float set_voltage(float v);
    float read_voltage();
    float read_current();


private:
    std::string send_command(std::string command);

    std::string _device,_mscbPath;
    int _node;
    int _readAdd, _writeAdd;
    std::string _execute;
};


class TemperatureMSCB
{
public:
    TemperatureMSCB(const std::string mscbDevice,
                    const int node,
                    const std:: string mscbPath,
                    int Add
                    ){}

    double read_temp();
//private:
//    std::string _execute;
};

class RemotePower
{
public:
    // add usefull definition here maybe
    RemotePower(){}

    void turn_off(int ch);
    void turn_on(int ch);
    void turn_offAll();
    void turn_onAll();
};

#endif // KEITHLEYMSCB_HPP
