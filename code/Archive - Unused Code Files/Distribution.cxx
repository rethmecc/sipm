/*******************************************************************************
* Distribution.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>

#include "DistributionFinder.h"
#include "SetupParameter.h"
#include "DataFile.h"
#include "WfmFile.h"
#include "Waveform.h"


int main(int argc, char** argv){
  if (argc < 2) {
    printf("Usage: %s RUN [MAX EVENT]\n", argv[0]);
    return 1;
  }

  const char* runInfoFilename = "RunInfo.txt";
  int run = atoi(argv[1]);
  int maxEvent = argc>2 ? atoi(argv[2]) : -1;

  SetupParameter* setupPar =  SetupParameter::instance(runInfoFilename, run);
  
  // Setup distribution finder
  DistributionFinder* distributionFinder = new
    DistributionFinder(setupPar->PTimeMin, setupPar->Max1peMin,
        setupPar->Max1peMax, setupPar->PulseSlopeThreshold,
        setupPar->TriggerLevel);

  //__________________________________________________________________________
  // --- Open data files for trigger and signal

  DataFile* signalDataFile = new WfmFile(setupPar->getFilename(), setupPar->Run,
      setupPar->FileCount, "HWF%i", 1e9, 1);

  if (!signalDataFile->isGood()) return 1;

		//DataFile* triggerDataFile = new WfmFile(setupPar->getFilename(true),
     //setupPar->Run, setupPar->FileCount, "HWFTrigger%i", 1e9, 1);
  //if (!signalDataFile->isGood() || !triggerDataFile->isGood()) {
		// return 2;

  Waveform* WF = signalDataFile->getWaveform(0);
  WF->setBaselineLimits(setupPar->BaselineMin,
			setupPar->BaselineMax,
			setupPar->SigMin,
			setupPar->SigMax);

  int NEvent = signalDataFile->getWaveformCount();
  if (NEvent > maxEvent && maxEvent != -1) {
    NEvent=maxEvent;
  }

  //__________________________________________________________________________
  // --- Open output file
  mkdir("./distribution", 0775);
  char directory[200];
  sprintf(directory, "./distribution/%s-%dnm-%dC", setupPar->Device, setupPar->Wlen, setupPar->Temp);
  mkdir(directory, 0775);
  char OutFileName[200];
  sprintf(OutFileName, "%s/%dV.root", directory, setupPar->Voltage);
  TFile OutFile(OutFileName, "RECREATE");


  //__________________________________________________________________________
  // ---
  for(int ti=0; ti<NEvent; ti++){
    if (ti%1000==0) {
      std::cout << "Processing event #: " << ti << std::endl;
    }
    distributionFinder->processEvent(signalDataFile->getWaveform(ti))
			 //triggerDataFile->getWaveform(ti));
  }
  
  // Write the results of the distribution finder to the file
  distributionFinder->write();

  // Append the FWHM results to a file
  //double fwhm, fwhmError;
  //distributionFinder->getFwhmResults(&fwhm, &fwhmError);
  //std::ofstream fwhmFile("./fwhm-results.txt", std::ios::app);
  //fwhmFile << setupPar->Run << "\t" << setupPar->Device << "\t" <<
	//setupPar->Temp << "\t" << setupPar->Wlen << "\t" << setupPar->Voltage / 10.
	//<< "\t" << fwhm << "\t" << fwhmError << std::endl;

  // Clean up
  delete signalDataFile;
  delete distributionFinder;
  OutFile.Close();
  std::cout << "Results written to " << OutFileName << std::endl;
}
