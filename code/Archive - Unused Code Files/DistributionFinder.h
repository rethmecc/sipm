/*******************************************************************************
* DistributionFinder.h
*
* Description:
* Finds pulses in a set of input waveforms and produces a distribution based on
* them
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TMath.h"

class Waveform;

class DistributionFinder {
  public:
    // Public functions

    DistributionFinder(double pulseStart, double max1peMin, double max1peMax,
        double pulseSlopeThreshold, double triggerLevel);
    ~DistributionFinder();

    // Process the next event. This function should be called for each event
    // individually.
    void processEvent(Waveform* signalWaveform, Waveform* triggerWaveform);

    // Write all of the results to a file
    void write();

    // Retrieve FWHM results
    void getFwhmResults(double* fwhm, double* fwhmError);

  private:
    // Setup/reset all of the classes used before processing an event.
    void setupEvent(Waveform* signalWaveform, Waveform* triggerWaveform);

    // Determine the trigger offset from the trigger waveform and load mWaveform
    // with the results of it applied to the original waveform.
    double processTrigger(Waveform* signalWaveform, Waveform* triggerWaveform);

    // Function representing a convolution of an exponential and a gaussian for
    // use with root's TF1 class.
    static double exponentialGaussianConvolution(double* x, double* p);

    // Perform linear interpolation to find the crossing point of a threshold.
    double interpolateThresholdCrossing(double x1, double y1, double x2, double
        y2, double threshold);

    // Find the time at which a waveform crosses a given threshold after a given
    // start time.
    double findThresholdTime(Waveform* waveform, double start, double
        threshold);

    // Find the time at which a waveform crosses a fraction of the maximum
    // value.
    double findCfdTime(Waveform* waveform, double start);

    // Calculate an integral (ROOT's version of this is very slow). This version
    // requires that all bins are the same size. Bin indexes are 1 based like in
    // ROOT.
    double calculateIntegral(Waveform* waveform, int binMin, int binMax);
    
    // Calculate the mean and sigma of the baseline up to end then subtract the
    // baseline from the waveform
    double subtractBaseline(Waveform* waveform, double end, double* mean,
        double* sigma);

    // Count the number of pulses in a waveform. Returns the maximum slope and
    // the number of pulses in the maxSlope and pulseCount variables.
    void countPulses(Waveform* waveform, double pulseSlopeThreshold, int*
        pulseCount, double* maxSlope);

    // Generate a plot of the distribution results
    void generateDistributionPlot();


  private:
    // Member variables
    double mPulseStart;
    int mEventCount;
    double mMax1peMin;
    double mMax1peMax;
    double mPulseSlopeThreshold;
    double mTriggerLevel;

    // Result objects
    TH1F* mRiseTimeWaveform;
    TH1F* mPeakSlopeWaveform;
    TH1F* mPeakSlopeSingleWaveform;
    TH1F* mTime;
    TNtuple* mNtupleAll;
    TNtuple* mNtuple1pe;
    double mFwhm;
    double mFwhmError;
    
    // Root objects used for event processing
    Waveform* mWaveform;
    Waveform* mDerivativeWaveform;

  private:
    // Settings for the distribution finder... these are values that seem to
    // work well for all waveforms so they aren't in the SetupParameter file
    // yet. That might need to be changed for new devices.
    static const int kDerivativeRange = 10;
    static const double kPulseThreshold;
    static const double kCfdRatio;
};
