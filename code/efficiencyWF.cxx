//Carl Rethmeier; May-June 2015; rethmeier.carl@gmail.com

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <math.h>
#include <time.h>
#include "LecroyFile.h"
#include "WaveformProcessor.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"

int getZeros(TNtuple* ntp, double Hlow);

int main(int argc,char* argv[]){
  int run=0;
  int numEvents=-1;
  int channel=1;
  int verbosity=0;
  double level=3.5;
  int window=3000;
  int darkStart=900;
  int lightStart=4400;
  double numWindows=1;
  bool trunc=false;
  bool quick=false;
  bool simple=false;
  char saveNameDef[10]="PEWF.txt";
  char* saveName=saveNameDef;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "r:n:c:thv:l:qw:sb:a:f:u:")) != -1)
   switch (c)
     {
     case 'r':  //Run#
       run = atoi(optarg);
       break;
     case 'n':  //Number of events
       numEvents = atoi(optarg);
       break;
     case 'c':  //Channel#
       channel = atoi(optarg);
       break;
     case 'f':  //Savefile name
       saveName = optarg;
       break;
     case 'l':  //Threshold level in # of sigma
       level = atof(optarg);
       break;
     case 'w':  //Window length
       window = atoi(optarg);
       break;
     case 't':  //Erase savefile contents
       trunc=true;
       break;
     case 'v':  //amount of text displayed
       verbosity = atoi(optarg);
       break;
     case 'q':  //Quick analysis
       quick = true;
       break;
     case 's':  //Simple peak finder using threshold and smoothing
       simple = true;
       break;
     case 'b':  //Start of dark window
       darkStart = atoi(optarg);
       break;
     case 'a':  //Start of light window
       lightStart = atoi(optarg);
       break;
     case 'u':  //Number of windows
       numWindows = atoi(optarg);
       break;
     case 'h':  //Print usage
       std::cout << "Usage: " << argv[0] << " [parameters]... [options]...\n"
                 << "This application uses the number of \"zeros\" in a waveform data file to calculate the relative efficiency of a SiPM.\n" 
                 << "It assumes that the \"dark\" region is on the left, and the \"light\" region is on the right.\n"
                 << "\nParameters and options:\n"
                 << "-r run#          Run number in \'LampRunInfo.txt\'; default=0\n"
                 << "-n numEvents     Number of waveforms to be processed, starting from 0. -1 for all events; default=-1\n"
                 << "-c channel#      Oscilloscope channel #; default=1\n"
                 << "-f fileName      Name of ASCII file in which resukts will be saved; default=\"PEWF.txt\"\n"
                 << "-l level         Minimum peak height in baseline RMS; default=3.5\n"
                 << "-w windowLength  Length of window to check for zeros in bins; default=3000\n"
                 << "-u numChunks     Number of windows in each region; default=1\n"
                 << "-b beforeLight   Start of region before lamp activation in bins; default=900\n"
                 << "-a afterLight    Start of region after lamp activation in bins; default=4400\n"
                 << "-v verbosity     Verbosity level (0,1,2,3,4,5); default=0\n"
                 << "-q               Perform quick analysis (no waveform smoothing)\n"
                 << "-s               Find peaks using threshold and smoothing only (use only for low noise waveforms)\n"
                 << "-t               Flag to truncate savefile contents (currently \'PEWF.txt\')\n"
                 << "-h               Display this usage info\n";
       exit(0);
       break;
     case '?':
       if (optopt == 'r' || optopt == 'u' || optopt == 'n' || optopt == 'c' || optopt == 'l' || optopt == 'w' || optopt == 'v')
         fprintf (stderr, "Option -%c requires an argument.\n", optopt);
       else if (isprint (optopt))
         fprintf (stderr, "Unknown option `-%c'.\n", optopt);
       else
         fprintf (stderr,
                  "Unknown option character `\\x%x'.\n",
                  optopt);
     return 1;
     default:
     abort ();
     }
  
  float dark=0;
  float light=0;
  int nBins;
  double secPerBin;
  double PE;
  double Hlow;
  int percent=0;
  
  char name[20];  
  if (channel==1)
    sprintf(name,"proc/minHist%i.root",run);
  else
    sprintf(name,"proc/minHist%i.%i.root",run,channel);
  
  TFile* saveHist = new TFile(name,"RECREATE");
  TNtuple* ntp = new TNtuple("ntp","ntp","W#:time:height:charge:width:trigTime");
  TNtuple* minHistDark = new TNtuple("minHistDark","minHistDark","W#:minHeight");
  TNtuple* minHistLight = new TNtuple("minHistLight","minHistLight","W#:minHeight");
  
  WaveformProcessor* wfProc = new WaveformProcessor("LampRunInfo.txt",run,channel,-1);
  wfProc->readWaveform(0);
  nBins=wfProc->getCurrentWaveform()->GetNbinsX();
  secPerBin=wfProc->getCurrentWaveform()->GetBinWidth(0);
  if (numEvents>wfProc->getWaveformCount() || numEvents<0) numEvents=wfProc->getWaveformCount();
  
  for (int i=0;i<numEvents;i++){
    if (verbosity==0 && (i+1)*100./numEvents>=(percent+1)){
      percent=(i+1)*100./numEvents;
      std::cout << std::string(4,'\b');
      std::cout << percent << "%" << std::flush;
    }
    
    if (verbosity>0){
      std::cout << std::string(25,'\b');
      std::cout << "Processing WF " << i;
    }
    wfProc->readWaveform(i);
    if (!quick || simple)
      wfProc->smoothWaveform();
    for (double j=0;j<numWindows;j++){
      minHistDark->Fill(i,wfProc->findMin(darkStart+window/numWindows*j,darkStart+window/numWindows*(j+1),verbosity));
      minHistLight->Fill(i,wfProc->findMin(lightStart+window/numWindows*j,lightStart+window/numWindows*(j+1),verbosity));
    }
    if (wfProc->findMin(lightStart,lightStart+window,0)<Hlow)
      Hlow=wfProc->findMin(lightStart,lightStart+window,0);
    if (wfProc->findMin(darkStart,darkStart+window,0)<Hlow)
      Hlow=wfProc->findMin(darkStart,darkStart+window,0);
    wfProc->processBaseline();
    wfProc->findClearPulses(ntp,darkStart,lightStart+window,level,verbosity-1);
//     if (verbosity>0)
//       std::cout << "Check Dark " << i << std::endl;
//     if (wfProc->checkZero(darkStart,darkStart+window,level,simple,verbosity-1))
//       dark++;
//     if (verbosity>0)
//       std::cout << "Check light " << i << std::endl;
//     if (wfProc->checkZero(lightStart,lightStart+window,level,simple,verbosity-1))
//       light++;
  }
  std::cout << std::endl;
  std::ofstream fout;
  if (trunc){
    fout.open(saveName,std::ios::trunc);
    fout << "run <PE> dark light\n";
  }
  else
    fout.open(saveName,std::ios::app);
  
//   PE=log(dark/light) / (window*secPerBin) *0.000001;
//   
//   std::cout << "light=" << light << "   dark=" << dark << std::endl;
//   std::cout << "<PE> = " << PE << " (us^-1)\n";
//   
//   fout << run << " " << PE << " " << dark << " " << light << std::endl;

  delete wfProc;
  
  saveHist->cd();
  
  ntp->Write();
  dark=getZeros(minHistDark,Hlow);
  light=getZeros(minHistLight,Hlow);
  
  PE=log(dark/light) / (window/numWindows*secPerBin) *0.000001;
  
  std::cout << "light=" << light << "   dark=" << dark << std::endl;
  std::cout << "<PE> = " << PE << " (us^-1)\n";
  
  fout << run << " " << PE << " " << dark << " " << light << std::endl;
  
  minHistDark->Write();
  minHistLight->Write();
  saveHist->Close();
  
  return 0;
}

int getZeros(TNtuple* ntp,double Hlow){
//---------------------------------------------------------------------  
  static int run=0; //run #
  
  int nBins=100;  //bins in hist
  double Hhigh=0;  //rightmost value
  int minLoc; //location of minimum after zeroeth peak
  int maxLoc; //location of maximum (assumed to be largest bin in zeroeth peak)
  double minVal=1000;  //value at minLoc
  double last;  //used to store value of previous bin when finding minLoc
  int consec; //counter used in finding minimum; ensures that noise does not result in false minimum
  bool minFound=false;
  double sum = 0; //integral of zeroeth peak
  
  char name[10];
  sprintf(name,"hist%i",run);
  
  TH1D* hist = new TH1D(name,name,nBins,Hlow,Hhigh);

  ntp->Project(name,"minHeight");

  hist->GetBinContent(1); //Deals with a weird (and hopefully temporary) bug in ROOT 6-02-08; works fine in 5.34-08
  maxLoc=hist->GetMaximumBin();
  last = hist->GetBinContent(maxLoc);
  consec=0;
  for (int i=maxLoc-1; i>0; i--){
    if (hist->GetBinContent(i)>last){ //find size of upward slope
      consec++;
    }
    /*else if (consec>0 && hist->GetBinContent(i)<0.01*hist->GetBinContent(maxLoc))
      consec--;
    */else
      consec=0;
    if (hist->GetBinContent(i)<minVal){ //record minimum value
      minVal=hist->GetBinContent(i);
      minLoc=i;
    }
    if (consec>2 || hist->GetBinContent(i)>minVal*10 || (hist->GetBinContent(i)==0 && i<maxLoc)){  //when reached upward slope of at least 2 bins, stop looking for min
      minFound=true;
      break;
    }
    last=hist->GetBinContent(i);
  }
  if (minFound){  //integrate zeroeth peak
    for (int i=hist->FindBin(0); i>minLoc; i--){
      sum+=hist->GetBinContent(i);
    }
  }
  else
    std::cout << "No minimum\n";

  hist->Write();  //save histogram in results.root
  run++;
  
  return sum;
}
//---------------------------------------------------------------------
