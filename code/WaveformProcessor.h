#ifndef WaveformProcessor_h
#define WaveformProcessor_h

class TH1F;
class TH1D;
class TGraph;
class DataFile;
class TF1;
class Waveform;
class TNtuple;

struct Pulse{
    double mBaseline;
    double mAbsAmp;
    double mAmp;
    double mTime;
    double mQ;
    double mWidth;
    int mFirstPulseInGroup;
    double m50NanoAvg;
    double mPeakRise;
    double mAvgVar;
    double mPeakiness;
};

struct EventData{
    double pulseBounds[100][3];
    double peaks[100];
    int peakBins[100];
    double peakiness[100];
    double lb;
    double nPulse;
    double nPeak;
  double lMax[1000][2];
  int nlMax;
};

struct BinData{
  bool islMax;
  bool isProbPeak;
  bool isPeak;
  bool edge;
  double rise;
  double slopecheck;
  double peakiness;
};

class WaveformProcessor{
public:
  WaveformProcessor(const char* aSetupFileName, int aRun, int aChannel, int aPolarity);
    ~WaveformProcessor();
    static WaveformProcessor* instanceForRootMacro(const char* aSetupFileName, int aRun, int aChannel=1, int aPolarity=-1);

    // ---
    char* getFileName(){return mFileName;}
    int getPolarity() {return mPolarity;}

    // --- Access  to waveform
    void readNextWaveform();
    void readWaveform(int aIndex);
    void readWaveform(const char* aHName);
    int getCurrentWaveformIndex();
    Waveform* getCurrentWaveform(){return mWF;}

    // --- baseline
    int processBaseline();
    void calcDeriv(int binSep);
    TH1F* getBaselineHisto(){return mHBaseline;}
    TH1D* getDeriv(int binSep);
    TH1D* getSecDeriv(int binSepFirstDeriv, int binSep);
    TH1D* getDerivHisto(int binSep){return mHDeriv[binSep];}
    double getBaselineMu(){return mBmu;}
    double getBaselineRMS(){return mBRMS;}

    // --- pulse finding
    void smoothWaveform(double lenNs=30, int flag=0);
    int findPulseGeneric(double nSSCR, double nSSCF, double dAFR, double dAFF, double lBLF, double mPW, double pT, int flag=0, int buf=0);
    int findSmallPulse(int startingBin, int endingBin);
    Waveform* getUnsmoothedWF(){return mDataWF;}

    EventData mEventData;
    BinData* mBins;
    bool mForMacro;

    int getWaveformCount(){return mDataFile->getWaveformCount();}

    int getNPulse(){return mNPulse;}
    double getPulseTime(int aPulse){return mPulse[aPulse].mTime;}
    double getPulseAbsAmplitude(int aPulse){return mPulse[aPulse].mAbsAmp;}
    double getPulseAmplitude(int aPulse){return mPulse[aPulse].mAmp;}
    double getPulseBaseline(int aPulse){return mPulse[aPulse].mBaseline;}
    double getPulseCharge(int aPulse){return mPulse[aPulse].mQ;}
    double getPulseWidth(int aPulse){return mPulse[aPulse].mWidth;}
    double get50NanoAvg(int aPulse){return mPulse[aPulse].m50NanoAvg;}
    double getPeakRise(int aPulse){return mPulse[aPulse].mPeakRise;}
    double getAvgVar(int aPulse){return mPulse[aPulse].mAvgVar;}
    double getPeakiness(int aPulse){return mPulse[aPulse].mPeakiness;}
    TGraph* getPulseGraph();
    void findClearPulses(TNtuple* ntp, int start, int end, double level, int verbose);
    double findMin(int start, int end, int verbose=0);

    //
    double get_blrms(){return mDiffAmpThresh;}

    double getTriggerTime(){return mDataFile->getTriggerTime();}

private:
    static WaveformProcessor* mInstanceForRootMacro;
    static int mRun;
    static int mChannel;
    static int mEv;
    void init(const char* aSetupFileName, int aRun, int aChannel, int aPolarity);
    void clear();
    void setForMacro(bool flag){mForMacro=flag;}
    void reFitMulti(); // controlled by type
    // >>> parameters
    char mSetupFileName[200];
    char mFileName[200];
    double mWFAmpBining;
    double mAmpMin;
    double mAmpMax;
    double mNoise1Bin;
    double mDiffAmpThresh;
    int mPolarity;

    // >>> Raw data
    DataFile* mDataFile;
    Waveform* mWF;

    // >>> Tools
    TH1F* mHBaseline;
    TH1D* mHDeriv[50];
    TH1D* mDeriv[50];
    TH1D* mSecDeriv[50];
    TF1* mDerivFit[50];
    TF1* mHBaselineFit;
    double mBmu;
    double mBRMS;
    double mDerivRMS[50];
    double mDerivmu[50];
    Waveform* mSmoothedWF;
    Waveform* mDataWF;

    // >>> Pulse
    int mNPulse;
    int mNPulseMax;
    Pulse* mPulse;
    int* mPulseIndex;
    double* mPulseTime;//for sorting only
    bool mPulseFound;

};

#endif
