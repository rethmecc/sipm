import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0.01,10,1000)
def fnc(a,alpha):
  s = np.array(a)
  for i in range(len(s)):
    s[i] = s[i-1]*(1.-alpha) + alpha*s[i]
  return s

y=fnc(x,0.34)
plt.plot(x,y)
plt.grid()
plt.show()
