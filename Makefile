INC_DIR         = $(MIDASSYS)/include
LIB_DIR         = $(MIDASSYS)/linux/lib
DRV_DIR         = $(MIDASSYS)/drivers
MSCB_DIR        = $(MIDASSYS)/../mscb


LIBS = -lbsd -lm -lutil -lpthread -lrt
OSFLAGS = -DOS_LINUX -DHAVE_LIBUSB -DHAVE_USB
DRIVERS =  obj/strlcpy.o obj/mxml.o obj/mscb.o obj/musbstd.o
LIB = $(LIB_DIR)/libmidas.a -lusb

CC = cc
CXX = c++
GFLAGS = -g
LDFLAGS =
CFLAGS = -c -fPIC $(GFLAGS) $(shell root-config --cflags) -I. -I$(HDF5SYS)/include -I$(MIDASSYS)/../mxml -I$(INC_DIR) -I$(DRV_DIR) -I$(MSCB_DIR)
ifneq (,$(wildcard $(HDF5SYS)/lib64))
    LFLAGS      = $(GFLAGS) $(shell root-config --libs) -lMinuit -lSpectrum -L$(HDF5SYS)/lib64 -lhdf5 -lhdf5_cpp
else
    LFLAGS      = $(GFLAGS) $(shell root-config --libs) -lMinuit -lSpectrum -L$(HDF5SYS)/lib -lhdf5 -lhdf5_cpp
endif
CXXFLAGS =-O3 -std=c++0x -pg -D_DEBUG -g -c -Wall -I$(MIDASSYS)/../mxml -I$(INC_DIR) -I$(DRV_DIR) -I$(MSCB_DIR)

ALL: lib/libSipmAnalysis.so bin/PulseFinding.exe DAQSoftware/setvolt.exe DAQSoftware/readcurrent.exe bin/NormGain.exe bin/integrateWF.exe bin/efficiencyWF.exe

NG : bin/NormGain.exe

FN : bin/fillNtp.exe

IW : bin/integrateWF.exe

EW : bin/efficiencyWF.exe

PF : bin/PulseFinding.exe

lib : lib/libSipmAnalysis.so

DAQ : DAQSoftware/setvolt.exe DAQSoftware/readcurrent.exe


obj/mscbdev.o: $(DRV_DIR)/device/mscbdev.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
obj/musbstd.o: $(DRV_DIR)/usb/musbstd.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
obj/mscbrpc.o: $(MSCB_DIR)/mscbrpc.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
obj/mscb.o: $(MSCB_DIR)/mscb.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
obj/mxml.o: $(MIDASSYS)/../mxml/mxml.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
obj/strlcpy.o: $(MIDASSYS)/../mxml/strlcpy.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<


DAQSoftware/setvolt.exe:  $(LIB) $(LIB_DIR)/mfe.o obj/setvolt.o $(DRIVERS)
	$(CXX) -o $@ obj/setvolt.o $(LIB_DIR)/mfe.o $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

obj/setvolt.o: DAQSoftware/setvolt.cxx
	$(CXX) $(CFLAGS) $(OSFLAGS) -c $< -o $@

DAQSoftware/readcurrent.exe:  $(LIB) $(LIB_DIR)/mfe.o obj/readcurrent.o $(DRIVERS)
	$(CXX) -o $@ obj/readcurrent.o $(LIB_DIR)/mfe.o $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

obj/readcurrent.o: DAQSoftware/readcurrent.cxx
	$(CXX) $(CFLAGS) $(OSFLAGS) -c $< -o $@



obj/LecroyHdfFile.o : code/LecroyHdfFile.cxx code/LecroyHdfFile.h code/Waveform.h
	g++ -o obj/LecroyHdfFile.o code/LecroyHdfFile.cxx $(CFLAGS)

lib/libSipmAnalysis.so : obj/Waveform.o obj/WaveformProcessor.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/RootDict.o
	g++ -shared -fPIC -o lib/libSipmAnalysis.so $^ $(LFLAGS)

bin/PulseFinding.exe : obj/PulseFinding.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o obj/Waveform.o
	g++ -o bin/PulseFinding.exe obj/PulseFinding.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o  obj/Waveform.o $(LFLAGS)
	
bin/efficiencyWF.exe : obj/efficiencyWF.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o obj/Waveform.o
	g++ -o bin/efficiencyWF.exe obj/efficiencyWF.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o  obj/Waveform.o $(LFLAGS)
	
bin/fillNtp.exe : obj/fillNtp.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o obj/Waveform.o
	g++ -o bin/fillNtp.exe obj/fillNtp.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o  obj/Waveform.o $(LFLAGS)
	
bin/NormGain.exe : obj/NormGain.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o obj/Waveform.o
	g++ -o bin/NormGain.exe obj/NormGain.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o  obj/Waveform.o $(LFLAGS)

bin/integrateWF.exe : obj/integrateWF.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o obj/Waveform.o
	g++ -o bin/integrateWF.exe obj/integrateWF.o obj/LecroyFile.o obj/LecroyHdfFile.o obj/WaveformProcessor.o  obj/Waveform.o $(LFLAGS)

obj/PulseFinding.o : code/PulseFinding.cxx code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h
	g++ -o obj/PulseFinding.o code/PulseFinding.cxx $(CFLAGS)

obj/efficiencyWF.o : code/efficiencyWF.cxx code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h
	g++ -o obj/efficiencyWF.o code/efficiencyWF.cxx $(CFLAGS)
	
obj/fillNtp.o : code/fillNtp.cxx code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h
	g++ -o obj/fillNtp.o code/fillNtp.cxx $(CFLAGS)
	
obj/NormGain.o : code/NormGain.cxx code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h
	g++ -o obj/NormGain.o code/NormGain.cxx $(CFLAGS)
	
obj/integrateWF.o : code/integrateWF.cxx code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h
	g++ -o obj/integrateWF.o code/integrateWF.cxx $(CFLAGS)
	
obj/Waveform.o : code/Waveform.cxx code/Waveform.h 
	g++ -o obj/Waveform.o code/Waveform.cxx $(CFLAGS)

obj/LecroyFile.o : code/LecroyFile.cxx code/LecroyFile.h code/DataFile.h code/Waveform.h
	g++ -o obj/LecroyFile.o code/LecroyFile.cxx $(CFLAGS)

obj/WaveformProcessor.o : code/WaveformProcessor.cxx code/LecroyFile.h
	g++ -c -o obj/WaveformProcessor.o $(CFLAGS) code/WaveformProcessor.cxx

obj/RootDict.o : code/Waveform.h code/LecroyFile.h code/LecroyHdfFile.h code/WaveformProcessor.h code/Linkdef.h
	rootcint -f code/RootDict.cxx -c -I$(HDF5SYS)/include $^
	g++ -c -o$@ code/RootDict.cxx $(CFLAGS)

%.o: %.cxx experim.h
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

.c.o:
	$(CC) $(CFLAGS)  $(OSFLAGS) -c $<

$(LIB) $(LIB_DIR)/mfe.o:
	@cd $(MIDASSYS) && make


clean:
	rm -f obj/*.o bin/*.exe lib/*.so DAQSoftware/*.o DAQSoftware/*.exe DAQSoftware/*.pyc
