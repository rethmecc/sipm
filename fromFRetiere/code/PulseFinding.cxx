/*******************************************************************************
* Distribution.cxx
*
* Description:
* Reads a wfm file and produces an ntuple with the distribution of the pulses
* for waveforms in that wfm file.
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "LecroyFile.h"
#include "WaveformProcessor.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"

// Parameters

int main(int argc, char** argv){
  int aRun = argc>1 ? atoi(argv[1]) : 0;
  int aFitType = argc>2 ? atoi(argv[2]) : 0;
  int aNEventMax = argc>3 ? atoi(argv[3]) : 100000000;
  char aRunInfo[100];
  if(argc>4) sprintf(aRunInfo,"ntp%s/%sRunInfo.txt",argv[4],argv[4]);
  else strcpy(aRunInfo,"RunInfo.txt");
  int aChannel = argc>4 ? atoi(argv[5]) : 1;

  WaveformProcessor wfProc(aRunInfo,aRun,5,aChannel);
  // --- Open output file
  //>>> Strip directory name from file name
  int slashIndex=0;
  for(int index=0; index<strlen(wfProc.getFileName()); index++){
    if(strncmp(wfProc.getFileName()+index,"/",1)==0) slashIndex=index;
  }
  char outFileName[200];
  if(argc>4){
    sprintf(outFileName,"ntp%s/%s.fanat%i",argv[4],wfProc.getFileName()+slashIndex,aFitType);
  }
  else{
    sprintf(outFileName,"ntp/%s.fanat%i",wfProc.getFileName()+slashIndex,aFitType);
  }
  //sprintf(outFileName,"/home/huth/Desktop/nEXO/testdata%s.fanat%i",wfProc.getFileName()+slashIndex,aFitType);
  std::cout << outFileName <<  " " << wfProc.getBaselineRMS() << slashIndex << std::endl;

  TFile outFile(outFileName ,"RECREATE");
  TNtuple ntp("ntp","ntp",
	      "evt:tt:blmu:blRMS:np:pbl:paa:pa:pt:pq:pw:tchi2:fa:ft:frt:fft:ff2:fft2:fblmu:fchi2:ndf:frchi2:frndf");
  float ntpCont[20];
  int skippedcount = 0;
  // ---
  int nEvent = wfProc.getWaveformCount();
  if(nEvent>aNEventMax) nEvent=aNEventMax;
  for(int iEvent=0; iEvent<nEvent; iEvent++){
    wfProc.readNextWaveform();
    int skipFit=0;
    if(wfProc.processBaseline() && wfProc.findPulse()){
      wfProc.fit(aFitType);
    }
    else{
      skippedcount++;
      skipFit=1;
    }
    //std::cout << "Event: " << iEvent <<"\t nPulses: "
    //          << wfProc.getNPulse() << std::endl;
    for(int iPulse=0; iPulse<wfProc.getNPulse(); iPulse++){
      ntpCont[0]=iEvent;
      ntpCont[1]=wfProc.getTriggerTime();
      ntpCont[2]=wfProc.getBaselineMu();
      ntpCont[3]=wfProc.getBaselineRMS();
      ntpCont[4]=wfProc.getNPulse();
      ntpCont[5]=wfProc.getPulseBaseline(iPulse);
      ntpCont[6]=wfProc.getPulseAbsAmplitude(iPulse);
      ntpCont[7]=wfProc.getPulseAmplitude(iPulse);
      ntpCont[8]=wfProc.getPulseTime(iPulse);
      ntpCont[9]=wfProc.getPulseCharge(iPulse);
      ntpCont[10]=wfProc.getPulseWidth(iPulse);
      ntpCont[11]=wfProc.getSPTemplateChi2(iPulse);
      ntpCont[12]=wfProc.getFitAmplitude(iPulse);
      ntpCont[13]=wfProc.getFitTime(iPulse);
      ntpCont[14]=wfProc.getFitRiseTime(iPulse);
      ntpCont[15]=wfProc.getFitFallTime(iPulse);
      ntpCont[16]=wfProc.getFitTime2Frac(iPulse);
      ntpCont[17]=wfProc.getFitFallTime2(iPulse);
      ntpCont[18]=wfProc.getFitBaseline(iPulse);
      ntpCont[19]=wfProc.getChi2(iPulse);
      ntpCont[20]=skipFit? -1 : wfProc.getNDF(iPulse);
      ntpCont[21]=wfProc.getChi2Refit(iPulse);
      ntpCont[22]=wfProc.getNDFRefit(iPulse);
      ntp.Fill(ntpCont);
    }

    if(iEvent==0) wfProc.getBaselineHisto()->Write();
    
    if(floor(iEvent*200./nEvent)==(iEvent*200./nEvent)){
        std::cout << iEvent << "/" << nEvent << std::endl; //"\r";
        //std::cout.flush();
    }
    //if(iEvent>195000) std::cout << iEvent << std::endl;
  }		

  std::cout << "Writing output in " << outFileName << std::endl;
  std::cout << "Skipped " << skippedcount << " triggers." << std::endl;
  ntp.Write();

}

