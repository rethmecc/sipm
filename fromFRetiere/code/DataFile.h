/*******************************************************
* File reader interface
*
* History:
* v1	2011/11/25	Initial version (Kyle Boone)
*********************************************************/

#ifndef DATA_FILE_H
#define DATA_FILE_H

class Waveform;

class DataFile {
  protected:
    int mWaveformCount;
    bool mGood;
    int mRun;
    double mTriggerTime;

  public:
    // General functions
    
    DataFile(int run) : mGood(true), mWaveformCount(0), mRun(run), mTriggerTime(0) {}

    int getWaveformCount() { return mWaveformCount; }
    int getRun() { return mRun; }
    double getTriggerTime(){return mTriggerTime;}

    // If the file is not opened properly, or if there are errors reading it
    // this will return false.
    bool isGood() { return mGood; }

    virtual ~DataFile() {};

    // Interface to be implemented by subclasses

    // Retrieve the waveform at a given index
    virtual Waveform* getWaveform(int index) = 0;
		virtual Waveform* getWaveform(const char* aName){return 0;}
		virtual Waveform* getNextWaveform(){return 0;};
		
    // Set the format for the names of waveforms that will be retrieved.
    virtual void setWaveformNameFormat(const char* format) {};
};

#endif // include guard
