/*******************************************************************************
* DarkNtpAnalysis.cxx
*
* Description:
* Analyzes an NTuple for dark noise, afterpulse probability, etc. Also provides
* formatting for histogram generation.
*
*******************************************************************************/

#include <sys/stat.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"

#include "DarkNtpAnalysis.h"
#include "WaveformProcessor.h"
#include "LecroyFile.h"
#include "LecroyHdfFile.h"
#include "Waveform.h"

using namespace std;

DarkNtpAnalysis::DarkNtpAnalysis(int run1, double windowsize)
{
	init(run1, 0, windowsize, false);
	nDatasets = 1;
}

DarkNtpAnalysis::DarkNtpAnalysis(int run1, int run2, double windowsize)
{
	init(run1, run2, windowsize, true);
	nDatasets = 2;
}

//par[0] = tau, par[1] = p_ap, par[2] = tau_ap
double fitFunction(double *x, double *par)
{
	double arg1 = 0, arg2 = 0, fitval = 0;
	if(par[0] != 0) arg1 = 1/par[0];
	if(par[2] != 0) arg2 = arg1 + 1/par[2];
	
	fitval = par[1]*exp(-1*x[0]*arg2)*arg2 + arg1*exp(-1*x[0]*arg1)*(1-par[1]);
	return fitval;
}

void DarkNtpAnalysis:init(int run1, int run2, double windowsize)
{
	aRun1 = run1;
	aRun2 = run2;
	maxTime = windowsize * 1e-9;

	char configname[100];
	sprintf(configname, "./code/DarkAnalysis.config");
	ifstream iFile;
	iFile.open(configname);

	char buffer[50];

	//useFittedPeaks
	iFile >> buffer >> buffer >> buffer >> buffer;
	useFittedPeaks = (buffer != "false");

	//fitPulses
	iFile >> buffer >> buffer >> buffer >> buffer;
	fitPulses = (buffer != "false");

	//savePulseFits
	iFile >> buffer >> buffer >> buffer >> buffer;
	savePulseFits = (buffer != "false");

	//triggerMinTime
	iFile >> buffer >> buffer >> buffer >> buffer;
	triggerMinTime = atof(buffer);

	//prePulseThreshold
	iFile >> buffer >> buffer >> buffer >> buffer;
	prePulseThreshold = atof(buffer);

	//polarity
	iFile >> buffer >> buffer >> buffer >> buffer;
	polarity = atoi(buffer);

	//xTalkThreshold
	iFile >> buffer >> buffer >> buffer >> buffer;
	xTalkThreshold = atof(buffer);

	//fitType
	iFile >> buffer >> buffer >> buffer >> buffer;
	fitType = atoi(buffer);
	
}

//For collection data from a single data file
void DarkNtpAnalysis::processData(int dataSet)
{
	char* fileName;

	int currentRun;
	int nPulse;
	int iPulse;
	int nEventPulse;
	int nPostTriggerPulse;
	int nFittedPulse;

	int firstPulseIndex;
	int secondPulseIndex;
	int xTalkIndex;

	bool prePulseExists;
	bool subPulseExists;
	bool xTalkPresent;

	double peakTime[10];
	double peakAmp[10];
	double timeDiff;

	double ampValues[300000];
	double riseValues[300000];
	double fallValues[300000];
	double chiValues[300000];

	int peakCount [10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};



	pulseAmplitude = 0;
	pulseRiseTime = 0;
	pulseFallTime = 0;
	pulseChi2 = 0;
	UNCpulseAmplitude = 0;
	UNCpulseRiseTime = 0;
	UNCpulseFallTime = 0;
	UNCpulseChi2 = 0;

	if(dataSet == 1) currentRun = aRun1;
	else currentRun = aRun2;

	fileName = getFileName(currentRun, "./RunInfo.txt", fitType);

	TFile* f1 = new TFile(fileName);
	TNtuple* ntp = (TNtuple*) f1->Get("ntp");
	NtpCont* cont = (NtpCont*) ntp->GetArgs();

	nPulse = ntp->GetEntries();
	iPulse = 0;
	nEvent = 0;
	nPostTriggerPulse = 0;
	nFittedPulse = 0;

	prePulseExists = false;
	xTalkPresent = false;
	subPulseExists = false;

	firstPulseIndex = 0;
	secondPulseIndex = 0;
	xTalkIndex = 0;

	for(int i = 0; i < 10; i++) xTalkCounts[i] = 0;

	while(iPulse<nPulse)
	{
		//Stores data for all peaks in a given event
		ntp->GetEntry(iPulse);
		if(useFittedPeaks){
			nEventPulse = cont->nfp;
			peakTime[0] = cont->ft;
			peakAmp[0] = cont->fa;
		}
		else{
			nEventPulse = cont->np;
			peakTime[0] = cont->pt;
			peakAmp[0] = cont->pa;
		}
		for(int i=1; i<nEventPulse; i++)
		{
			iPulse++;
			if(useFittedPeaks){
				ntp->GetEntry(iPulse);
				peakTime[i] = cont->ft;
				peakAmp[i] = cont->fa;
			}
			else{
				ntp->GetEntry(iPulse);
				peakTime[i] = cont->pt;
				peakAmp[i] = cont->pa;
			}
		}

		//Checks for pre-trigger peaks
		for(int i=0; i<nEventPulse; i++)
		{
			if((peakTime[i] < triggerMinTime) && (peakTime[i] > prePulseThreshold)) prePulseExists = true;
			else if (peakTime[i] > triggerMinTime) nPostTriggerPulse++;
		}	

		//Finds the first peak of the event after the trigger
		for(int i=1; i<nEventPulse; i++)
		{
			if(peakTime[firstPulseIndex] > triggerMinTime)
			{
				if((peakTime[i] > triggerMinTime) && (peakTime[i] < peakTime[firstPulseIndex])) firstPulseIndex = i;
			}
			else firstPulseIndex = i;
		}
		nEvent++;

		//Counts cross-talk

		if(peakAmp[firstPulseIndex]*polarity > xTalkThreshold)
		{
			if(dataSet == 1) xTalkCounts[xTalkIndex]++;
			xTalkPresent = true;
		}


		if((!xTalkPresent) && (!prePulseExists))
		{
			//Checks for a subsequent peak
			for(int i=0; i<nEventPulse; i++)
			{
				if(peakTime[i] > peakTime[firstPeakIndex])
				{
					subPulseExists = true;
				}
			}
	
			if(subPulseExists)
			{
				//Finds the subsequent peak
				for(int i=1; i<nEventPulse; i++)
				{
					if(peakTime[secondPeakIndex] > peakTime[firstPeakIndex])
					{
						if((peakTime[i] > peakTime[firstPeakIndex]) && (peakTime[i] < peakTime[secondPeakIndex])) secondPeakIndex = i;
					}
					else secondPeakIndex = i;
				}

				//Calculates the time difference between the two and puts into a histogram
				timeDiff = peakTime[secondPeakIndex] - peakTime[firstPeakIndex];
				if(timeDiff < maxTime)
				{
					if(dataSet == 1) TimeDist1->Fill(timeDiff*1e9);
					else TimeDist2->Fill(timeDiff*1e9);
		
				}
			}

			else if ((nEventPulse == 1) && (fitPulses) && (dataSet == 1))
			{
				//Calculated parameters related to the fit shape
				WaveformProcessor* wfProc = new WaveformProcessor("RunInfo.txt",currentRun);
				wfProc->setQuietMode(true);
				wfProc->readWaveform(cont->evt);
				wfProc->processBaseline();
				wfProc->findPulse();

				if(wfProc->getNPulse()==1)
				{
					wfProc->setFitOption("QR0");
					wfProc->fitSingle(fitType);
					if(fitType==7){
						wfProc->reFitMulti();
					}
					if(savePulseFits)
					{
						gROOT->SetBatch();
						TCanvas *c1 = new TCanvas("PulsesFit", "PulsesFit", 900, 700);
						Waveform* wf = wfProc->getCurrentWaveform();
						wf->SetLineColor(1);
						wf->Draw();
						TGraph* GPulse = wfProc->getPulseGraph();
						GPulse->SetMarkerStyle(23);
						GPulse->SetMarkerColor(2);
						GPulse->Draw("p");
					
						wfProc->getFitFunction()->Draw("same");
						if((fitType==8)) wfProc->getFitFunction2()->Draw("same");

						char filename [100];
						sprintf(filename, "/home/deap/nEXO/Pulse Fits 2/%d/Single Pulse/%d-%d.pdf", currentRun, cont->evt, fitType);
						c1->SaveAs(filename);

						delete wf;
						delete c1;
						delete GPulse;
					}
					pulseAmplitude = pulseAmplitude * nFittedPulse + wfProc->getFitAmplitude(0);
					pulseAmplitude /= (nFittedPulse+1);
					ampValues [nFittedPulse] = pulseAmplitude;
					pulseRiseTime = pulseRiseTime * nFittedPulse + wfProc->getFitRiseTime();
					pulseRiseTime /= (nFittedPulse+1);
					riseValues [nFittedPulse] = pulseRiseTime;
					pulseFallTime = pulseFallTime * nFittedPulse + wfProc->getFitFallTime();
					pulseFallTime /= (nFittedPulse+1);
					fallValues [nFittedPulse] = pulseFallTime;
					pulseChi2 = pulseChi2 * nFittedPulse + wfProc->getChi2();
					pulseChi2 /= (nFittedPulse+1);
					chiValues [nFittedPulse] = pulseChi2;
					nFittedPulse++;
				}
				delete wfProc;
			}

		}

		peakCount[nPostTriggerPulse]++;	

		nPostTriggerPulse = 0;
		firstPulseIndex = 0;
		secondPulseIndex = 0;
		
		if(xTalkIndex == 9) xTalkIndex = 0;
		else xTalkIndex++;

		xTalkPresent = false;
		prePulseExists = false;
		subPulseExists = false;

		int event_i = cont->evt;
		if(event_i % 100 == 0) cout << "Event " << cont->evt << " processed." << endl;

		iPulse++;
	}

	//Finds uncertainties in parameters related to the fit shape
	if((fitPulses == true) && (dataSet == 1))
	{
		for(int i = 0; i < nFittedPulse; i++)
		{
			UNCpulseAmplitude += (ampValues[i]-pulseAmplitude)*(ampValues[i]-pulseAmplitude);
			UNCpulseRiseTime += (riseValues[i]-pulseRiseTime)*(riseValues[i]-pulseRiseTime);
			UNCpulseFallTime += (fallValues[i]-pulseFallTime)*(fallValues[i]-pulseFallTime);
			UNCpulseChi2 += (chiValues[i]-pulseChi2)*(chiValues[i]-pulseChi2);
		}
		UNCpulseAmplitude /= nFittedPulse;
		UNCpulseAmplitude = sqrt(UNCpulseAmplitude);
		UNCpulseAmplitude /= sqrt(1.0 * nFittedPulse);

		UNCpulseRiseTime /= nFittedPulse;
		UNCpulseRiseTime = sqrt(UNCpulseRiseTime);
		UNCpulseRiseTime /= sqrt(1.0 * nFittedPulse);

		UNCpulseFallTime /= nFittedPulse;
		UNCpulseFallTime = sqrt(UNCpulseFallTime);
		UNCpulseFallTime /= sqrt(1.0 * nFittedPulse);

		UNCpulseChi2 /= nFittedPulse;
		UNCpulseChi2 = sqrt(UNCpulseChi2);
		UNCpulseChi2 /= sqrt(1.0 * nFittedPulse); 
	}
	else if ((fitPulses == false) && (dataSet == 1))
	{
		pulseAmplitude = 0;
		UNCpulseAmplitude = 0;
		pulseRiseTime = 0;
		UNCpulseRiseTime = 0;
		pulseFallTime = 0;
		UNCpulseFallTime = 0;
		pulseChi2 = 0;
		UNCpulseChi2 = 0;
	}

	for(int i = 1; i < 10; i++) cout << i << " peaks = " << peakCount[i] << endl;
}

//calculate all parameters
void DarkNtpAnalysis::calculateAllParameters()
{
	char DName [30];

	sprintf(DName,"Timing Distribution %i",aRun);

	TimeDist = new TH1D(DName,DName,maxTime*1e9,1,maxTime*1e9);

	TimeDist1 = new TH1D("DNoise","DNoise",100,1,maxTime*1e9); //nspb = maxTime / 100
	processData(1);

	TimeDist2 = new TH1D("APulsing","APulsing",100,1,1000);
	processData(2);

	maxTime *= 1e9;

	for (int i = 1; i < TimeDist2->GetXaxis()->GetNbins(); i++)
	{
		TimeDist->SetBinContent(TimeDist->FindBin(TimeDist2->GetBinCenter(i)), TimeDist2->GetBinContent(i));
		TimeDist->SetBinError(TimeDist->FindBin(TimeDist2->GetBinCenter(i)), TimeDist2->GetBinError(i));
	}
	double endheight = TimeDist2->GetBinContent(TimeDist2->GetXaxis()->GetNbins() - 1);
	double startheight = TimeDist1->GetBinContent(2);
	double heightratio = endheight / startheight;
	for (int i = 1; i < TimeDist1->GetXaxis()->GetNbins(); i++)
	{
		TimeDist->SetBinContent(TimeDist->FindBin(TimeDist1->GetBinCenter(i)), TimeDist1->GetBinContent(i) * heightratio);
		TimeDist->SetBinError(TimeDist->FindBin(TimeDist1->GetBinCenter(i)), TimeDist1->GetBinError(i) * heightratio);
	}

	//TimeDist->SetMaximum(1e-6);

	TimeDist1->Draw("EP");
	TimeDist2->Draw("EP same");
	TimeDist->GetXaxis()->SetTitle("Time Distribution(ns)");
	TimeDist->GetYaxis()->SetTitle("Counts");
	TimeDist->GetXaxis()->CenterTitle();
	TimeDist->GetYaxis()->CenterTitle();

	if ((mode == 2) || (mode == 3) || (mode == 8))
	{

		double integral = TimeDist->Integral("width");
		cout << "Integral: " << integral << endl;
		for (int i = 0; i <= TimeDist->GetXaxis()->GetNbins(); i++){
			TimeDist->SetBinContent(i, TimeDist->GetBinContent(i) / integral);
			TimeDist->SetBinError(i, TimeDist->GetBinError(i) / integral);
		}

		TF1* f1 = new TF1("fit", fitFunction, 0, maxTime, 3);
		f1->SetParameter(0, 11000);
		f1->SetParameter(1, 0.17);
		f1->SetParLimits(1, 0.0, 1.0);
		f1->SetParameter(2, 120);
		TimeDist->Fit("fit", "", "");
		f1->Draw("same");

		double error;
		double percerror;

		double Tau = f1->GetParameter(0);
		darkNoiseRate = 1/Tau;
		cout << "DN Rate: " << darkNoiseRate << endl;
		error = f1->GetParError(0);
		percerror = error / Tau;
		UNCdarkNoiseRate = percerror * darkNoiseRate;


		double p_ap = f1->GetParameter(1);
		avgNumAfterpulse = 1/(1-p_ap);
		cout << "Apulse: " << avgNumAfterpulse << endl;
		error = f1->GetParError(1);
		percerror = error / (1-p_ap);
		UNCavgNumAfterpulse = percerror * avgNumAfterpulse;

		crossTalkProbability = 1.0 * numxtalk / totalevents;
		//EVALUATE
		UNCcrossTalkProbability = 0;
		for(int i = 0; i < 10; i++) UNCcrossTalkProbability += (10.0*xtalkcounts[i]/totalevents - crossTalkProbability) * (10.0*xtalkcounts[i]/totalevents - crossTalkProbability);
		UNCcrossTalkProbability /= 10;
		UNCcrossTalkProbability = sqrt(UNCcrossTalkProbability);
		UNCcrossTalkProbability /= sqrt(10.0);
	}

	char savename[100];
	if (mode == 8) sprintf(savename, "./DarkAnalysis/Combined Distros/%i-%i.pdf", aRun, par2);
	else sprintf(savename, "./DarkAnalysis/Single Distros/%i-%i.pdf", aRun, mode);
	c1->SaveAs(savename);

}

//Make plots. If true, save and close plots
void DarkNtpAnalysis::makePlots() 
{
	TCanvas *c1 = new TCanvas("Timing Distribution", "Timing Distribution", 900, 700);
	c1->SetLogx();
	c1->SetLogy();

	TimeDist->Draw();
	char savename [100];
	sprintf(savename, "Time Distribution %i.pdf", aRun);

	c1->SaveAs(savename);
}

//write all parameters to a text file
void DarkNtpAnalysis::writeAll(int run, int mode)
{
	char savename[100];
	if(willFit == true) sprintf(savename, "./DarkAnalysis/Parameters - %i/%i-DarkAnalysis.txt", mode, run);
	else sprintf(savename, "./DarkAnalysis/Parameters - %i/%i-DarkAnalysis-NoFit.txt", mode, run);
	ofstream oFile;
	oFile.open(savename);
	oFile << "DN_Rate\tAvgNumAfterpulse\tCrosstalkProb\tPulseAmp\tPulseRiseTime\tPulseFallTime\tPulseChi2\n";
	oFile << darkNoiseRate << " +- " << UNCdarkNoiseRate << "\t";
	oFile << avgNumAfterpulse << " +- " << UNCavgNumAfterpulse << "\t";
	oFile << crossTalkProbability << " +- " << UNCcrossTalkProbability << "\t";
	oFile << pulseAmplitude << " +- " << UNCpulseAmplitude << "\t";
	oFile << pulseRiseTime << " +- " << UNCpulseRiseTime << "\t";
	oFile << pulseFallTime << " +- " << UNCpulseFallTime << "\t";
	oFile << pulseChi2 << " +- " << UNCpulseChi2;
	oFile.close();
}

//Opens run data file of type .anat(#) where # = aTraining
char* DarkNtpAnalysis::getFileName(int run, char* aLog, int aTraining=0)
{
	char FileNames [100] [2] [100];
	char* activename = new char[1024];
	ifstream iFile;
	iFile.open(aLog);
	int counter = 0;
	char buffer [10000];
	int runnum;
	char* newbuff;
	for (int i = 0; i < 10; i++){
		iFile >> buffer;
	}
	while(!iFile.eof()){
		iFile >> FileNames[counter][0];
		iFile >> FileNames[counter][1];
		sscanf(FileNames[counter][0], "%d", &runnum);
		if((runnum == run)){
			newbuff = FileNames[counter][1];
			sprintf(activename, "%s.anat%i", newbuff, aTraining);	
			return activename;
		}
		for (int i = 0; i < 8; i++){
			iFile >> buffer;
		}
	}
	std::cout << "File not found" << std::endl;
}
