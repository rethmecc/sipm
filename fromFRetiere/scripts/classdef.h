#include <iostream>
using namespace std;

class Rectangle {
    int width, height;
  public:
    Rectangle(int, int);
    int area();
};
